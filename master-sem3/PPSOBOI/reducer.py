#!/usr/bin/python3
import sys

def main(argv, separator='\t'):
    current_user = None
    current_count = 0
    user_id = None

    # input comes from STDIN
    for line in sys.stdin:
        # remove leading and trailing whitespace
        line = line.strip()

        # parse the input we got from mapper.py
        user_id, count = line.split(separator, maxsplit=1)

        # convert count (currently a string) to int
        try:
            count = int(count)
        except ValueError:
            # count was not a number, so silently
            # ignore/discard this line
            continue

        # this IF-switch only works because Hadoop sorts map output
        # by key (here: user_id) before it is passed to the reducer
        if current_user == user_id:
            current_count += count
        else:
            if current_user:
                print(f'{current_user}{separator}{current_count}')
            current_count = count
            current_user = user_id

    # do not forget to output the last word if needed!
    if current_user == user_id:
        print(f'{current_user}{separator}{current_count}')

if __name__ == "__main__":
    main(sys.argv)
