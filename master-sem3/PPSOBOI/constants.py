S3_BUCKET_NAME = 'aws-bucket-content'

USERS_TOPIC = 'users'
USER_CONNECTIONS_TOPIC = 'user_connections'

USERS_FOLDER_PREFIX = 'hadoop/users'
USER_CONNECTIONS_FOLDER_PREFIX = 'hadoop/user_connections'

BOOTSTRAP_SERVERS = ['b-2.demo-kafka-1.xyyscb.c2.kafka.eu-north-1.amazonaws.com:9094',
                     'b-1.demo-kafka-1.xyyscb.c2.kafka.eu-north-1.amazonaws.com:9094']
