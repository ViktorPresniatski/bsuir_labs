#!/usr/bin/python3
import sys

def main(argv, separator='\t'):
    for line in sys.stdin:
        line = line.rstrip()
        user_id1, user_id2 = line.split(',', maxsplit=1)
        print('%s%s%d' % (user_id1, separator, 1))
        print('%s%s%d' % (user_id2, separator, 1))


if __name__ == "__main__":
    main(sys.argv)
