import time
from threading import Thread

import boto3
import logging

from kafka import KafkaProducer

from constants import S3_BUCKET_NAME, USERS_FOLDER_PREFIX, USERS_TOPIC, USER_CONNECTIONS_FOLDER_PREFIX, \
    USER_CONNECTIONS_TOPIC, BOOTSTRAP_SERVERS

LOGGER = logging.getLogger(__name__)

s3 = boto3.resource('s3')
bucket = s3.Bucket(S3_BUCKET_NAME)


def _produce(topic, input_prefix):
    producer = KafkaProducer(bootstrap_servers=BOOTSTRAP_SERVERS, security_protocol="SSL")

    def on_send_success(record_metadata):
        print(f'Success: {record_metadata.topic}, {record_metadata.partition}, {record_metadata.offset}')

    def on_send_error(excp):
        LOGGER.error('Error', exc_info=excp)

    existing_objects = set()

    while True:
        for object_summary in bucket.objects.filter(Prefix=input_prefix):
            if object_summary.key in existing_objects:
                continue

            for line in object_summary.get()['Body'].iter_lines():
                future = producer.send(topic, line).add_callback(on_send_success).add_errback(on_send_error)
                future.get(timeout=10)

            producer.flush()  # block until all async messages are sent

            existing_objects.add(object_summary.key)

        time.sleep(5)


def produce_users():
    _produce(topic=USERS_TOPIC, input_prefix=USERS_FOLDER_PREFIX)


def produce_user_connections():
    _produce(topic=USER_CONNECTIONS_TOPIC, input_prefix=USER_CONNECTIONS_FOLDER_PREFIX)


if __name__ == '__main__':
    t1 = Thread(target=produce_users)
    t2 = Thread(target=produce_user_connections)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
