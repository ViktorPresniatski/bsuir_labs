from threading import Thread

import boto3
import logging

from kafka import KafkaConsumer

from constants import USERS_TOPIC, USER_CONNECTIONS_TOPIC, BOOTSTRAP_SERVERS

LOGGER = logging.getLogger(__name__)

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('Users')


def _get_or_update(table, data):
    response = table.get_item(Key={'user_id': data['user_id']})
    if 'Item' in response:
        item = response['Item']
        item = {**item, **data}
    else:
        item = data

    table.put_item(Item=item)


def consume_users():
    consumer = KafkaConsumer(USERS_TOPIC, bootstrap_servers=BOOTSTRAP_SERVERS, security_protocol="SSL")

    for message in consumer:
        if not message.value:
            continue

        print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                             message.offset, message.key,
                                             message.value))

        line = message.value.decode()
        line = line.split(',', maxsplit=10)
        user_data = {
            'user_id': int(line[0]),
            'wall_comments_count': int(line[1]),
            'mentions_received': int(line[2]),
            'shared_posts_recevied': int(line[3]),
            'mentions_sent': int(line[4]),
            'shared_posts_sent': int(line[5]),
            'posts': int(line[6]),
            'photos_count': int(line[7]),
            'audio_count': int(line[8]),
        }
        _get_or_update(table, user_data)


def consumer_user_connections():
    consumer = KafkaConsumer(USER_CONNECTIONS_TOPIC, bootstrap_servers=BOOTSTRAP_SERVERS, security_protocol="SSL")

    for message in consumer:
        if not message.value:
            continue

        print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                             message.offset, message.key,
                                             message.value))

        line = message.value.decode()

        line = line.split('\t', maxsplit=2)
        user_data = {
            'user_id': int(line[0]),
            'friends_count': int(line[1]),
        }
        _get_or_update(table, user_data)


if __name__ == '__main__':
    t1 = Thread(target=consume_users)
    t2 = Thread(target=consumer_user_connections)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
