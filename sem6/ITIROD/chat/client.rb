require 'dbi'
require 'socket'
require_relative 'gui'


class Client
  attr_reader :socket

  def initialize(host, port, dbh, username)
    @host = host
    @port = port
    @dbh = dbh
    @username = username
  end

  def start
    socket = UDPSocket.new
    socket.connect(@host, @port)
    window = Gui.new

    window.on_send do |message|
      socket.send("#{message}%#{@username}", 0)
    end

    window.on_destroy do
      socket.send("$disconnected%#{@username}", 0)
      socket.close
    end

    thread = Thread.new do
      loop do
        message_id, sender = socket.recvfrom(1024)
        message = select_message(message_id.to_i)
        window.write(message)
      end
    end
    socket.send("$connected%#{@username}", 0)
    Gtk.main
  end

  def select_message(id)
    row = @dbh.select_one("SELECT * FROM messages WHERE id = '#{id}'")
    username = @dbh.select_one("SELECT name FROM users WHERE id = '#{row[2].to_i}'")[0]
    "#{row[3].strftime("%T")} - #{username}: #{row[1]}"
  end
end


if __FILE__ == $0
  dbh = DBI.connect("dbi:OCI8:", "eugene", "eugene")
  print "Enter username: "
  username = gets.chomp
  row = dbh.select_one("SELECT name FROM users WHERE name = '#{username}'")
  while username.empty? || !row.nil?
    puts username.empty? ? "Username must be present" : "Username already exists"
    print "Enter username: "
    username = gets.chomp
    row = dbh.select_one("SELECT name FROM users WHERE name = '#{username}'")
  end
  client = Client.new('localhost', 4321, dbh, username)
  client.start
end