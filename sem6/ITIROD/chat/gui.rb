require "gtk3"


class Gui < Gtk::Window
  def initialize
    super

    init_ui
  end

  def on_send(&block)
    @send_btn.signal_connect "clicked" do
      block.call(@input.text)
      @input.set_text ""
    end
  end

  def on_destroy(&block)
    signal_connect "destroy" do
      block.call
      Gtk.main_quit 
    end   
  end
  
  def init_ui
    set_border_width 10
    
    vbox = Gtk::Box.new :vertical, 0
    hbox = Gtk::Box.new :horizontal, 20
    
    init_label(vbox)
    init_input(hbox)
    init_button(hbox)
    
    halign = Gtk::Alignment.new 0, 0, 0, 0
    halign.add hbox
    
    vbox.pack_start halign, :expand => false, 
      :fill => false, :padding => 1

    add vbox
    set_title "Chat"
    
    set_default_size 0, 100
    set_window_position :center
    
    show_all        
  end

  def write(message)
    text = "#{@label.text}#{message}\n" 
    @label.set_text text
  end

  private

  def init_label(box)
    @label = Gtk::Label.new ""
    e_space = Gtk::Alignment.new 0, 0, 0, 0
    e_space.add @label
    box.pack_start e_space, :expand => true
  end

  def init_input(box)
    @input = Gtk::Entry.new
    @input.set_size_request 400, 30
    box.add @input
  end

  def init_button(box)
    @send_btn = Gtk::Button.new :label => "Send"
    @send_btn.set_size_request 100, 30
    box.add @send_btn
  end
end