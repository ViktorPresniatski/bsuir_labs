require 'socket'
require 'dbi'


class UDPServer
  attr_reader :socket

  def initialize(host, port)
    @host = host
    @port = port
    @clients = []
    @dbh = DBI.connect("dbi:OCI8:", "eugene", "eugene")
  end

  def start
    @socket = UDPSocket.new
    @socket.bind(@host, @port)
  
    loop do
      message, sender = @socket.recvfrom(1024)
      message, username = message.split('%')
      @clients.delete(sender[1]) if message.start_with?("$disconnected")


      if message.start_with?("$connected")
        @clients << sender[1]
        save_user(username)
      end

      message_id = save_message(username, message)
      @clients.each { |cl| @socket.send(message_id, 0, @host, cl) }
      p message_id, sender
    end
  end

  def save_message(sender, message)
    row = @dbh.select_one("SELECT id FROM users WHERE NAME = '#{sender}'")
    sth = @dbh.prepare("INSERT INTO messages VALUES (NULL, ?, ?, CURRENT_TIMESTAMP)")
    sth.execute(message, row[0].to_i)
    sth.finish
    @dbh.commit
    @dbh.select_one("SELECT max(id) FROM messages")[0].to_i.to_s
  end

  def save_user(name)
    sth = @dbh.prepare("INSERT INTO users VALUES (NULL, ?, CURRENT_TIMESTAMP)")
    sth.execute(name)
    sth.finish
    @dbh.commit
  end
end
  

if __FILE__ == $0
  server = UDPServer.new('localhost', 4321)
  server.start
end