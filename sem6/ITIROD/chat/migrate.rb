require 'dbi'

dbh = DBI.connect("dbi:OCI8:", "eugene", "eugene")

dbh.do("DROP TABLE USERS")
dbh.do("DROP SEQUENCE users_id_seq")
# dbh.do("DROP TRIGGER id_increment")

dbh.do('CREATE TABLE USERS (ID NUMBER, NAME VARCHAR2(10), CREATED_AT TIMESTAMP )')
dbh.do("CREATE SEQUENCE users_id_seq START WITH 1")
dbh.do("CREATE OR REPLACE TRIGGER id_increment 
		BEFORE INSERT ON USERS 
		FOR EACH ROW
		BEGIN
		  IF :NEW.ID IS NULL THEN
		    :NEW.id := users_id_seq.NEXTVAL;
		  END IF;
		END;
	  ")

dbh.do("DROP TABLE MESSAGES")
dbh.do("DROP SEQUENCE messages_id_seq")
# dbh.do("DROP TRIGGER mesages.mess_id_increment")

dbh.do("CREATE TABLE MESSAGES (ID NUMBER, BODY VARCHAR2(100), user_id NUMBER, CREATED_AT TIMESTAMP )")
dbh.do("CREATE SEQUENCE messages_id_seq START WITH 1")
dbh.do("CREATE OR REPLACE TRIGGER mes_id_increment 
		BEFORE INSERT ON MESSAGES 
		FOR EACH ROW
		BEGIN
		  IF :NEW.ID IS NULL THEN
		    :NEW.id := messages_id_seq.NEXTVAL;
		  END IF;
		END;")

dbh.do("DROP TABLE USER_MESSAGES_LOGS")
dbh.do("DROP SEQUENCE user_messages_logs_id_seq")
# dbh.do("DROP TRIGGER mesages.mess_id_increment")

dbh.do("CREATE TABLE USER_MESSAGES_LOGS (ID NUMBER, message_id NUMBER, user_id NUMBER)")
dbh.do("CREATE SEQUENCE user_messages_logs_id_seq START WITH 1")
dbh.do("CREATE OR REPLACE TRIGGER mes_log_id_increment 
		BEFORE INSERT ON USER_MESSAGES_LOGS 
		FOR EACH ROW
		BEGIN
		  IF :NEW.ID IS NULL THEN
		    :NEW.id := user_messages_logs_id_seq.NEXTVAL;
		  END IF;
		END;")

dbh.do("CREATE OR REPLACE TRIGGER message_logs 
		AFTER INSERT ON MESSAGES
		FOR EACH ROW
		BEGIN
		  INSERT INTO USER_MESSAGES_LOGS VALUES (NULL, :NEW.id, :NEW.user_id);
		END;")


dbh.disconnect