require 'socket'
require_relative './utils/process'

server = TCPServer.new(1234)
begin
  while true
    Thread.start(server.accept) do |connection|
      puts "[Accepted] - #{Time.now} - #{connection.peeraddr}"
      response = Processing.call(connection)
      connection.puts response
      connection.close
    end
  end
rescue Errno::ECONNRESET, Errno::EPIPE => e
  puts e.message
end   
