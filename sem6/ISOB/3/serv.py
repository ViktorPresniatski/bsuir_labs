import socket
import sys
import threading

def multi_work(conn, addr):
    try:
        data = conn.recv(255)
        if data:
            conn.sendall(data)
    finally:
        conn.close()

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_adress = ('localhost', 5555)
sock.bind(server_adress)
sock.listen(100)
try:
    while True:
        conn, client_addr = sock.accept()
        print("Set con with {}".format(client_addr))
        t = threading.Thread(target=multi_work, args=(conn, client_addr))
        t.start()
finally:
    sock.close();


