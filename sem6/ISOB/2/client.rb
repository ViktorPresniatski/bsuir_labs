require_relative 'utils'
require_relative 'AS_server'
require_relative '../des'

NAME = "CLIENT"
CID = 2
ID_S = 1

key = ASserver.registr(CID)

puts "#{NAME} request AS\n\n"
puts "#{'+'*100}\n\n"

ans = ASserver.request(CID)

puts "#{NAME} gets by AS "
p ans

tgs = Des.decrypt(key, ans[0])
puts "\n#{NAME} decrypts TGS-Kas_tgs by Kc_as - #{tgs}\n\n"

kc_tgs = Des.decrypt(key, ans[1])
kc_tgs = kc_tgs[0..55]
print "#{NAME} decrypt Kc_tgs by Kc_as - #{kc_tgs.to_i(2)}\n\n"

puts "#{'+'*100}\n\n"

req = []
req << tgs

string = CID.to_s + ','
string += Time.now.to_i.to_s + ','
puts "#{NAME} generates aut - #{string}\n\n"
aut = string.unpack("B*").first

aut = Des.encrypt(kc_tgs, aut)
puts "#{NAME} encrypts aut by Kc_tgs - #{aut}\n\n"
req << aut

req <<  ID_S
puts "#{NAME} requests TGS:"
p req

puts "\n\n#{'+'*100}\n\n"
ans = TGSserver.request(req)
puts "#{NAME} gets by TGS_SERVER:"
puts "#{ans}\n\n"

unless ans
  puts "Access denied"
  exit(0)
end

tgs = Des.decrypt(kc_tgs, ans[0])
puts "#{NAME} decrypts TGS-Ktgs_ss by Kc_tgs - #{tgs}\n\n"
kc_ss = Des.decrypt(kc_tgs, ans[1])[0..55]
puts "#{NAME} decrypts Kc_ss by Kc_tgs - #{kc_ss.to_i(2)}\n\n"

tt = Time.now.to_i
string = CID.to_s + ","
string += tt.to_s + ","
aut = string.unpack("B*").first
aut = Des.encrypt(kc_ss, aut)
puts "#{NAME} encrypts aut by Kc_ss - #{aut}\n\n"
req = []
req << tgs
req << aut
puts "#{NAME} requests SERVER"
p req
puts "\n\n#{'+'*100}\n\n"

ans = Server.request(req)
puts "#{NAME} gets by SERVER:"
p ans

ans = Des.decrypt(kc_ss, ans)
puts "\n\n#{NAME} decrypt t4 + 1 by Kc_ss - #{ans}\n\n"

ttt, *_ = *[ans].pack("B*").split(',').map(&:to_i)
if tt + 1 == ttt
  puts "#{tt}, #{ttt}"
  puts "OK"
else
  puts "ERROR"
end