require_relative '../des'


class Server
  @tgs_ss = 0
  NAME = "SERVER"

  class << self
    attr_accessor :tgs_ss
  end

  def self.request(req)
    tgt = req[0]
    aut = req[1]
    puts "#{NAME} gets "
    p req

    tgs = Des.decrypt(tgs_ss, tgt)
    puts "\n\n#{NAME} decrypts tgs by Ktgs_ss - #{tgs}\n\n"
    
    c1, ss, t3, p2, kc_ss, *_ = *[tgs].pack("B*").split(',').map(&:to_i)
    puts "#{NAME} Kc_ss - #{kc_ss}\n\n"
    
    kc_ss = to_bit(kc_ss)
    aut = Des.decrypt(kc_ss, aut)
    c2, t4 , *_ = *[aut].pack("B*").split(',').map(&:to_i)
    puts "#{NAME} decrypts aut by Kc_ss - %#{aut}\n\n"
    
    if c1 == c2 and t4 - t3 < p2
      puts "#{NAME} c1==c2 and t4 - t3 < p2 - TRUE\n\n"
      t4 = t4 + 1
      string = t4.to_s + ","
      t = string.unpack("B*").first
      puts "#{NAME} enrypts t4 + 1 by Kc_ss - #{t}\n\n"
      t = Des.encrypt(kc_ss, t)

      puts "#{NAME} send"
      p t
      puts "\n\n#{'+'*100}\n\n"
      return t
    end

    nil
  end
end