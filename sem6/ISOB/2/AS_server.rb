require_relative '../des'
require_relative 'TGS_server'

class ASserver
  @clients_keys = {}
  NAME = "AS_SERVER"
  KAS_TGS = to_bit(rand(1 << 56))
  TGSserver.kas_tgs = KAS_TGS 

  def self.registr(cid)
    @clients_keys[cid] = to_bit(Des.get_key) unless @clients_keys.include?(cid)
    return @clients_keys[cid]
  end

  def self.get_TGT(c, tgs, kc_tgs)
    ans = []
    ans << c.to_s
    ans << tgs.to_s
    ans << Time.now.to_i.to_s
    ans << 360.to_s
    ans << kc_tgs.to_s
    ans.join(',') + ','
  end

  def self.request(cid)
    puts "#{NAME} gets #{cid}\n\n"
    registr(cid)

    kc_tgs = Des.get_key
    puts "#{NAME} generates KC_TGS - #{kc_tgs}\n\n"

    tgt = get_TGT(cid, 1, kc_tgs)
    puts "#{NAME} creates TGT - #{tgt}\n\n"

    tgt = tgt.unpack("B*").first

    result = []

    tgs_1 = Des.encrypt(KAS_TGS, tgt)
    puts "#{NAME} encrypts TGT by Kas_tgs - #{tgs_1}\n\n"

    tgs_2 = Des.encrypt(@clients_keys[cid], tgs_1)
    puts "#{NAME} encrypts TGT by Kc_tgs - #{tgs_2}\n\n"

    result << tgs_2

    kc_tgs = to_bit(kc_tgs)

    kc_tgs_1 = Des.encrypt(@clients_keys[cid], kc_tgs)
    puts "#{NAME} encrypts Kc_TGS by Kc_as - #{kc_tgs_1}\n\n"
    result << kc_tgs_1

    puts "#{NAME} send"
    p result
    puts "\n\n#{'+'*100}\n\n"
    result
  end

end