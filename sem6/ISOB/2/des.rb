class Des
  class << self
    IP = [58, 50, 42, 34, 26, 18, 10, 2,
        60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6,
        64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9,  1,
        59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5,
        63, 55, 47, 39, 31, 23, 15, 7]

    E = [32, 1,  2,  3,  4,  5,  4,  5,  6,  7,  8,  9,
         8,  9,  10, 11, 12, 13, 12, 13, 14, 15, 16, 17,
         16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25,
         24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1]

    S = [[14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
         [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
         [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
         [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
        ]

    P = [16, 7,  20, 21, 29, 12, 28, 17,
         1,  15, 23, 26, 5,  18, 31, 10,
         2,  8,  24, 14, 32, 27, 3,  9,
         19, 13, 30, 6,  22, 11, 4,  25]

    IP_1 = [40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41, 9,  49, 17, 57, 25]

    KEY_P = [57, 49, 41, 33, 25, 17, 9,
             1,  58, 50, 42, 34, 26, 18,
             10, 2,  59, 51, 43, 35, 27,
             19, 11, 3,  60, 52, 44, 36,
             63, 55, 47, 39, 31, 23, 15,
             7,  62, 54, 46, 38, 30, 22,
             14, 6,  61, 53, 45, 37, 29,
             21, 13, 5,  28, 20, 12, 4]

    KEY_END_P = [14, 17, 11, 24, 1,  5,  3,  28,
                 15, 6,  21, 10, 23, 19, 12, 4,
                 26, 8,  16, 7,  27, 20, 13, 2,
                 41, 52, 31, 37, 47, 55, 30, 40,
                 51, 45, 33, 48, 44, 49, 39, 56,
                 34, 53, 46, 42, 50, 36, 29, 32]

    SHIFT = [1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1]
    SHIFT2 =[0,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1]

    C0 = '0' * 28
    D0 = '0' * 28

    def shift_right(value, count)
      arr = value.split('')
      count.times { arr.unshift('0'); arr.pop }
      arr.join
    end

    def shift_left(value, count)
      arr = value.split('')
      count.times { arr.shift; arr.push('0') }
      arr.join
    end
    
    def make_c0_d0(key)
      for i in 1.upto(8)
        n = i * 8
        ch = key[n-8, n-1].split('').select { |c| c == '1'}.count % 2 == 0 ? '1' : '0'
        key.insert(n, ch)
      end
      for i in (0..27)
        C0[i] = key[KEY_P[i] - 1]
      end
      for i in (28..55)
        D0[i - 28] = key[KEY_P[i] - 1]
      end
    end

    def make_key(round, type)
      if type
        shift_left(C0, SHIFT[round - 1])
        shift_left(D0, SHIFT[round - 1])
      else
        shift_right(C0, SHIFT2[round - 1])
        shift_right(D0, SHIFT2[round - 1])
      end
      new_key = '0' * 48
      for i in (0..47)
        temp = KEY_END_P[i] - 1
        if temp < 28
          new_key[i] = C0[temp]
        else
          new_key[i] = D0[temp - 28]
        end
      end
      new_key
    end

    def start_reshafle(data)
      new_data = '0' * 64
      data = check_bit(data, 64)
      for i in (0..63)
        new_data[i] = data[IP[i] - 1]
      end
      new_data
    end

    def end_reshafle(data)
      new_data = '0' * 64
      data = check_bit(data, 64)
      for i in (0..63)
        new_data[i] = data[IP_1[i] - 1]
      end
      new_data
    end

    def expansion_E(data)
      new_data = '0' * 48
      data = check_bit(data, 32)
      for i in (0..47)
        new_data[i] = data[E[i] - 1]
      end
      new_data
    end

    def expansion_S(data)
      i = '0' * 2
      j = '0' * 4
      new_data = ""
      data = check_bit(data, 48)
      for jj in (0..7)
        slice = data[jj*6..jj*6 + 5]
        i[0] = slice[0]
        i[1] = slice[5]
        j = slice[1..4]
        new_data += to_bit(S[i.to_i(2)][j.to_i(2)], length = 4)
      end
      new_data
    end

    def expansion_P(data)
      new_data = '0' * 32
      data = check_bit(data, 32)
      for i in (0..31)
        new_data[i] = data[P[i] - 1]
      end
      new_data
    end

    def feistel(r, key)
      data = expansion_E(r)
      data = (data.to_i(2) ^ key.to_i(2)).to_s(2)
      data = expansion_S(data)
      data = expansion_P(data)
      data
    end

    def des(key, data)
      make_c0_d0(key)
      dt = start_reshafle(data)
      l = dt[0..31]
      r = dt[32..-1]
      for i in 1.upto(16)
        temp = l
        l = r
        r = (temp.to_i(2) ^ feistel(r, make_key(i, true)).to_i(2)).to_s(2)
      end

      dt = l + r
      end_reshafle(dt)
    end

    def in_des(key, data)
      make_c0_d0(key)
      dt = start_reshafle(data)
      l = dt[0..31]
      r = dt[32..-1]
      for i in 16.downto(1)
        temp = r
        r = l
        l = (temp.to_i(2) ^ feistel(l, make_key(i, false)).to_i(2)).to_s(2)
      end
      dt = l + r
      end_reshafle(dt)
    end

    def encrypt(key, data)
      i = 0;
      ans = ""
      while i < data.size
        ans += des(key, data[i..[i + 64, data.size].min - 1])
        i += 64
      end
      ans
    end

    def decrypt(key, data)
      i = 0;
      ans = ""
      while i < data.size
        ans += in_des(key, data[i..[i + 64, data.size].min - 1])
        i += 64
      end
      ans
    end


    def get_key
      rand(1<<56)
    end
  end
end