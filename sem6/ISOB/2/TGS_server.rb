require_relative '../des'
require_relative 'server'


class TGSserver
  @kas_tgs = 0
  NAME = "TGS_server"
  TGS_SS = to_bit(rand(1 << 56))
  Server.tgs_ss = TGS_SS

  class << self
    attr_accessor :kas_tgs
  end

  def self.get_TGS(c, ss, kc_ss)
    ans = []
    ans << c.to_s
    ans << ss.to_s
    ans << Time.now.to_i.to_s
    ans << 360.to_s
    ans << kc_ss.to_s
    ans.join(',') + ','
  end

  def self.request(req)
    tgt = req[0]
    aut = req[1]
    id = req[2]
    puts "#{NAME} gets "
    p req

    tgt = Des.decrypt(kas_tgs, tgt)
    puts "\n\n#{NAME} decrypts TGT by Kas_tgs - #{tgt}\n\n"
    c1, tgs, t1, p1, kc_tgs, *_ = *[tgt].pack("B*").split(',').map(&:to_i)
    puts "#{NAME} Kc_tgs - #{kc_tgs}\n\n"
    kc_tgs = to_bit(kc_tgs)

    aut = Des.decrypt(kas_tgs, aut)
    c2, t2 , *_= *[aut].pack("B*").split(',').map(&:to_i)
    puts "#{NAME} decrypts aut by Kc_tgs - #{aut}\n\n"

    if c1 == c2 && t2 - t1 < p1
      puts "#{NAME} c1==c2 and t2 - t1 < p1 - TRUE\n\n"
      
      kc_ss = Des.get_key
      puts "#{NAME} generates Kc_ss - #{kc_ss}\n\n"

      tgs = get_TGS(c2, id, kc_ss)
      puts ("#{NAME} creates tgs - #{tgs}\n\n")

      tgs = tgs.unpack("B*").first
      tgs = Des.encrypt(TGS_SS, tgs)
      puts ("#{NAME} encrypts TGS by Ktgs_ss - #{tgs}\n\n")

      tgs = Des.encrypt(kc_tgs, tgs)
      puts "#{NAME} encrypts TGS-Ktgs_ss by Kc_tgs - #{tgs}\n\n"

      req = []
      req << tgs
      kc_ss = to_bit(kc_ss)
      kc_ss = Des.encrypt(kc_tgs, kc_ss)
      puts ("#{NAME} encrypts Kc_ss by Kc_tgs - #{kc_ss}\n\n")
      req << kc_ss

      puts "#{NAME} send:"
      p req
      puts "\n\n#{'+'*100}\n\n"
      return req
    end

    nil
  end

end