require_relative 'cezar'


class Vigenere
  @letters = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
  @n = @letters.size

  def self.perform(text, key)
    answer = []
    k_size = key.size
    t_size = text.size
    if k_size < t_size
      m = t_size / k_size
      key *= m + 1
    end
    text.split('').each_with_index do |ch, i|
      key_for_cezar = @letters.index(key[i].upcase)
      answer.push(yield ch, key_for_cezar)
    end
    answer.join
  end

  def self.encrypt(text, key)
    perform(text, key) { |ch, key| Cezar.encrypt(ch, key) }
  end

  def self.decrypt(text, key)
    perform(text, key) { |ch, key| Cezar.decrypt(ch, key) }
  end
end


if __FILE__ == $0
  file_name = ARGV[0]
  key = ARGV[1]
  text = File.open(file_name).read
  
  puts "Input: " + text
  puts "Key: #{key}"
  
  enc = Vigenere.encrypt(text, key)
  f = File.open('encrypt.txt', "w")
  puts "Encryption: " + enc
  f.write(enc)
  f.close

  enc_text = File.open('encrypt.txt').read
  dec = Vigenere.decrypt(enc_text, key)
  f = File.open('decrypt.txt', "w")
  puts "Deccryption: " + dec
  f.write(dec)
  f.close
end