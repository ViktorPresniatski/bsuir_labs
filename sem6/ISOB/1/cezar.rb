class Cezar
  @letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  @n = @letters.size

  def self.perform(text)
    text.split('').map do |ch|
      is_upper = ch == ch.upcase
      x = @letters.index(ch.upcase)
      x ? [(yield x), is_upper, false] : [0, false, ch]
    end.map do |y, is_upper, other|
      other ? other : @letters[y].send(is_upper ? :upcase : :downcase) 
    end.join
  end

  def self.encrypt(text, key)
    perform(text) { |x| (x + key) % @n }
  end

  def self.decrypt(text, key)
    perform(text) { |x| (x - key + @n) % @n }
  end
end

if __FILE__ == $0
  file_name = ARGV[0]
  key = ARGV[1].to_i
  text = File.open(file_name).read

  puts "Input: " + text
  puts "Key: #{key}"
  
  enc = Cezar.encrypt(text, key)
  f = File.open('encrypt.txt', "w")
  puts "Encryption: " + enc
  f.write(enc)
  f.close

  enc_text = File.open('encrypt.txt').read
  dec = Cezar.decrypt(enc_text, key)
  f = File.open('decrypt.txt', "w")
  puts "Deccryption: " + dec
  f.write(dec)
  f.close
end