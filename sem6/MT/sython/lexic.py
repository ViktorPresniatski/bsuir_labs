#!/usr/bin/python2.7

import re
from collections import OrderedDict


IGNORE = OrderedDict([
    ('SPACE', r'[ \n\t]+'),
    ('COMMENTS', r'#[^\n]*'),
])


RESERVED = OrderedDict([
    ('LPAR', r'\('),
    ('RPAR', r'\)'),
    ('LSQB', r'\['),
    ('RSQB', r'\]'),
    ('COLON', r'\:'),
    ('COMMA', r','),
    ('SEMI', r';'),
    ('LESSEQUAL', r'<='),
    ('GREATEREQUAL', r'>='),
    ('PLUSEQUAL', r'\+='),
    ('MINEQUAL', r'-='),
    ('EQEQUAL', r'=='),
    ('NOTEQUAL', r'!='),
    ('LEFTSHIFT', r'<<'),
    ('RIGHTSHIFT', r'>>'),
    ('DOUBLESTAR', r'\*\*'),
    ('LESS', r'<'),
    ('GREATER', r'>'),
    ('EQUAL', r'='),
    ('PLUS', r'\+'),
    ('MINUS', r'-'),
    ('STAR', r'\*'),
    ('SLASH', r'/'),
    ('AMPER', r'&'),
    ('VBAR', r'\|'),
    ('DOT', r'\.'),
    ('PERCENT', r'%'),
    ('LBRACE', r'\{'),
    ('RBRACE', r'\}'),
    ('TILDE', r'~'),
    ('CIRCUMFLEX', r'\^'),
    ('FOR', r'for\b'),
    ('WHILE', r'while\b'),
    ('DEF', r'def\b'),
    ('IF', r'if\b'),
    ('ELIF', r'elif\b'),
    ('ELSE', r'else\b'),
    ('OR', r'or\b'),
    ('AND', r'and\b'),
    ('NOT', r'not\b'),
    ('IN', r'in\b'),
    ('RETURN', r'return\b'),
])


OTHER = OrderedDict([
    ('NAME', r'[_A-Za-z][_A-Za-z0-9]*'),
    ('FLOAT', r'[0-9]+\.[0-9]*'),
    ('INT', r'[0-9]+'),
    ('STRING', r'".*"\\|\'.*\''),
])


PATTERNS = list(IGNORE.items()) + list(RESERVED.items()) + list(OTHER.items())

def tokenize(code):
    line_start = 0
    line_count = 1
    pos = 0
    tokens = []
    errors = []
    while pos < len(code):
        if code[pos] == '\n':
            token = ('\n', 'NEWLINE', line_count, pos-line_start+1)
            tokens.append(token)
            pos += 1
            line_start = pos
            line_count += 1
            continue
        for name, pattern in PATTERNS:
            regex = re.compile(pattern)
            match = regex.match(code, pos)
            if match and name:
                if name not in IGNORE:
                    text = match.group(0)
                    token = (text, name, line_count, pos-line_start+1)
                    tokens.append(token)
                pos = match.end(0)
                break
        if not match:
            errors.append('Illegal character: \'{}\' in line {} position {}'.format(code[pos], line_count, pos-line_start+1))
            pos += 1
    return tokens, errors


if __name__ == '__main__':
    f = open('../python.py')
    code = f.read()
    tokens = tokenize(code)
    for t in tokens[0]:
        print(t)
    print
    for t in tokens[1]:
        print(t)