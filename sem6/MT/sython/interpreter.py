import lexic, parser, compiler#, Output

class SyObject:
    def __init__(self, dictionary):
        self.ref_count = 0
        self.sy_type = type(self)
        self.dictionary = dictionary
        self.dictionary['__dict__'] = dictionary
        self.dictionary['__class__'] = type(self)

    def inc_ref(self):
        self.ref_count += 1

    def dec_ref(self):
        self.ref_count -= 1

    def get_attribute(self, name):
        if '__getattr__' in self.dictionary:
            return self.dictionary['__getattr__'](name)
        elif name in self.dictionary:
            return self.dictionary[name]
        else:
            return SyException(
                'Error: Object <{}> has no attribute {}.'.format(
                    self.name, name
            ))

    def to_PyObject(self):
        if '__str__' in self.dictionary:
            return self.dictionary['__str__']().base_string
        else:
            return '<"{}" object>'.format(self.sy_type.name)


class SyType(SyObject):
    def __init__(self, name, bases, dictionary):
        super(SyType, self).__init__(dictionary)
        self.name = name
        self.bases = bases

    name = 'type'
    bases = ()
    dictionary = dict()
SyType.sy_type = SyType


class SyNoneType(SyType):
    def __init__(self):
        super(SyNoneType, self).__init__(
            'NoneType',
            tuple(), {
                '__str__': SyBuiltInFunction(lambda: SyString('None')),
                '__eq__': SyBuiltInFunction(lambda x: SyBool(isinstance(x, SyNoneType))),
            }
        )

    def to_PyObject(self):
        return None


class SyException(SyType):
    def __init__(self, message):
        super(SyException, self).__init__(
            'exception',
            tuple(), dict()
        )
        self.message = message


class SyBool(SyType):
    def __init__(self, base_bool):
        super(SyBool, self).__init__(
            'bool',
            tuple(), {
                '__str__': SyBuiltInFunction(lambda: SyString(str(self.base_bool))),
                '__nonzero__': SyBuiltInFunction(self.__nonzero__),
                '__eq__': SyBuiltInFunction(self.__eq__),
            }
        )
        self.base_bool = base_bool

    def __nonzero__(self):
        return self.base_bool.__nonzero__()

    def __eq__(self, right):
        return SyBool(self.base_bool == right)

    def to_PyObject(self):
        return self.base_bool


class SyList(SyType):
    def __init__(self, args):
        super(SyList, self).__init__(
            'list',
            tuple(), {
                '__str__': SyBuiltInFunction(self.__str__, 0),
                '__iter__': SyBuiltInFunction(lambda: SyBuiltInIter(iter(self.base_list))),
                '__getitem__': SyBuiltInFunction(self.__getitem__),
                '__setitem__': SyBuiltInFunction(self.__setitem__),
                'sort': SyBuiltInFunction(self.sort, 0),
                'append': SyBuiltInFunction(self.append, 1),
                'pop': SyBuiltInFunction(self.pop, 0),
            }
        )
        self.base_list = args

    def __getitem__(self, index):
        index = index.to_PyObject()
        try:
            return self.base_list[index]
        except:
            return SyException('Error: index {} out of range.'.format(index))

    def __setitem__(self, index, value):
        self.base_list[index.to_PyObject()] = value

    def append(self, item):
        self.base_list.append(item)

    def pop(self):
        return self.base_list.pop()

    def to_PyObject(self):
        return self.base_list

    def __str__(self):
        result = SyString('[')
        if len(self.base_list) == 0:
            return result + SyString(']')
        for sy_object in self.base_list[:-1]:
            result = result + sy_object.get_attribute('__str__')() + SyString(', ')
        result = result + self.base_list[-1].get_attribute('__str__')()
        return result + SyString(']')

    def sort(self):
        types = [type(el) for el in self.base_list]
        sy_list = [el.to_PyObject() for el in self.base_list]
        try:
            sy_list.sort()
            self.base_list = [t(e) for t, e in zip(types, sy_list)]
        except:
            return SyException('Error: Can\'t sort this list.')


class SyBuiltInIter(SyType):
    def __init__(self, base_iter):
        super(SyBuiltInIter, self).__init__(
            'BuiltInIterator',
            tuple(), {
                '__str__': SyBuiltInFunction(lambda x: SyString('Iter')),
                '__next__': SyBuiltInFunction(self.next),
            }
        )
        self.base_iter = base_iter

    def next(self):
        return next(self.base_iter)

    def to_PyObject(self):
        return self.base_iter


class SyString(SyType):
    def __init__(self, base_string):
        super(SyString, self).__init__(
            'string',
            tuple(), {
                '__add__': SyBuiltInFunction(self.__add__),
                '__str__': SyBuiltInFunction(lambda: self),
                '__eq__': SyBuiltInFunction(self.__eq),
            }
        )
        self.base_string = base_string

    def __add__(self, right):
        return SyString(self.base_string + right.base_string)

    def __eq(self, right):
        return SyBool(self.base_string == right.base_string)

    def to_PyObject(self):
        return self.base_string


class SyInt(SyType):
    def __init__(self, base_int):
        super(SyInt, self).__init__(
            'int',
            tuple(), {
                '__add__': SyBuiltInFunction(self.__add__),
                '__sub__': SyBuiltInFunction(self.__sub__),
                '__pow__': SyBuiltInFunction(self.__pow__),
                '__div__': SyBuiltInFunction(self.__div__),
                '__mod__': SyBuiltInFunction(self.__mod__),
                '__mul__': SyBuiltInFunction(self.__mul__),
                '__str__': SyBuiltInFunction(self.__str),
                '__eq__': SyBuiltInFunction(self.__eq),
                '__ne__': SyBuiltInFunction(self.__ne),
                '__cmp__': SyBuiltInFunction(self.__cmp),
            }
        )
        self.base_int = int(base_int)

    def __add__(self, right):
        return type(right)(self.base_int + right.to_PyObject())

    def __sub__(self, right):
        return type(right)(self.base_int - right.to_PyObject())

    def __mul__(self, right):
        return type(right)(self.base_int * right.to_PyObject())

    def __pow__(self, right):
        return type(right)(self.base_int ** right.to_PyObject())

    def __div__(self, right):
        return SyFloat(self.base_int / right.to_PyObject())

    def __mod__(self, right):
        return type(right)(self.base_int % right.to_PyObject())

    def __str(self):
        return SyString(str(self.base_int))

    def __eq(self, right):
        return SyBool(self.base_int == right.to_PyObject())

    def __ne(self, right):
        return SyBool(self.base_int != right.to_PyObject())

    def __cmp(self, other):
        other = other.to_PyObject()
        if self.base_int < other:
            return SyInt(-1)
        elif self.base_int > other:
            return SyInt(1)
        else:
            return SyInt(0)

    def to_PyObject(self):
        return self.base_int


class SyFloat(SyType):
    def __init__(self, base_float):
        super(SyFloat, self).__init__(
            'float',
            tuple(), {
                '__add__': SyBuiltInFunction(self.__add__),
                '__sub__': SyBuiltInFunction(self.__sub__),
                '__pow__': SyBuiltInFunction(self.__pow__),
                '__div__': SyBuiltInFunction(self.__div__),
                '__mod__': SyBuiltInFunction(self.__mod__),
                '__mul__': SyBuiltInFunction(self.__mul__),
                '__str__': SyBuiltInFunction(self.__str),
                '__eq__': SyBuiltInFunction(self.__eq),
                '__ne__': SyBuiltInFunction(self.__ne),
                '__cmp__': SyBuiltInFunction(self.__cmp),
            }
        )
        self.base_float = float(base_float)

    def __add__(self, right):
        return SyFloat(self.base_float + right.to_PyObject())

    def __sub__(self, right):
        return SyFloat(self.base_float - right.to_PyObject())

    def __mul__(self, right):
        return SyFloat(self.base_float * right.to_PyObject())

    def __pow__(self, right):
        return SyFloat(self.base_float ** right.to_PyObject())

    def __div__(self, right):
        return SyFloat(self.base_float / right.to_PyObject())

    def __mod__(self, right):
        return SyFloat(self.base_float % right.to_PyObject())

    def __str(self):
        return SyString(str(self.base_float))

    def __eq(self, right):
        return SyBool(self.base_float == right.to_PyObject())

    def __cmp(self, other):
        other = other.to_PyObject()
        if self.base_float < other:
            return SyInt(-1)
        elif self.base_float > other:
            return SyInt(1)
        else:
            return SyInt(0)

    def __ne(self, right):
        return SyBool(self.base_float != right.to_PyObject())

    def to_PyObject(self):
        return self.base_float


class SyBuiltInFunction(SyType):
    def __init__(self, base_function, arg_count=None):
        super(SyBuiltInFunction, self).__init__(
            'BuiltInFunction',
            tuple(), {
                '__call__': self
            }
        )
        self.base_function = base_function
        if arg_count is None:
            self.arg_count = base_function.__code__.co_argcount
        else:
            self.arg_count = arg_count
        self.name = base_function.__name__

    def __call__(self, *args, **kwargs):
        return self.base_function(*args, **kwargs)

    def to_PyObject(self):
        return self.__call__


class SyCode(SyType):
    def __init__(self, bytecode, args):
        super(SyCode, self).__init__(
            'code',
            tuple(), {
                '__args_count__': len(args),
            }
        )
        self.bytecode = bytecode
        self.arg_count = len(args)
        self.arg_names = args


class SyFunction(SyType):
    def __init__(self, name, sy_code, parent_frame):
        super(SyFunction, self).__init__(
            'function',
            tuple(), {
                '__call__': self,
                '__code__': sy_code,
                '__name__': name
            }
        )
        self.sy_code = sy_code
        self.arg_count = sy_code.arg_count
        self.parent_frame = parent_frame
        self.name = name.to_PyObject()
        self.call_frame = None

    def __call__(self, *args, **kwargs):
        namespace = dict(zip(self.sy_code.arg_names, args))
        sy_frame = SyFrame(
            self.sy_code.bytecode,
            self.name,
            namespace,
            self.parent_frame
        )
        sy_frame.call_frame = self.call_frame
        return sy_frame.execute()


class SyFrame(SyType):
    def __init__(self, bytecode, name, namespace, parent_frame=None):
        super(SyFrame, self).__init__(
            'frame',
            tuple(), {
                '__name__': SyString(name),
                '__getattr__': SyBuiltInFunction(self.__getattr),
            }
        )
        self.name = name
        self.namespace = { **namespace }
        self.parent_frame = parent_frame
        self.call_frame = None
        self.bytecode = bytecode
        self.length = len(bytecode)
        self.stack = list()
        self.ip = 0
        self.handlers = {
            'CALL_FUNCTION': call_function,
            'IMPORT_NAME': import_name,
            'LOAD_INT': load_int,
            'LOAD_FLOAT': load_float,
            'LOAD_STRING': load_string,
            'LOAD_NAME': load_name,
            'LOAD_CODE': load_code,
            'LOAD_ATTRIBUTE': load_attribute,
            'MAKE_FUNCTION': make_function,
            'STORE_NAME': store_name,
            'STORE_SUBSCRIPT': store_subscript,
            'GET_ITER': get_iter,
            'FOR_ITER': for_iter,
            'BUILD_LIST': build_list,
            'JUMP_IF_FALSE': jump_if_false,
            'JUMP': jump,
            'ADD': add,
            'SUBTRACT': subtract,
            'MULTIPLY': multiply,
            'DIVIDE': divide,
            'MODULO': modulo,
            'POWER': power,
            'SUBSCRIPT': subscript,
            'COMPARE': compare,
            'NEGATIVE': negative,
            'RETURN': ret,
            'POP': pop,
        }

    def __getattr(self, name):
        sy_object = self.namespace[name]
        if sy_object:
            return sy_object

    def init(self):
        self.ip = 0

    def end(self):
        return self.ip >= self.length

    def current_command(self):
        return self.bytecode[self.ip]

    def jump(self, step):
        self.ip += step

    def execute(self):
        self.init()
        while not self.end():
            command, argument = self.current_command()
            self.handlers[command](self, argument)
            self.check_exceptions()
        if hasattr(self, 'ret'):
            return self.ret
        else:
            return SyNoneType()

    def check_exceptions(self):
        if self.stack:
            if isinstance(self.stack[-1], SyException):
                self.emergency_stop()
                print('\t', self.stack[-1].message)

    def stop(self):
        self.ip = self.length

    def emergency_stop(self):
        self.stop()
        if self.call_frame:
            self.call_frame.emergency_stop()
        print('In {}:'.format(self.name))

    def push(self, value):
        self.stack.append(value)

    def pop(self):
        return self.stack.pop()


def execute(module_name, name = '__main__', call_frame=None):
    try:
        code = open('{}.py'.format(module_name), 'r').read()
    except:
        return SyException('Error: No module named {}.'.format(module_name))

    tokens, errors = lexic.tokenize(code)
    #print(tokens)
    if errors:
        return SyException('\n'.join(errors))

    ast = parser.parse(tokens)
    #Output.pretty(ast)
    #Output.ete3_show(ast)
    if isinstance(ast, str):
        return SyException(ast)

    bytecode = compiler.compile(ast)
    #Output.print_bytecode(bytecode)

    namespace = {
        '__name__': SyString(name),
        'None': SyNoneType(),
        'True': SyBool(True),
        'False': SyBool(False),
        'print': SyBuiltInFunction(sy_print, -1),
        'len': SyBuiltInFunction(sy_len, 1),
        'str': SyBuiltInFunction(lambda x: x.get_attribute('__str__')()),
        'range': SyBuiltInFunction(sy_range, -1),
    }
    sy_frame = SyFrame(bytecode, name, namespace)
    sy_frame.call_frame = call_frame
    sy_frame.execute()
    return sy_frame


def sy_print(*sy_objects):
    result = ''
    for sy_object in reversed(sy_objects):
        result += sy_object.get_attribute('__str__')().to_PyObject()
    print(result)


def sy_range(*args):
    args = [arg.base_int for arg in args]
    return SyList(list(map(SyInt, range(*args))))


def sy_len(sy_object):
    try:
        return SyInt(len(sy_object.to_PyObject()))
    except:
        return SyException('Error: object {} has no len.'.format(
            sy_object.get_attribute('__str__')().to_PyObject()
        ))


def call_function(sy_frame, argument):
    args = list(reversed([sy_frame.pop() for i in range(argument)]))
    sy_function = sy_frame.pop()
    sy_function.call_frame = sy_frame
    if len(args) == sy_function.arg_count or sy_function.arg_count == -1:
        sy_frame.push(sy_function(*args))
        sy_frame.jump(1)
    else:
        sy_frame.push(SyException(
            'Error: Function {} takes exactly {} argument(s), {} given.'.format(
                sy_function.name, sy_function.arg_count, len(args)
            )
        ))


def load_float(sy_frame, argument):
    sy_float = SyFloat(float(argument))
    sy_frame.push(sy_float)
    sy_frame.jump(1)


def load_int(sy_frame, argument):
    sy_int = SyInt(int(argument))
    sy_frame.push(sy_int)
    sy_frame.jump(1)


def load_string(sy_frame, argument):
    if argument[0] == '\'':
        argument = argument[1:-1]
    sy_string = SyString(bytes(argument, "utf-8").decode("unicode_escape"))
    sy_frame.push(sy_string)
    sy_frame.jump(1)


def load_name(sy_frame, argument):
    frame = sy_frame
    while argument not in frame.namespace and frame.parent_frame != None:
        frame = frame.parent_frame
    if argument not in frame.namespace:
        sy_frame.push(SyException('Error: No object named {}.'.format(argument)))
    else:
        sy_object = frame.namespace[argument]
        sy_frame.push(sy_object)
        sy_frame.jump(1)


def import_name(sy_frame, argument):
    sy_frame.push(execute(argument, argument, sy_frame))
    sy_frame.jump(1)


def get_iter(sy_frame, argument):
    sy_object = sy_frame.pop()
    sy_iter = sy_object.get_attribute('__iter__')()
    sy_frame.push(sy_iter)
    sy_frame.jump(1)


def for_iter(sy_frame, argument):
    sy_iter = sy_frame.pop()
    try:
        sy_frame.push(sy_iter)
        sy_frame.push(sy_iter.get_attribute('__next__')())
        sy_frame.jump(1)
    except:
        sy_frame.jump(argument)


def load_code(sy_frame, argument):
    sy_code = SyCode(argument['code'], argument['args'])
    sy_frame.push(sy_code)
    sy_frame.jump(1)


def load_attribute(sy_frame, argument):
    sy_object = sy_frame.pop()
    sy_frame.push(sy_object.get_attribute(argument))
    sy_frame.jump(1)


def make_function(sy_frame, argument):
    name = sy_frame.pop()
    sy_code = sy_frame.pop()
    sy_frame.push(SyFunction(name, sy_code, sy_frame))
    sy_frame.jump(1)


def store_name(sy_frame, argument):
    sy_object = sy_frame.pop()
    sy_frame.namespace[argument] = sy_object
    sy_frame.jump(1)


def store_subscript(sy_frame, argument):
    argument = sy_frame.pop()
    sy_object = sy_frame.pop()
    sy_value = sy_frame.pop()
    sy_object.dictionary['__setitem__'](argument, sy_value)
    sy_frame.jump(1)


def build_list(sy_frame, argument):
    args = list(reversed([sy_frame.pop() for i in range(argument)]))
    sy_frame.push(SyList(args))
    sy_frame.jump(1)


def subscript(sy_frame, argument):
    argument = sy_frame.pop()
    sy_object = sy_frame.pop()
    sy_frame.push(sy_object.dictionary['__getitem__'](argument))
    sy_frame.jump(1)


def jump_if_false(sy_frame, argument):
    sy_bool = sy_frame.pop()
    if not sy_bool.base_bool:
        sy_frame.jump(argument)
    else:
        sy_frame.jump(1)


def jump(sy_frame, argument):
    sy_frame.jump(argument)


def add(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    sy_object = left.dictionary['__add__'](right)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def subtract(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    sy_object = left.dictionary['__sub__'](right)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def multiply(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    sy_object = left.dictionary['__mul__'](right)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def divide(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    sy_object = left.dictionary['__div__'](right)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def modulo(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    sy_object = left.dictionary['__mod__'](right)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def power(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    sy_object = left.dictionary['__pow__'](right)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def negative(sy_frame, argument):
    sy_object = sy_frame.pop()
    sy_object = sy_object.dictionary['__mul__'](SyInt(-1))
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def compare(sy_frame, argument):
    right = sy_frame.pop()
    left = sy_frame.pop()
    if argument == '==':
        sy_object = left.get_attribute('__eq__')(right)
    elif argument == '!=':
        sy_object = left.get_attribute('__ne__')(right)
    elif argument == '>':
        sy_object = left.get_attribute('__cmp__')(right)
        sy_object = SyBool(sy_object.base_int == 1)
    elif argument == '>=':
        eq = left.get_attribute('__eq__')(right)
        grater = left.get_attribute('__cmp__')(right)
        grater = SyBool(grater.base_int == 1)
        sy_object = SyBool(eq.base_bool or grater.base_bool)
    elif argument == '<':
        sy_object = left.get_attribute('__cmp__')(right)
        sy_object = SyBool(sy_object.base_int == -1)
    elif argument == '<=':
        eq = left.get_attribute('__eq__')(right)
        less = left.get_attribute('__cmp__')(right)
        less = SyBool(less.base_int == -1)
        sy_object = SyBool(eq.base_bool or less.base_bool)
    sy_frame.push(sy_object)
    sy_frame.jump(1)


def ret(sy_frame, argument):
    sy_frame.ret = sy_frame.pop()
    sy_frame.stop()


def pop(sy_frame, argument):
    sy_frame.pop()
    sy_frame.jump(1)


if __name__ == '__main__':
    execute('../python')