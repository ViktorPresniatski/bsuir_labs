import lexic


def compile_node(node):
    handlers = {
        'block': compile_node,
        'statement': compile_node,
        'simple_statement': compile_node,
        'compound_statement': compile_node,
        'class_statement': compile_class_statement,
        'function_statement': compile_function_statement,
        'for_statement': compile_for_statement,
        'while_statement': compile_while_statement,
        'if_statement': compile_if_statement,
        'return_statement': compile_return_statement,
        'assignment_statement': compile_assignment_statement,
        'tail': compile_node,
        'attribute_loading': compile_attribute_loading,
        'function_call': compile_function_call,
        'subscription': compile_subscription,
        'import_statement': compile_import_statement,
        'expression_statement': compile_expression_statement,
        'expression': compile_or_test,
        'expression_list': compile_node,
        'and_test': compile_and_test,
        'not_test': compile_not_test,
        'comparison': compile_comparison,
        'comp_op': compile_comp_op,
        'arithmetic_expression': compile_arithmetic_expression,
        'term': compile_term,
        'factor': compile_factor,
        'power': compile_power,
        'atom_expression': compile_node,
        'list_creation': compile_list_creation,
        'brackets_expression': compile_brackets_expression,
        'atom': compile_atom,
        'EMPTY': ignore_node,
        'NEWLINE': ignore_node,
        'RETURN': ignore_node,
        'COMMA': ignore_node,
        'EQUAL': ignore_node
    }
    bytecode = []
    if isinstance(node, list):
        for sub_node in node:
            bytecode += compile_node(sub_node)
    else:
        node_name, node = tuple(node.items())[0]
        bytecode = handlers[node_name](node)
    return bytecode


def generate_command(name, argument):
    return [name, argument]


def ignore_node(node):
    return []


def open_node(node):
    return tuple(node.items())[0]


def compile(ast):
    return compile_node(ast)


def compile_function_statement(node):
    function_name = open_node(node[1])[1]
    function_arguments = get_arguments(node[3])
    function_code = {
        'args': function_arguments,
        'code': compile_node(node[7])
    }
    bytecode = [generate_command('LOAD_CODE', function_code)]
    bytecode.append(generate_command('LOAD_STRING', function_name))
    bytecode.append(generate_command('MAKE_FUNCTION', None))
    bytecode.append(generate_command('STORE_NAME', function_name))
    return bytecode


def compile_class_statement(node):
    bytecode = [generate_command('LOAD_BUILD_CLASS', None)]
    bases = []
    if isinstance(node[2], list):
        bases = get_arguments(node[2][1])
    class_code = {
        'args': bases,
        'code': compile_node(node[5])
    }
    bytecode += [generate_command('LOAD_CODE', class_code)]
    class_name = open_node(node[1])[1]
    bytecode += [generate_command('LOAD_STRING', class_name)]
    bytecode.append(generate_command('MAKE_FUNCTION', None))
    bytecode.append(generate_command('CALL_FUNCTION', 1))
    bytecode.append(generate_command('STORE_NAME', class_name))
    return bytecode


def compile_while_statement(node):
    bytecode = compile_node(node[1])
    while_block = compile_node(node[4])
    while_block.append(generate_command(
        'JUMP',
        -(len(while_block) + len(bytecode) + 1)
    ))
    bytecode.append(generate_command(
        'JUMP_IF_FALSE',
        len(while_block) + 1
    ))
    bytecode += while_block
    return bytecode


def compile_for_statement(node):
    for_arguments = get_arguments(node[1])
    for_expression = compile_node(node[3])
    for_expression.append(generate_command('GET_ITER', None))
    for_block = []
    if len(for_arguments) > 1:
        for_block.append(generate_command(
            'UNPACK_SEQUENCE',
            len(for_arguments)
        ))
    for argument in for_arguments:
        for_block.append(generate_command('STORE_NAME', argument))
    for_block += compile_node(node[6])
    for_block.append(generate_command(
        'JUMP',
        -(len(for_block) + len(for_arguments))
    ))
    for_expression.append(generate_command('FOR_ITER', len(for_block)+1))
    for_block.append(generate_command('POP', None))
    return for_expression + for_block



def get_arguments(node):
    arguments = []
    arguments_node = open_node(node)[1]
    if isinstance(arguments_node, list):
        arguments.append(open_node(arguments_node[0])[1])
        if isinstance(arguments_node[1], list):
            for argument in arguments_node[1]:
                arguments.append(open_node(argument[-1])[1])
    return arguments


def compile_if_statement(node):
    if_bytecode = compile_if(node)

    else_bytecode = []
    if isinstance(node[6], list):
        else_bytecode += compile_node(node[6][3])

    elif_blocks = []
    if isinstance(node[5], list):
        for elif_statement in node[5]:
            elif_blocks.append(compile_if(elif_statement))

    jump_len = len(else_bytecode)+1
    elif_bytecode = []
    for elif_block in reversed(elif_blocks):
        elif_block.append(generate_command('JUMP', jump_len))
        elif_bytecode = elif_block + elif_bytecode
        jump_len += len(elif_block)

    if_bytecode.append(generate_command('JUMP', jump_len))
    return if_bytecode + elif_bytecode + else_bytecode


def compile_if(node):
    if_test = compile_node(node[1])
    if_block = compile_node(node[4])
    jump_command = [generate_command('JUMP_IF_FALSE', len(if_block)+2)]
    return if_test + jump_command + if_block


def compile_return_statement(node):
    bytecode = compile_node(node[1])
    bytecode.append(generate_command('RETURN', None))
    return bytecode


def compile_import_statement(node):
    module_name = open_node(node[1])[1]
    bytecode = [generate_command('IMPORT_NAME', module_name)]
    bytecode.append(generate_command('STORE_NAME', module_name))
    return bytecode


def compile_expression_statement(node):
    bytecode = compile_node(node[0])
    bytecode.append(generate_command('POP', None))
    return bytecode


def compile_assignment_statement(node):
    bytecode = compile_node(node[2])
    bytecode.append(generate_command('LOAD_NAME', open_node(node[0])[1]))
    bytecode += compile_node(node[1])
    bytecode[-1][0] = {
        'LOAD_NAME': 'STORE_NAME',
        'LOAD_ATTRIBUTE': 'STORE_ATTRIBUTE',
        'SUBSCRIPT': 'STORE_SUBSCRIPT',
    }[bytecode[-1][0]]
    return bytecode


def compile_attribute_loading(node):
    return [generate_command('LOAD_ATTRIBUTE', open_node(node[1])[1])]


def compile_function_call(node):
    node_name, value = open_node(node[1])
    if isinstance(value, list):
        args_count = 1
        if isinstance(value[1], list):
            args_count += len(value[1])
    else:
        args_count = 0
    bytecode = compile_node(node[1])
    bytecode.append(generate_command('CALL_FUNCTION', args_count))
    return bytecode


def compile_subscription(node):
    bytecode = compile_node(node[1])
    bytecode.append(generate_command('SUBSCRIPT', None))
    return bytecode


def compile_or_test(node):
    bytecode = compile_node(node[0])
    if isinstance(node[1], list):
        and_bytecode = compile_node(node[1][1])
        bytecode.append(generate_command(
            'JUMP_IF_TRUE',
            len(and_bytecode)+1
        ))
        bytecode += and_bytecode
    return bytecode


def compile_and_test(node):
    bytecode = compile_node(node[0])
    if isinstance(node[1], list):
        not_bytecode = compile_node(node[1][1])
        bytecode.append(generate_command(
            'JUMP_IF_FALSE',
            len(not_bytecode)+1
        ))
        bytecode += not_bytecode
    return bytecode


def compile_not_test(node):
    if len(node) == 2:
        bytecode = compile_node(node[1])
        bytecode.append(generate_command('NOT', None))
    else:
        bytecode = compile_node(node)
    return bytecode


def compile_comparison(node):
    bytecode = compile_node(node[0])
    if isinstance(node[1], list):
        for comp_op, arithmetic_expression in node[1]:
            bytecode += compile_node(arithmetic_expression)
            bytecode += compile_node(comp_op)
    return bytecode


def compile_comp_op(node):
    name, value = open_node(node)
    return [generate_command('COMPARE', value)]


def compile_arithmetic_expression(node):
    bytecode = compile_node(node[0])
    if isinstance(node[1], list):
        for operation_node, term in node[1]:
            operation = {
                'PLUS': 'ADD',
                'MINUS': 'SUBTRACT'
            }[open_node(operation_node)[0]]
            bytecode += compile_node(term)
            bytecode.append(generate_command(operation, None))
    return bytecode


def compile_term(node):
    bytecode = compile_node(node[0])
    if isinstance(node[1], list):
        for operation_node, factor in node[1]:
            operation = {
                'STAR': 'MULTIPLY',
                'SLASH': 'DIVIDE',
                'PERCENT': 'MODULO'
            }[open_node(operation_node)[0]]
            bytecode += compile_node(factor)
            bytecode.append(generate_command(operation, None))
    return bytecode


def compile_factor(node):
    if len(node) == 2:
        bytecode = compile_node(node[1])
        if open_node(node[0])[0] == 'MINUS':
            bytecode.append(generate_command('NEGATIVE', None))
    else:
        bytecode = compile_node(node)
    return bytecode


def compile_power(node):
    bytecode = compile_node(node[0])
    if isinstance(node[1], list):
        bytecode += compile_node(node[1][1])
        bytecode.append(generate_command('POWER', None))
    return bytecode


def compile_atom_expression(node):
    return compile_node(node[0])


def compile_list_creation(node):
    node_name, value = open_node(node[1])
    if isinstance(value, list):
        args_count = 1
        if isinstance(value[1], list):
            args_count += len(value[1])
    else:
        args_count = 0
    bytecode = compile_node(node[1])
    bytecode.append(generate_command('BUILD_LIST', args_count))
    return bytecode


def compile_brackets_expression(node):
    return compile_node(node[1])


def compile_atom(node):
    name, value = open_node(node)
    return [generate_command('LOAD_'+name, value)]
