def repair_line(tokens, index):
  line_number = tokens[index][2]
  begin, end = index, index + 1
  line_tokens = []
  while (tokens[begin][2] == line_number and begin >= 0):
    line_tokens.insert(0, tokens[begin])
    begin -= 1
  while (tokens[end][2] == line_number and end < len(tokens)):
    line_tokens.append(tokens[end])
    end += 1
  words = []
  for i, token in enumerate(line_tokens):
      words.append(token[0])
  words[index - begin - 1] = '->({})'.format(words[index - begin - 1])

  return '    ' * (line_tokens[0][3]-1) + ' '.join(words)


def parse(tokens):
  node, index = block(tokens, 0, 0)
  if not node:
    error = 'Error: Invalid syntax in line {} at {}:\n{}'.format(
      tokens[index][2],
      tokens[index][3],
      repair_line(tokens, index)
    )
    return error
  return node


def statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'statement',
    ( simple_statement, compound_statement )
  )


def compound_statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'compound_statement',
    ( function_statement,
      if_statement,
      for_statement,
      while_statement,
      class_statement )
  )


def simple_statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'simple_statement',
    ( expression_statement,
      assignment_statement,
      import_statement,
      return_statement,
      'NEWLINE' )
  )


def optional_production(tokens, index, production):
  production = production['optional']
  node, new_index = check_production(tokens, index, production)
  if not node:
    return generate_node('EMPTY', None), index
  return node, new_index


def any_count_production(tokens, index, production):
  new_index = index
  nodes = []
  production = production['any_count']
  while(True):
    node, next_index = check_production(tokens, new_index, production)
    if not node:
      break
    new_index = next_index
    nodes.append(node)
  if not nodes:
    return generate_node('EMPTY', None), index
  return nodes, new_index


def any_production(tokens, index, productions):
  best_index = index
  new_index = index
  for production in productions:
    node, new_index = check_production(tokens, new_index, production)
    if node:
      return node, new_index
    elif new_index > best_index:
      best_index = new_index
    new_index = index
  return None, best_index


def all_productions(tokens, index, productions):
  new_index = index
  nodes = []
  for production in productions:
    node, new_index = check_production(tokens, new_index, production)
    if node:
      nodes.append(node)
    else:
      return None, new_index
  return nodes, new_index


def check_production(tokens, index, production):
  if isinstance(production, tuple):
    node, index = any_production(tokens, index, production)
  elif isinstance(production, list):
    node, index = all_productions(tokens, index, production)
  elif isinstance(production, dict):
    if 'any_count' in production:
      node, index = any_count_production(tokens, index, production)
    elif 'optional' in production:
      node, index = optional_production(tokens, index, production)
  elif callable(production):
    node, index = production(tokens, index)
  elif isinstance(production, str):
    node, index = check_terminal(tokens, index, production)
  return node, index


def generate_node(name, value):
  return {
    name: value
  }


def check_terminal(tokens, index, name):
  if tokens[index][1] == name:
    return generate_node(name, tokens[index][0]), index + 1
  return None, index


def check_nonterminal(tokens, index, name, production):
  node, new_index = check_production(
    tokens, index, production
  )
  if node:
    return generate_node(name, node), new_index
  return None, new_index


def function_statement(tokens, index):
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'function_statement',
    [ 'DEF', 'NAME', 'LPAR',
      { 'optional': arguments_list }, 'RPAR', 'COLON', 'NEWLINE',
      lambda x, y: block(x, y, depth) ]
  )


def if_statement(tokens, index):
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'if_statement',
    [ 'IF', expression, 'COLON', 'NEWLINE',
      lambda x, y: block(x, y, depth),
      { 'any_count': [
          'ELIF', expression, 'COLON', 'NEWLINE',
          lambda x, y: block(x, y, depth),
        ]},
      { 'optional': [
          'ELSE', 'COLON', 'NEWLINE',
          lambda x, y: block(x, y, depth),
        ]},
    ]
  )


def for_statement(tokens, index):
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'for_statement',
    [ 'FOR', arguments_list, 'IN', expression, 'COLON', 'NEWLINE',
      lambda x, y: block(x, y, depth) ]
  )


def while_statement(tokens, index):
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'while_statement',
    [ 'WHILE', expression, 'COLON', 'NEWLINE',
      lambda x, y: block(x, y, depth) ]
  )


def class_statement(tokens, index):
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'class_statement',
    [ 'CLASS', 'NAME',
      {'optional': [ 'LPAR', arguments_list, 'RPAR'] },
      'COLON', 'NEWLINE',
      lambda x, y: block(x, y, depth) ]
  )


def block(tokens, index, parent_depth=0):
  new_index = index
  nodes = []
  while tokens[new_index][1] == 'NEWLINE':
    new_index += 1
    if new_index>=len(tokens):
      return None, index
  depth = tokens[new_index][3]
  if depth <= parent_depth:
    return None, index
  while new_index < len(tokens) and (
    depth == tokens[new_index][3]
    or tokens[new_index][1]=='NEWLINE'
  ):
    if tokens[new_index][1] == 'NEWLINE':
      new_index += 1
    else:
      node, new_index = statement(tokens, new_index)
      if node:
        nodes.append(node)
      else:
        return None, new_index
  return generate_node('block', nodes), new_index


def import_statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'import_statement',
    ['IMPORT', 'NAME', 'NEWLINE']
  )

def arguments_list(tokens, index):
  return check_nonterminal(
    tokens, index, 'arguments_list',
    {'optional': [ 'NAME', {'any_count': ['COMMA', 'NAME'] }]}
  )


def expression_statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'expression_statement',
    [expression, 'NEWLINE']
  )


def assignment_statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'assignment_statement',
    [ 'NAME', ( assignment_tail, 'EQUAL' ), expression, 'NEWLINE']
  )


def assignment_tail(tokens, index):
  return check_nonterminal(
    tokens, index, 'tail',
    (   [ (attribute_loading, subscription), 'EQUAL'  ],
      [ (attribute_loading, subscription, function_call),
        assignment_tail ] )
  )


def return_statement(tokens, index):
  return check_nonterminal(
    tokens, index, 'return_statement',
    [ 'RETURN', expression, 'NEWLINE']
  )


def expression(tokens, index):
  return check_nonterminal(
    tokens, index, 'expression',
    [ and_test, {'optional': ['OR', and_test]} ]
  )


def and_test(tokens, index):
  return check_nonterminal(
    tokens, index, 'and_test',
    [ not_test, {'optional': ['AND', not_test]} ]
  )


def not_test(tokens, index):
  return check_nonterminal(
    tokens, index, 'not_test',
    ( ['NOT', not_test], comparison )
  )


def comparison(tokens, index):
  return check_nonterminal(
    tokens, index, 'comparison',
    [   arithmetic_expression,
      {'any_count': [comp_op, arithmetic_expression]} ]
  )


def comp_op(tokens, index):
  return check_nonterminal(
    tokens, index, 'comp_op',
    ( 'LESS', 'LESSEQUAL', 'GREATER', 'GREATEREQUAL',
      'EQEQUAL', 'NOTEQUAL', 'IN', ['NOT', 'IN'],
      'IS', ['IS', 'NOT'] )
  )


def arithmetic_expression(tokens, index):
  return check_nonterminal(
    tokens, index, 'arithmetic_expression',
    [ term, {'any_count': [('PLUS', 'MINUS'), term]}]
  )


def expression_list(tokens, index):
  return check_nonterminal(
    tokens, index, 'expression_list',
    [ expression, { 'any_count': ['COMMA', expression]  }]
  )


def term(tokens, index):
  return check_nonterminal(
    tokens, index, 'term',
    [ factor, {'any_count': [('STAR', 'SLASH', 'PERCENT'), factor]}]
  )


def factor(tokens, index):
  return check_nonterminal(
    tokens, index, 'factor',
    ([('PLUS', 'MINUS'), factor], power)
  )


def power(tokens, index):
  return check_nonterminal(
    tokens, index, 'power',
    [ atom_expression, {'optional': ['DOUBLESTAR', factor]}]
  )


def atom_expression(tokens, index):
  return check_nonterminal(
    tokens, index, 'atom_expression',
    [(
      brackets_expression,
      list_creation,
      atom
    ), {'optional': tail }]
  )


def brackets_expression(tokens, index):
  return check_nonterminal(
    tokens, index, 'brackets_expression',
    ['LPAR', expression, 'RPAR']
  )


def tail(tokens, index):
  return check_nonterminal(
    tokens, index, 'tail',
    [(
      attribute_loading,
      function_call,
      subscription
    ),
    {'optional': tail}  ]
  )


def attribute_loading(tokens, index):
  return check_nonterminal(
    tokens, index, 'attribute_loading',
    [ 'DOT', 'NAME']
  )


def function_call(tokens, index):
  return check_nonterminal(
    tokens, index, 'function_call',
    ['LPAR', {'optional': expression_list}, 'RPAR']
  )


def subscription(tokens, index):
  return check_nonterminal(
    tokens, index, 'subscription',
    [ 'LSQB', expression, 'RSQB']
  )


def list_creation(tokens, index):
  return check_nonterminal(
    tokens, index, 'list_creation',
    ['LSQB', {'optional': expression_list}, 'RSQB']
  )


def atom(tokens, index):
  return check_nonterminal(
    tokens, index, 'atom',
    ( 'NAME', 'FLOAT', 'INT', 'STRING' )
  )


if __name__ == '__main__':
  from lexic import tokenize

  f = open("../python.py")
  code = f.read()
  tokens = tokenize(code)
  p = parse(tokens[0])
  print(p)