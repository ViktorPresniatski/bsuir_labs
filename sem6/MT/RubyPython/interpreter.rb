require_relative 'lexic'
require_relative 'syntax'
require_relative 'compiler'


class SyObject
  attr_accessor :ref_count, :sy_type, :dictionary

  def initialize(dictionary)
    self.ref_count = 0
    self.sy_type = self.class
    self.dictionary = dictionary
    self.dictionary['__dict__'] = dictionary
    self.dictionary['__class__'] = self.class
  end

  def inc_ref
    self.ref_count += 1
  end

  def dec_ref
    self.ref_count -= 1
  end
  
  def get_attribute(name)
    if self.dictionary.include?('__getattr__')
      return self.dictionary['__getattr__'].call(name)
    elsif self.dictionary.include?(name)
      return self.dictionary[name]
    else
      return SyException.new(
        "Error: Object <#{self.name}> has no attribute #{name}."
      )
    end
  end

  def to_PyObject
    if self.dictionary.include?('__str__')
      return self.dictionary['__str__'].call.base_string
    else
      return "<'#{self.sy_type.name}' object>"
    end
  end
end


class SyType < SyObject
  attr_accessor :name, :bases

  def initialize(name, bases, dictionary)
    super(dictionary)
    self.name = name
    self.bases = bases
  end

  class << self
    attr_accessor :name, :bases, :dictionary, :sy_type
  end

  self.name = 'type'
  self.bases = []
  self.dictionary = {}
end
SyType.sy_type = SyType


class SyNoneType < SyType
  def initialize
    super(
      'NoneType', [], {
        '__str__' => SyBuiltInFunction.new(method(:__str__)),
        '__eq__' => SyBuiltInFunction.new(method(:__eq__)),
      }
    )
  end

  def __str__
    return SyString.new('None')
  end

  def __eq__(right)
    SyBool.new(right.is_a?(SyNoneType))
  end
  
  def to_PyObject
    nil
  end
end


class SyException < SyType
  attr_accessor :message

  def initialize(message)
    super('exception', [], {})
    self.message = message
  end
end


class SyBool < SyType
  attr_accessor :base_bool

  def initialize(base_bool)
    super(
        'bool',
        [], {
            '__str__' => SyBuiltInFunction.new(method(:__str__)),
            '__nonzero__' => SyBuiltInFunction.new(method(:__nonzero__)), 
            '__eq__' => SyBuiltInFunction.new(method(:__eq__))
        }
    )
    self.base_bool = base_bool
  end

  def __str__
    SyString.new(self.base_bool.to_s)
  end

  def __nonzero__
    return self.base_bool
  end
  
  def __eq__(right)
    return SyBool(self.base_bool == right)
  end

  def to_PyObject
    return self.base_bool
  end
end


class SyList < SyType
  attr_accessor :base_list

  def initialize(args)
    super(
      'list',
      [], {
        '__str__' => SyBuiltInFunction.new(method(:__str__), 0),
        '__iter__' => SyBuiltInFunction.new(method(:__iter__)),
        '__getitem__' => SyBuiltInFunction.new(method(:__getitem__)),
        '__setitem__' => SyBuiltInFunction.new(method(:__setitem__ )),
        'sort' => SyBuiltInFunction.new(method(:sort), 0),
        'append' => SyBuiltInFunction.new(method(:append), 1),
        'pop' => SyBuiltInFunction.new(method(:pop), 0),
      }
    )
    self.base_list = args
  end

  def __getitem__(index)
    index = index.to_PyObject()
    begin
      return self.base_list[index]
    rescue
      return SyException.new("Error: index #{index} out of range.")
    end
  end
  
  def __setitem__(index, value)
    self.base_list[index.to_PyObject()] = value
  end

  def __iter__
    SyBuiltInIter.new(self.base_list.each)
  end

  def append(item)
    self.base_list << item
  end

  def pop
    return self.base_list.pop
  end

  def to_PyObject
    return self.base_list
  end

  def __str__
    result = SyString.new('[')
    return result + SyString.new(']') if self.base_list.size == 0
    self.base_list[0...-1].each do |sy_object|
      result = result + sy_object.get_attribute('__str__').call + SyString.new(', ')
    end
    result = result + self.base_list[-1].get_attribute('__str__').call
    return result + SyString.new(']')
  end

  def sort
    sy_list = self.base_list.map { |el| el.to_PyObject() }
    begin      
      self.base_list = sy_list.sort
    rescue
      return SyException.new('Error: Can\'t sort this list.')
    end
  end
end


class SyBuiltInIter < SyType
  attr_accessor :base_iter

  def initialize(base_iter)
    super(
      'BuiltInIterator',
      [], {
          '__str__' => SyBuiltInFunction.new(method(:__str__)),
          '__next__' => SyBuiltInFunction.new(method(:next)),
      }
    )
    self.base_iter = base_iter
  end

  def __str__
    SyString.new('Iter')
  end

  def next
    return self.base_iter.next
  end

  def to_PyObject
    return self.base_iter
  end
end


class SyString < SyType
  attr_accessor :base_string

  def initialize(base_string)
    super(
      'string',
      [], {
          '__add__' => SyBuiltInFunction.new(method(:+)),
          '__str__' => SyBuiltInFunction.new(method(:__str__)),
          '__eq__' => SyBuiltInFunction.new(method(:__eq)),
      }
    )
    self.base_string = base_string.gsub('\\n', "\n")
  end

  def +(right)
    return SyString.new(self.base_string + right.base_string)
  end

  def __eq(right)
    return SyBool.new(self.base_string == right.base_string)
  end

  def __str__
    self
  end

  def to_PyObject
    return self.base_string
  end
end


class SyInt < SyType
  attr_accessor :base_int

  def initialize(base_int)
    super(
      'int',
      [], {
        '__add__' => SyBuiltInFunction.new(method(:+)),
        '__sub__' => SyBuiltInFunction.new(method(:-)),
        '__pow__' => SyBuiltInFunction.new(method(:**)),
        '__div__' => SyBuiltInFunction.new(method(:/)),
        '__mod__' => SyBuiltInFunction.new(method(:%)),
        '__mul__' => SyBuiltInFunction.new(method(:*)),
        '__str__' => SyBuiltInFunction.new(method(:__str)),
        '__eq__'  => SyBuiltInFunction.new(method(:__eq)),
        '__ne__'  => SyBuiltInFunction.new(method(:__ne)),
        '__cmp__' => SyBuiltInFunction.new(method(:__cmp)),
      }
    )
    self.base_int = base_int.to_i
  end

  def +(right)
    return right.class.new(self.base_int + right.to_PyObject())
  end

  def -(right)
    return right.class.new(self.base_int - right.to_PyObject())
  end

  def *(right)
    return right.class.new(self.base_int * right.to_PyObject())
  end
  
  def **(right)
    return right.class.new(self.base_int ** right.to_PyObject())
  end
  
  def /(right)
    return SyException.new("Error: Division by zero") if right.to_PyObject() == 0
    return SyFloat.new(self.base_int / right.to_PyObject())
  end

  def %(right)
    return right.class.new(self.base_int % right.to_PyObject())
  end

  def __str
    return SyString.new(self.base_int.to_s)
  end

  def __eq(right)
    return SyBool.new(self.base_int == right.to_PyObject())
  end

  def __ne(right)
    return SyBool.new(self.base_int != right.to_PyObject())
  end

  def __cmp(other)
    other = other.to_PyObject()
    if self.base_int < other
      return SyInt.new(-1)
    elsif self.base_int > other
      return SyInt.new(1)
    else
      return SyInt.new(0)
    end
  end

  def to_PyObject
    return self.base_int
  end
end


class SyFloat < SyType
  attr_accessor :base_float

  def initialize(base_float)
    super(
      'float',
      [], {
        '__add__' => SyBuiltInFunction.new(method(:+)),
        '__sub__' => SyBuiltInFunction.new(method(:-)),
        '__pow__' => SyBuiltInFunction.new(method(:**)),
        '__div__' => SyBuiltInFunction.new(method(:/)),
        '__mod__' => SyBuiltInFunction.new(method(:%)),
        '__mul__' => SyBuiltInFunction.new(method(:*)),
        '__str__' => SyBuiltInFunction.new(method(:__str)),
        '__eq__' => SyBuiltInFunction.new(method(:__eq)),
        '__ne__' => SyBuiltInFunction.new(method(:__ne)),
        '__cmp__'=> SyBuiltInFunction.new(method(:__cmp)),
      }
    )
    self.base_float = base_float.to_f
  end

  def +(right)
    return SyFloat.new(self.base_float + right.to_PyObject())
  end

  def -(right)
    return SyFloat.new(self.base_float - right.to_PyObject())
  end  

  def *(right)
    return SyFloat.new(self.base_float * right.to_PyObject())
  end

  def **(right)
    return SyFloat.new(self.base_float ** right.to_PyObject())
  end

  def /(right)
    return SyException.new("Error: Division by zero") if right.to_PyObject() == 0
    return SyFloat.new(self.base_float / right.to_PyObject())
  end

  def %(right)
    return SyFloat.new(self.base_float % right.to_PyObject())
  end

  def __str
    return SyString.new(self.base_float.to_s)
  end

  def __eq(right)
    return SyBool.new(self.base_float == right.to_PyObject())
  end

  def __cmp(other)
    other = other.to_PyObject()
    if self.base_float < other
      return SyInt.new(-1)
    elsif self.base_float > other
      return SyInt.new(1)
    else
      return SyInt.new(0)
    end
  end

  def __ne(right)
    return SyBool.new(self.base_float != right.to_PyObject())
  end

  def to_PyObject
    return self.base_float
  end
end

class SyBuiltInFunction < SyType
  attr_accessor :base_function, :name, :arg_count, :call_frame

  def initialize(base_function, arg_count=nil)
    super(
      'BuiltInFunction',
      [], {
        '__call__' => self
      }
    )
    self.base_function = base_function
    if arg_count.nil?
      self.arg_count = base_function.parameters.count
    else
      self.arg_count = arg_count
    end
    self.name = base_function.is_a?(Method) ? base_function.name : 'lambda' 
  end

  def call(*args, **kwargs)
    if args.any? && kwargs.any?
      return self.base_function.call(*args, **kwargs)
    elsif args.any? && kwargs.empty?
      return self.base_function.call(*args)
    elsif args.empty? && kwargs.any?
      return self.base_function.call(**kwargs)
    else
      return self.base_function.call(*args)
    end
  end

  def to_PyObject
    return self
  end
end

class SyCode < SyType
  attr_accessor :bytecode, :arg_count, :arg_names

  def initialize(bytecode, args)
    super(
        'code',
        [], {
          '__args_count__' => args.size(),
        }
    )
    self.bytecode = bytecode
    self.arg_count = args.size
    self.arg_names = args
  end
end


class SyFunction < SyType
  attr_accessor :sy_code, :arg_count, :parent_frame, :name, :call_frame

  def initialize(name, sy_code, parent_frame)
    super(
      'function',
      [], {
        '__call__' => self,
        '__code__' => sy_code,
        '__name__' => name
      }
    )
    self.sy_code = sy_code
    self.arg_count = sy_code.arg_count
    self.parent_frame = parent_frame
    self.name = name.to_PyObject()
    self.call_frame = nil
  end

  def call(*args, **kwargs)
    namespace = self.sy_code.arg_names.zip(args).each_with_object({}) { |a, memo| memo[a[0]] = a[1] }
    sy_frame = SyFrame.new(
      self.sy_code.bytecode,
      self.name,
      namespace,
      self.parent_frame
    )
    sy_frame.call_frame = self.call_frame
    return sy_frame.execute()
  end
end


SyFrame = Class.new(SyType) do
  attr_accessor :name, :namespace, :parent_frame, :call_frame, :bytecode,
                :length, :stack, :ip, :handlers, :ret

  def initialize(bytecode, name, namespace, parent_frame=nil)
    super(
      'frame',
      [], {
        '__name__' => SyString.new(name),
        '__getattr__' => SyBuiltInFunction.new(method(:__getattr)),
      }
    )
    self.name = name
    self.namespace = namespace.dup
    self.parent_frame = parent_frame
    self.call_frame = nil
    self.bytecode = bytecode
    self.length = bytecode.size
    self.stack = []
    self.ip = 0
    self.handlers = {
      'CALL_FUNCTION' => method(:call_function),
      'IMPORT_NAME' => method(:import_name),
      'LOAD_INT' => method(:load_int),
      'LOAD_FLOAT' => method(:load_float),
      'LOAD_STRING' => method(:load_string),
      'LOAD_NAME' => method(:load_name),
      'LOAD_CODE' => method(:load_code),
      'LOAD_ATTRIBUTE' => method(:load_attribute),
      'MAKE_FUNCTION' => method(:make_function),
      'STORE_NAME' => method(:store_name),
      'STORE_SUBSCRIPT' => method(:store_subscript),
      'GET_ITER' => method(:get_iter),
      'FOR_ITER' => method(:for_iter),
      'BUILD_LIST' => method(:build_list),
      'JUMP_IF_FALSE' => method(:jump_if_false),
      'JUMP' => method(:jump_stack),
      'ADD' => method(:add),
      'SUBTRACT' => method(:subtract),
      'MULTIPLY' => method(:multiply),
      'DIVIDE' => method(:divide),
      'MODULO' => method(:modulo),
      'POWER' => method(:power),
      'SUBSCRIPT' => method(:subscript),
      'COMPARE' => method(:compare),
      'NEGATIVE' => method(:negative),
      'RETURN' => method(:ret_stack),
      'POP' => method(:pop_stack),
    }
  end

  def __getattr(name)
    sy_object = self.namespace[name]
    if sy_object.present?
      return sy_object
    end
  end

  def init
    self.ip = 0
  end

  def end
    return self.ip >= self.length
  end

  def current_command
    return self.bytecode[self.ip]
  end

  def jump(step)
    self.ip += step
  end

  def execute
    self.init
    while !self.end
      command, argument = self.current_command
      self.handlers[command].call(self, argument)
      self.check_exceptions
    end
    if self.ret
      return self.ret
    else
      return SyNoneType.new
    end
  end

  def check_exceptions
    if self.stack.any?
      if self.stack.last.is_a?(SyException)
        self.emergency_stop
        puts "\t#{self.stack.last.message}"
      end
    end
  end

  def stop
    self.ip = self.length
  end

  def emergency_stop
    self.stop
    if self.call_frame
      self.call_frame.emergency_stop
    end
    puts "In #{self.name}:"
  end

  def push(value)
    self.stack << value
  end

  def pop
    return self.stack.pop
  end
end


def execute(module_name, name='__main__', call_frame=nil)
  begin
    code = File.open("#{module_name}.py").read
  rescue
    return SyException.new("Error: No module named #{module_name}.")
  end
  tokens, errors = Lexic.tokenize(code)
  # require 'pp'; pp tokens

  if errors.any?
    return SyException.new(errors.join("\n"))
  end

  ast = Syntax.parse(tokens)
  # pp ast

  if ast.is_a?(String)
    return SyException.new(ast)
  end

  bytecode = Compiler.compile(ast)
  #Output.print_bytecode(bytecode)

  namespace = {
    '__name__' => SyString.new(name),
    'None' => SyNoneType.new,
    'True' => SyBool.new(true),
    'False' => SyBool.new(false),
    'print' => SyBuiltInFunction.new(method(:sy_print), -1),
    'len' => SyBuiltInFunction.new(method(:sy_len), 1),
    'str' => SyBuiltInFunction.new(lambda { |x| x.get_attribute('__str__').call }),
    'range' => SyBuiltInFunction.new(method(:sy_range), -1),
  }
  sy_frame = SyFrame.new(bytecode, name, namespace)
  sy_frame.call_frame = call_frame
  sy_frame.execute
  return sy_frame
end


def sy_print(*sy_objects)
  result = ''
  sy_objects.reverse.each do |sy_object|
    result += sy_object.get_attribute('__str__').call.to_PyObject()
  end
  puts result
end

def sy_range(*arg)
  args = args.map { |arg| arg.base_int }
  if args.size == 2
    return SyList.new((args.first...args.last).to_a)
  else
    return SyList.new((0...args.first).to_a)
  end
end

def sy_len(sy_object)
  begin
    return SyInt.new(sy_object.to_PyObject().size)
  rescue
    return SyException.new(
      "Error: object #{sy_object.get_attribute('__str__').call.to_PyObject()} has no len."
    )
  end
end


def call_function(sy_frame, argument)
  args = (0...argument).map { |i| sy_frame.pop }.reverse
  sy_function = sy_frame.pop
  sy_function.call_frame = sy_frame
  if args.size == sy_function.arg_count || sy_function.arg_count == -1
    sy_frame.push(sy_function.call(*args))
    sy_frame.jump(1)
  else
    sy_frame.push(SyException.new(
      "Error: Function #{sy_function.name} takes exactly #{sy_function.arg_count} argument(s), #{args.size} given."
    ))
  end
end

def load_float(sy_frame, argument)
  sy_float = SyFloat.new(argument.to_f)
  sy_frame.push(sy_float)
  sy_frame.jump(1)
end


def load_int(sy_frame, argument)
  sy_int = SyInt.new(argument.to_i)
  sy_frame.push(sy_int)
  sy_frame.jump(1)
end

def load_string(sy_frame, argument)
  argument = argument[1...-1] if argument[0] == "\'" || argument[0] == "\""
  sy_string = SyString.new(argument)
  sy_frame.push(sy_string)
  sy_frame.jump(1)
end

def load_name(sy_frame, argument)
  frame = sy_frame
  while !frame.namespace.include?(argument) and !frame.parent_frame.nil?
    frame = frame.parent_frame
  end
  if !frame.namespace.include?(argument)
    sy_frame.push(SyException.new("Error: No object named #{argument}."))
  else
    sy_object = frame.namespace[argument]
    sy_frame.push(sy_object)
    sy_frame.jump(1)
  end
end

def import_name(sy_frame, argument)
  sy_frame.push(execute(argument, argument, sy_frame))
  sy_frame.jump(1)
end


def get_iter(sy_frame, argument)
  sy_object = sy_frame.pop
  sy_iter = sy_object.get_attribute('__iter__').call
  sy_frame.push(sy_iter)
  sy_frame.jump(1)
end


def for_iter(sy_frame, argument)
  sy_iter = sy_frame.pop
  begin
    sy_frame.push(sy_iter)
    sy_frame.push(sy_iter.get_attribute('__next__').call)
    sy_frame.jump(1)
  rescue
    sy_frame.jump(argument)
  end
end

def load_code(sy_frame, argument)
  sy_code = SyCode.new(argument['code'], argument['args'])
  sy_frame.push(sy_code)
  sy_frame.jump(1)
end


def load_attribute(sy_frame, argument)
  sy_object = sy_frame.pop
  sy_frame.push(sy_object.get_attribute(argument))
  sy_frame.jump(1)
end


def make_function(sy_frame, argument)
  name = sy_frame.pop
  sy_code = sy_frame.pop
  sy_frame.push(SyFunction.new(name, sy_code, sy_frame))
  sy_frame.jump(1)
end


def store_name(sy_frame, argument)
  sy_object = sy_frame.pop
  sy_frame.namespace[argument] = sy_object
  sy_frame.jump(1)
end


def store_subscript(sy_frame, argument)
  argument = sy_frame.pop
  sy_object = sy_frame.pop
  sy_value = sy_frame.pop
  sy_object.dictionary['__setitem__'].call(argument, sy_value)
  sy_frame.jump(1)
end


def build_list(sy_frame, argument)
  args = (0...argument).map { |i| sy_frame.pop }.reverse
  sy_frame.push(SyList.new(args))
  sy_frame.jump(1)
end


def subscript(sy_frame, argument)
  argument = sy_frame.pop
  sy_object = sy_frame.pop
  sy_frame.push(sy_object.dictionary['__getitem__'].call(argument))
  sy_frame.jump(1)
end


def jump_if_false(sy_frame, argument)
  sy_bool = sy_frame.pop
  if !sy_bool.base_bool
    sy_frame.jump(argument)
  else
    sy_frame.jump(1)
  end
end

def jump_stack(sy_frame, argument)
  sy_frame.jump(argument)
end


def add(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  sy_object = left.dictionary['__add__'].call(right)
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end


def subtract(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  sy_object = left.dictionary['__sub__'].call(right)
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end


def multiply(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  sy_object = left.dictionary['__mul__'].call(right)
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end

def divide(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  sy_object = left.dictionary['__div__'].call(right)
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end

def modulo(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  sy_object = left.dictionary['__mod__'].call(right)
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end


def power(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  sy_object = left.dictionary['__pow__'].call(right)
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end


def negative(sy_frame, argument)
  sy_object = sy_frame.pop
  sy_object = sy_object.dictionary['__mul__'].call(SyInt.new(-1))
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end


def compare(sy_frame, argument)
  right = sy_frame.pop
  left = sy_frame.pop
  if argument == '=='
    sy_object = left.get_attribute('__eq__').call(right)
  elsif argument == '!='
    sy_object = left.get_attribute('__ne__').call(right)
  elsif argument == '>'
    sy_object = left.get_attribute('__cmp__').call(right)
    sy_object = SyBool.new(sy_object.base_int == 1)
  elsif argument == '>='
    eq = left.get_attribute('__eq__').call(right)
    grater = left.get_attribute('__cmp__').call(right)
    grater = SyBool.new(grater.base_int == 1)
    sy_object = SyBool.new(eq.base_bool || grater.base_bool)
  elsif argument == '<'
    sy_object = left.get_attribute('__cmp__').call(right)
    sy_object = SyBool.new(sy_object.base_int == -1)
  elsif argument == '<='
    eq = left.get_attribute('__eq__').call(right)
    less = left.get_attribute('__cmp__').call(right)
    less = SyBool.new(less.base_int == -1)
    sy_object = SyBool.new(eq.base_bool || less.base_bool)
  end
  sy_frame.push(sy_object)
  sy_frame.jump(1)
end


def ret_stack(sy_frame, argument)
  sy_frame.ret = sy_frame.pop
  sy_frame.stop
end


def pop_stack(sy_frame, argument)
  sy_frame.pop
  sy_frame.jump(1)
end


if __FILE__ == $0
  file = ARGV[0]
  execute("../#{file.gsub('.py', '')}")
end
