#!/home/viktor/.rvm/rubies/ruby-2.4.2/bin/ruby

class Lexic
  class << self
    IGNORE = {
        'SPACE'        => /[ \n\t]+/,
        'COMMENTS'     => /#[^\n]*/,
    }


    RESERVED = {
        'LPAR'         => /\(/,
        'RPAR'         => /\)/,
        'LSQB'         => /\[/,
        'RSQB'         => /\]/,
        'COLON'        => /\:/,
        'COMMA'        => /,/,
        'SEMI'         => /;/,
        'LEFTSHIFT'    => /<</,
        'RIGHTSHIFT'   => />>/,
        'DOUBLESTAR'   => /\*\*/,
        'PLUSEQUAL'    => /\+=/,
        'MINEQUAL'     => /-=/,
        'MULTEQUAL'    => /\*=/,
        'DIVEQUAL'     => /\/=/,
        'EQEQUAL'      => /==/,
        'NOTEQUAL'     => /!=/,
        'LESSEQUAL'    => /<=/,
        'GREATEREQUAL' => />=/,
        'EQUAL'        => /=/,
        'PLUS'         => /\+/,
        'MINUS'        => /-/,
        'STAR'         => /\*/,
        'SLASH'        => /\//,
        'AMPER'        => /\&/,
        'VBAR'         => /\|/,
        'LESS'         => /</,
        'GREATER'      => />/,
        'DOT'          => /\./,
        'PERCENT'      => /%/,
        'LBRACE'       => /\{/,
        'RBRACE'       => /\}/,
        'TILDE'        => /~/,
        'CIRCUMFLEX'   => /\^/,
        'FOR'          => /for\b/,
        'WHILE'        => /while\b/,
        'DEF'          => /def\b/,
        'IF'           => /if\b/,
        'ELIF'         => /elif\b/,
        'ELSE'         => /else\b/,
        'OR'           => /or\b/,
        'AND'          => /and\b/,
        'NOT'          => /not\b/,
        'IN'           => /in\b/,
        'RETURN'       => /return\b/,
    }


    OTHER = {
        'NAME'         => /[_A-Za-z][_A-Za-z0-9]*/,
        'FLOAT'        => /[0-9]+\.[0-9]*/,
        'INT'          => /[0-9]+/,
        'STRING'       => /\".*\"\\|\'.*\'/,
    }


    PATTERNS = IGNORE.to_a + RESERVED.to_a + OTHER.to_a


    def tokenize(code)
      line_start = 0
      line_count = 1
      pos = 0
      tokens = []
      errors = []
      while pos < code.size
        if code[pos] == "\n"
          token = ['\n', 'NEWLINE', line_count, pos-line_start+1]
          tokens << token
          pos += 1
          line_start = pos
          line_count += 1
          next
        end
        match = nil
        PATTERNS.each do |name, pattern|
          regex = Regexp.new(/\A/.to_s + pattern.to_s)
          match = regex.match(code[pos..-1])
          if match && name
            unless IGNORE.include?(name)
              text = match[0]
              token = [text, name, line_count, pos-line_start+1]
              tokens << token
            end
            pos += match.end(0)
            break
          end
        end
        unless match
          errors << "Illegal character: \'#{code[pos]}\' in line #{line_count} position #{pos-line_start+1}"
          pos += 1
        end
      end
      return tokens, errors
    end
  end
end


def print_result(tokens)
  tokens = tokens.first.sort_by { |a| a[1] }
  i = 0
  puts "Token type    Token value           Line  Column"
  tokens.each do |token|
    print "#{token[1]}#{' ' * (14 - token[1].size)}#{token[0]}#{' ' * (22 - token[0].size)}#{token[2]}#{' ' * (7 - token[2].to_s.size)}#{token[3]}\n"
  end
  puts
end


def print_errors(tokens)
  tokens.last.each do |error|
    puts error
  end
end

if __FILE__ == $0
  file = File.open("../python.py")
  code = file.read
  tokens = Lexic.tokenize(code)
  print_result(tokens)
  print_errors(tokens)
  file.close()
end