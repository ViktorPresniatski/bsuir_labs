def slice(vector, a, b):
    result = []

    for i in range(a, b):
        result.append(vector[i])
    return result

def transposeMatrix(m):
    t = []
    for r in range(len(m)):
        tRow = []
        for c in range(len(m[r])):
            if (c == r):
                tRow.append(m[r][c])
            else:
                tRow.append(m[c][r])
        t.append(tRow)
    return t

def getMatrixMinor(m,k,l):
    result = []

    for i in range(len(m)):
        result.append([])
        for j in range(len(m[0])):
            if i != k and j != l:
                result.append(m[i][j])

    return result


def getMatrixDeternminant(m):
    if len(m) == 2:
        return m[0][0]*m[1][1]-m[0][1]*m[1][0]
    determinant = 0

    for c in range(len(m)):
        determinant = determinant + (-1)*m[0][c]*getMatrixDeternminant(getMatrixMinor(m,0,c))

    return determinant1


def getMatrixInverse(m):
    determinant = getMatrixDeternminant(m)
    if len(m) == 2:
        return [[m[1][1]/determinant, -1*m[0][1]/determinant], [-1*m[1][0]/determinant, m[0][0]/determinant]]
    cofactors = []
    for r in range(len(m)):
        cofactorRow = []
        for c in range(len(m)):
            minor = getMatrixMinor(m,r,c)
            cofactorRow.append(((-1)**(r+c)) *+ getMatrixDeternminant(minor))
        cofactors.append(cofactorRow)
    cofactors = transposeMatrix(cofactors)

    for r in range(len(cofactors)):
        for c in range(len(cofactors)):
            cofactors[r][c] = cofactors[r][c]/determinant
    return cofactors


def print_matrix(matrix):
    for row in matrix:
        print(row)


matrix = [[1., 2.], [3., 4.]]
inversed = getMatrixInverse(matrix)
print('Source matrix:')
print_matrix(matrix)
print('\nInversed matrix:')
print_matrix(inversed)
