import numpy as np
import argparse
import scipy.linalg as sla


# def first_simplex(A, b, c):
#     m, n = A.shape
#     new_A = np.concatenate((A, np.eye(m)), axis=1)
#     new_x = np.concatenate((np.zeros(n), b), axis=0)
#     new_Jb = list(range(n, n + m))
#     new_c = np.concatenate((np.zeros(n), -np.ones(m)), axis=0)

#     basic_plan = simplex(new_A, new_c, new_x, new_Jb[:])
#     x = basic_plan['result']
#     Jb = basic_plan['basic_indices']
#     if len(set(x[n:])) > 1 or x[-1] != 0:
#         raise Exception("don't found basic plan")

#     depend = []
#     while len(set(Jb) & set(new_Jb)) != 0:
#         js = list(set(Jb) & set(new_Jb))[0]
#         j0 = list(set(range(n + m)) - set(Jb) - set(new_Jb))[0]

        
#         es = np.zeros(m)
#         es[Jb.index(js)] = 1
#         tmp = np.dot(es, np.linalg.inv(new_A[:,list(Jb)]))
#         aj = np.dot(tmp, new_A[:,j0].T)
#         if aj != 0:
#             Jb.remove(js)
#             Jb.append(j0)
#         else:
#             raise Exception(str(js - n + 1) + " linearly dependent")

#     return (x[:n], Jb)
    

def simplex(A, c, x, Jb, left, right):
    m, n = A.shape
    Jn = list(set(range(n)) - set(Jb))

    B = np.linalg.inv(A[:,list(Jb)])
    while True:
        # step 1
        u = np.dot(c[list(Jb)], B)
        # P, L, U = sla.lu(A[:,list(Jb)])
        # y = sla.solve_triangular(L, c[list(Jb)], lower=True)
        # u = sla.solve_triangular(U, y, lower=False)

        delta = np.dot(u, A) - c
        print("delta", delta)
        # step 2
        is_good = True
        for j in Jn:
            if abs(x[j] - left[j]) < 1e-15 and delta[j] < 0:
                j0 = j
                z0 = 1
                is_good = False
                break

            elif abs(x[j] - right[j]) < 1e-15 and delta[j] > 0:
                is_good = False
                j0 = j
                z0 = -1
                break

        if is_good:
            return {'success': True, 'result': x, 'basic_indices':Jb}

        print("j0", j0)
        z = -1 * (z0) * np.dot(B, A[:,j0])
        print("z", z)
        # y = sla.solve_triangular(L, A[:,j0], lower=True)
        # z = sla.solve_triangular(U, y, lower=False)
       
        if all(z[i] <= 0 for i in range(len(z))):
            return {'success': False}
       

        # step 4
        # array_theta = [(x[ji] / z[i], i, ji) for i, ji in enumerate(Jb) if z[i] > 0]
        array_theta = list()
        for i in range(m):
            if z[i] > 0:
                array_theta.append(((right[Jb[i]] - x[Jb[i]]) / z[i], i, Jb[i]))
            elif z[i] < 0:
                array_theta.append(((left[Jb[i]] - x[Jb[i]]) / z[i], i, Jb[i]))

        theta0, s, js = min(array_theta)
        print("array_theta", array_theta)
        # step 5
        # for j in Jn:
        #     if j != j0:
        #         x[j] = 0

        

        for i, ji in enumerate(Jb):
            x[ji] += theta0 * z[i]

        x[j0] += theta0 * z0

        save_index = Jb.index(js)
        Jb.remove(js)
        Jn.remove(j0)

        Jb = Jb[:save_index] + [j0] + Jb[save_index:]
        Jn.append(js)

        print("x",x)
        print("Jb",Jb)
        # print("Jb", Jb)
        if s == 0:
            continue
        # step 6
        M = np.identity(B.shape[0])
        zs = z[s]
        z[s] = -1
        z = -1 / zs * z
        M[:,s] = z
        B = np.dot(M, B)
    


if __name__ == '__main__':
    A = np.array([[1, 2, 0, -1, 0, -2, 1],
                  [0, -1, 1, -1, 0, 3, 2],
                  [0, 4, 0, -1, 1, 6, -4]], dtype=np.float)
    b = np.array([-13, 13, 29], dtype=np.float)
    c = np.array([1, 1, 1, 1, 1, 1, 1], dtype=np.float)
    left = np.array([0,0,0,0,0,0,0], dtype=np.float)
    right = np.array([5,6,4,4,5,5,6], dtype=np.float)

    # A = np.array([[-1, 1, 1, 0, 0],
    #               [0, 1, 0, 0, 1],
    #               [0, 2, 0, 0, 2]])
    # b = np.array([1, 2, 4])
    # c = np.array([1, 1, 0, 0, 0])


    x = np.array([1,0,2,4,3,5,0], dtype=np.float)
    Jb = [0, 2, 4]

    # x = np.array([0,0,3.3,4,5,4.9333333,0], dtype=np.float)
    # Jb = [1, 2, 5]
# [0.  0.1 3.3 4.  5.  4.6 0. ]
    print()
    print (x)
    print (Jb)
    result = simplex(A, c, x, Jb, left, right)
    if result['success']:
        print (result['result'], np.dot(c,result['result']))
    else:
        print ('No solution')

    print(np.dot(A, x))