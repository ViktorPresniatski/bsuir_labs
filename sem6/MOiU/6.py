import numpy as np


INFINITY = 99999999999


def J_not(J):
    return [i for i in range(len(c)) if i not in J]


def J_without(J, Jn):
    J_copy = J[:]
    
    for j in Jn:
        J_copy.remove(j)
    
    return J_copy


def find_j_0(delta):
    for i in range(len(delta)):
        if delta[i] < -0.00001:
            return i
    return None


# TRUE EXAMPLE
A = np.array([
        [1,1,1,2,3,1,1,-2],
        [0,1,0,1,1,2,3,-4],
        [0,1,1,3,-1,2,4,-5],
        ])

B = np.array([
    [1,1,0,3,-3,1,1,1],
    [-1,1,1,2,-1,0,1,1]
    ])

D = B.T.dot(B)

d_small = np.array([5,4])

c = (-d_small.T.dot(B)).T

x = np.array([3,4,1,0,0,0,0,0])
J_op = [0, 1, 2]
J_star = [0, 1, 2]





# EXAMPLE 0
# A = np.array([[1,2,0,1,0,4,-1,-3],
#               [1,3,0,0,1,-1,-1,2],
#               [1,4,1,0,0,2,-2,0]])

# b = np.array([4,5,6])

# D = np.array([[6, 11, -1, 0, 6, -7, -3, -2],
#               [11,41, -1, 0, 7, -24, 0, -3],
#               [-1, -1, 1, 0, -3, -4, 2, -1],
#               [0, 0, 0, 0, 0, 0, 0, 0],
#               [6, 7, -3, 0, 11, 6, -7, 1],
#               [-7,-24,-4, 0, 6, 42, -7, 10],
#               [-3, 0, 2, 0, -7, -7, 5, -1],
#               [-2, -3, -1, 0, 1, 10, -1, 3]])

# c = np.array([-10,-31,7,0,-21,-16,11,-7])

# x = np.array([0,0,6,4,5,0,0,0])
# J_op = [2, 3, 4]
# J_star = [2, 3, 4]


# # EXAMPLE 1
# A = np.array([
#         [11.0, 0.0, 0.0, 1.0, 0.0, -4.0, -1.0, 1.0],
#         [1.0, 1.0, 0.0, 0.0, 1.0, -1.0, -1.0, 1.0],
#         [1.0, 1.0, 1.0, 0.0, 1.0, 2.0, -2.0, 1.0],
#         ])

# B = np.array([
#     [1.0, -1.0, 0.0, 3.0, -1.0, 5.0, -2.0, 1.0],
#     [2.0, 5.0, 0.0, 0.0, -1.0, 4.0, 0.0, 0.0],
#     [-1.0, 3.0, 0.0, 5.0, 4.0, -1.0, -2.0, 1.0],
#     ])

# D = B.T.dot(B)

# d_small = np.array([6.0, 10.0, 9.0])

# c = (-d_small.T.dot(B)).T

# x = np.array([0.7273, 1.2727, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0])
# J_op = [0, 1, 2]
# J_star = [0, 1, 2]


# EXAMPLE 2
# A = np.array([
#         [2.0, -3.0, 1.0, 1.0, 3.0, 0.0, 1.0, 2.0],
#         [-1.0, 3.0, 1.0, 0.0, 1.0, 4.0, 5.0, -6.0],
#         [1.0, 1.0, -1.0, 0.0, 1.0, -2.0, 4.0, 8.0],
#         ])

# B = np.array([
#     [1.0, 0.0, 0.0, 3.0, -1.0, 5.0, 0.0, 1.0],
#     [2.0, 5.0, 0.0, 0.0, 0.0, 4.0, 0.0, 0.0],
#     [-1.0, 9.0, 0.0, 5.0, 2.0, -1.0, -1.0, 5.0],
#     ])

# D = B.T.dot(B)

# c = np.array([-13.0, -217.0, 0.0, -117.0, -27.0, -71.0, 18.0, -99.0])

# x = np.array([0.0, 2.0, 0.0, 0.0, 4.0, 0.0, 0.0, 1.0])
# J_op = [1, 4, 7]
# J_star = [1, 4, 7]


# EXAMPLE 3
# A = np.array([
#     [0.0, 2.0, 1.0, 4.0, 3.0, 0.0, -5.0, -10.0],
#     [-1.0, 3.0, 1.0, 0.0, 1.0, 3.0, -5.0, -6.0],
#     [1.0, 1.0, 1.0, 0.0, 1.0, -2.0, -5.0, 8.0],
#     ])

# D = np.array([
#     [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
#     ])

# c = np.array([1.0,3.0,-1.0,3.0,5.0,2.0,-2.0,0.0])

# x = np.array([0.0,2.0,0.0,0.0,4.0,0.0,0.0,1.0])
# J_op = [1, 4, 7]
# J_star = [1, 4, 7]


# EXAMPLE 4
# A = np.array([
#     [0.0, 2.0, 1.0, 4.0, 3.0, 0.0, -5.0, -10.0],
#     [-1.0, 1.0, 1.0, 0.0, 1.0, 1.0, -1.0, -1.0],
#     [1.0, 1.0, 1.0, 0.0, 1.0, -2.0, -5.0, 8.0],
#     ])

# D = np.array([
#     [25.0, 10.0, 0.0, 3.0, -1.0, 13.0, 0.0, 1.0],
#     [10.0, 45.0, 0.0, 0.0, 0.0, 20.0, 0.0, 0.0],
#     [0.0, 0.0, 20.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [3.0, 0.0, 0.0, 29.0, -3.0, 15.0, 0.0, 3.0],
#     [-1.0, 0.0, 0.0, -3.0, 21.0, -5.0, 0.0, -1.0],
#     [13.0, 20.0, 0.0, 15.0, -5.0, 61.0, 0.0, 5.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 20.0, 0.0],
#     [1.0, 0.0, 0.0, 3.0, -1.0, 5.0, 0.0, 21.0], 
#     ])

# c = np.array([1.0,-3.0,4.0,3.0,5.0,6.0,-2.0,0.0])

# x = np.array([3.0,0.0,0.0,2.0,4.0,0.0,0.0,0.0])
# J_op = [0, 3, 4]
# J_star = [0, 3, 4]


# EXAMPLE 5
# A = np.array([
#     [0.0, 0.0, 1.0, 5.0, 2.0, 0.0, -5.0, -4.0],
#     [1.0, 1.0, -1.0, 0.0, 1.0, -1.0, -1.0, -1.0],
#     [1.0, 1.0, 1.0, 0.0, 1.0, 2.0, 5.0, 8.0],
#     ])

# D = np.array([
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
#     ])

# c = np.array([1.0,-3.0,4.0,3.0,5.0,6.0,-2.0,0.0])

# x = np.array([4.0,0.0,5.0,2.0,0.0,0.0,0.0,0.0])
# J_op = [0, 2, 3]
# J_star = [0, 2, 3]


i = 0
while True:
    i += 1
    print "Iteration %d" % i
    c_x = c + np.dot(x, D)
    A_op_inv = np.linalg.inv(A[:, J_op])
    u_x = np.dot(-c_x[J_op], A_op_inv)
    delta = np.dot(u_x, A) + c_x
    print 'delta - {}'.format(delta)

    j0 = find_j_0(delta)
    if j0 is None:
        print 'Answer:\n{}'.format(x)
        print 'c: {}'.format(c.dot(x) + 0.5 * x.dot(D).dot(x))
        break

    print('j0 - {}'.format(j0))
    l = np.zeros(len(x))
    l[j0] = 1

    D_star = D[:,J_star][J_star]
    A_op_star = A[:,J_star]
    H_top = np.concatenate((D_star, A_op_star.T), axis=1)
    # print H_top, A_op_star
    H_down = np.concatenate((A_op_star, np.zeros( shape=(len(A),len(A)) )), axis=1)
    H = np.append(H_top, H_down, axis=0)

    print 'H - \n{}'.format(H)

    H_inv = np.linalg.inv(H)
    
    b_star = np.append(D[:, j0][J_star], (A[:, j0]))
    print 'b - {}'.format(b_star)
    y = np.dot(H_inv, -b_star)
    l[J_star] = y[range(len(J_star))]

    print 'L - {}'.format(l)

    theta = {}
    q = np.dot(np.dot(l, D), l)

    theta[j0] = abs(delta[j0]) / q if q > 0 else INFINITY

    for j in J_star:
        theta[j] = -x[j] / l[j] if l[j] < 0 else INFINITY

    j_star = min(theta, key=theta.get)
    theta_0 = theta[j_star]

    if theta_0 == INFINITY:
        print 'The objective function is not bounded above on the set of feasible plans'
        break

    x = x + theta_0 * l

    # import pdb; pdb.set_trace()
    if j_star == j0:
        J_star.append(j_star)
    elif j_star in J_without(J_star, J_op):
        J_star.remove(j_star)
    elif j_star in J_op:
        s = J_op.index(j_star)
        temp = {j: np.dot(A_op_inv, A[:, j])[s] == 0 for j in J_without(J_star, J_op)}

        print 's - {}'.format(s)
        print 'temp - {}'.format(temp)
        
        if not all(temp.values()):
            j_plus = min(temp, key=temp.get)
            J_star.remove(j_star)
            J_op[s] = j_plus
            # J_op.remove(j_star)
            # J_op.append(j_plus)
        elif J_op == J_star or all(temp.values()):
            J_op[s] = j0
            J_star[s] = j0
            # J_op.remove(j_star)
            # J_op.append(j0)
            # J_star.remove(j_star)
            # J_star.append(j0)

    print 'x - {}'.format(x)
    print 'J_op - {}'.format(J_op)
    print 'J_star - {}'.format(J_star)

