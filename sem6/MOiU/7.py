import numpy as np
import scipy.optimize as optimizer


B0 = np.array([[2, 1, 0, 4, 0, 3, 0, 0],
                [0, 4, 0, 3, 1, 1, 3, 2],
                [1, 3, 0, 5, 0, 4, 0, 4]], dtype=float)

B1 = np.array([[0, 0, 0.5, 2.5, 1, 0, -2.5, -2],
                [0.5, 0.5, -0.5, 0, 0.5, -0.5, -0.5, -0.5],
                [0.5, 0.5, 0.5, 0, 0.5, 1, 2.5, 4]], dtype=float)
B2 = np.array([[1, 2, -1.5, 3, -2.5, 0, -1, -0.5],
                [-1.5, -0.5, -1, 2.5, 3.5, 3, -1.5, -0.5],
                [1.5, 2.5, 1, 1, 2.5, 1.5, 3, 0]], dtype=float)
B3 = np.array([[0.75, 0.5, -1, 0.25, 0.25, 0, 0.25, 0.75],
                [-1., 1, 1, 0.75, 0.75, 0.5, 1, -0.75],
                [0.5, -0.25, 0.5, 0.75, 0.5, 1.25, -0.75, -0.25]], dtype=float)
B4 = np.array([[1.5, -1.5, -1.5, 2, 1.5, 0, 0.5, -1.5],
                [-0.5, -2.5, -0.5, -1, -2.5, 2.5, 1, 2],
                [-2.5, 1, -2, -1.5, -2.5, 0.5, 2.5, -2.5]], dtype=float)
B5 = np.array([[1, 0.25, -0.5, 1.25, 1.25, -0.5, 0.25, -0.75],
                [-1, -0.75, -0.75, 0.5, -0.25, 1.25, 0.25, -0.5],
                [0, 0.75, 0.5, -0.5, -1, 1, -1, 1]], dtype=float)
C = np.array([-1, -1, -1, -1, -2, 0, -2, -3], dtype=float)

c = np.array([[0, 60, 80, 0, 0, 0, 40, 0],
              [2, 0, 3, 0, 2, 0, 3, 0],
              [0, 0, 80, 0, 0, 0, 0, 0],
              [0, -2, 1, 2, 0, 0, -2, 1],
              [-4, -2, 6, 0, 4, -2, 60, 2]], dtype=float)
alpha = np.array([-51.75, -436.75, -33.7813, -303.375, -41.75], dtype=float)

x_star = np.array([1, 0, 0, 2, 4, 2, 0, 0], dtype=float)
x_base = np.array([0, 0, 0, 0, 0, 0, 0, 0], dtype=float)

D = np.dot(B0.T, B0)
d = np.array([np.dot(B1.T, B1),
              np.dot(B2.T, B2),
              np.dot(B3.T, B3),
              np.dot(B4.T, B4),
              np.dot(B5.T, B5)],
               dtype=float)
bounds = list(zip([-1, 0, -1, -1, -1, -1, 0, 0], [1, 1, 1, 1, 1, 1, 1, 1]))


def function(C, D, x, alpha = 0):
    a = np.dot(C, x)
    b = 0.5 * np.dot(x.T, np.dot(D, x))
    return a + b + alpha


def check_conditions(C, D, old_x, new_x, c, d, alpha):
    if function(C, D, new_x) >= function(C, D, old_x):
        return False

    for i in range(len(c)):
        if function(c[i], d[i], new_x, alpha[i]) > -10**-3:
            return False

    return True


x_optimal = None
func_to_opt = C + np.dot(D, x_star)

A = []
b = []

for i in range(len(c)):
    linear_part = np.sum(np.dot(c[i], x_star))
    quadratic_part = 0.5 * np.dot(x_star.T, (np.dot(d[i], x_star)))
    value = linear_part + quadratic_part + alpha[i]

    if np.abs(value) < 10**-3:
        A.append(c[i] + np.dot(d[i],x_star))
        b.append(0.)

if A:
    A = np.array(A)
    b = np.array(b).T
else:
    A = None
    b = None


result = optimizer.linprog(func_to_opt, A_ub=A, b_ub=b, bounds=bounds)

if (abs(result.fun) <= 0.0001):
    x_optimal = result.x
else:
    tmp = np.dot((x_base - x_star).T, func_to_opt)

    if tmp > 0:
        alpha_step = -result.fun / 2 * tmp
    else:
        alpha_step = 1

    t_step = 0.5
    while True:
        x_new = x_star + t_step * result.x + alpha_step * t_step * (x_base - x_star)
        x_old = x_star

        if not check_conditions(C, D, x_old, x_new, c, d, alpha):
            t_step *= 0.5
        else:
            x_optimal = x_new
            break

print "x", x_optimal
print "f(x) =", function(C, D, x_optimal)

