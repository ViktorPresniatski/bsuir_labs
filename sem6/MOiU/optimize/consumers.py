import numpy as np


def balance(producers, consumers, cost):
    producers_sum = np.sum(producers)
    consumers_sum = np.sum(consumers)

    if producers_sum == consumers_sum:
        return producers, consumers, cost

    if producers_sum > consumers_sum:
        consumers = np.append(consumers, producers_sum - consumers_sum)
        new_cost = np.zeros((cost.shape[0], cost.shape[1] + 1))
        new_cost[:, :-1] = cost
    else:
        producers = np.append(producers, consumers_sum - producers_sum)
        new_cost = np.vstack([cost, np.zeros_like(consumers)])

    return producers, consumers, new_cost


def begining_plan(producers, consumers):
    m = consumers.shape[0]
    n = producers.shape[0]

    current_producers = list(producers)
    current_consumers = list(consumers)

    i, j = 0, 0
    plan = np.zeros((n, m))
    basic_indicies = []

    flag = False
    while (i < m and j < n) or len(basic_indicies) <= m + n - 1:
        j = min(j, n -1)
        i = min(i, m - 1)
        basic_indicies.append((j, i))
        if current_consumers[i] <= current_producers[j] and not flag:
            plan[j, i] = current_consumers[i]
            current_producers[j] -= current_consumers[i]
            i += 1
            if i == m:
                flag = True
        else:
            plan[j, i] = current_producers[j]
            current_consumers[i] -= current_producers[j]
            j += 1
            if j == n:
                flag = True

    return plan, basic_indicies


def get_help_matrix(cost, indices):
    result = np.zeros_like(cost)

    for index in indices:
        result[index[0], index[1]] = cost[index[0], index[1]]

    return result


def eval_potentials(help_matrix, indices):
    u = np.zeros(help_matrix.shape[0])
    v = np.zeros(help_matrix.shape[1])

    u_found = [False for _ in range(help_matrix.shape[0])]
    v_found = [False for _ in range(help_matrix.shape[1])]

    u[0] = 0
    u_found[0] = True

    while not (all(u_found) and all(v_found)):
        print(u_found, v_found)
        for i, j in indices:
            if u_found[i] and not v_found[j]:
                v[j] = help_matrix[i, j] - u[i]
                v_found[j] = True

            if v_found[j] and not u_found[i]:
                u[i] = help_matrix[i, j] - v[j]
                u_found[i] = True

    return u, v


def eval_delta(help_matrix, u, v):
    m = help_matrix.shape[0]
    n = help_matrix.shape[1]

    working_matrix = np.zeros((m, n))
    for i in range(m):
        for j in range(n):
            working_matrix[i, j] = help_matrix[i, j]
            working_matrix[i, j] -= (u[i] + v[j])

    return working_matrix


def find_cycle(cost, indices, index):
    m = cost.shape[0]
    n = cost.shape[1]

    rows = [False for _ in range(m)]
    columns = [False for _ in range(n)]
    cycle_indices = list(indices)
    cycle_indices.append(index)
    while True:
        flag = False
        for i in range(m):
            if not rows[i]:
                count = 0

                for index in cycle_indices:
                    if index[0] == i and not columns[index[1]]:
                        count += 1

                if count <= 1:
                    flag = True
                    rows[i] = True

        for j in range(n):
            if not columns[j]:
                count = 0

                for index in cycle_indices:
                    if index[1] == j and not rows[index[0]]:
                        count += 1

                if count <= 1:
                    flag = True
                    columns[j] = True

        if not flag:
            break

    current_index = index
    i = 0
    result = []
    while True:
        for indices in cycle_indices:
            if (indices[i] == current_index[i] and indices[i ^ 1] != current_index[i ^ 1]
                    and not rows[indices[0]] and not columns[indices[1]]):
                result.append(indices)
                current_index = indices
                i ^= 1
                break

        if current_index == index:
            break

    return result


def method_of_potentials(cost, begining_plan, indices):
    while True:
        u, v = eval_potentials(cost, indices)

        delta = eval_delta(cost, u, v)

        if (delta >= 0).all():
            return (True, begining_plan, indices)

        min_index_nb = np.unravel_index(np.argmin(delta, axis=None), delta.shape)

        cycle_indices = find_cycle(cost, indices, min_index_nb)

        min_x = 1000
        for cycle_index in cycle_indices[::2]:
            if begining_plan[cycle_index] < min_x:
                min_x = begining_plan[cycle_index]
                min_index_b = cycle_index

        for i, cycle_index in enumerate(cycle_indices):
            if i % 2 == 1:
                begining_plan[cycle_index] += min_x
            else:
                begining_plan[cycle_index] -= min_x

        indices.remove(min_index_b)
        indices.append(min_index_nb)


def eval_common_cost(cost, plan):
    result = 0

    for i in range(plan.shape[0]):
        for j in range(plan.shape[1]):
            result += cost[i, j] * plan[i, j]

    return result


if __name__ == '__main__':
    producers = np.array([0, 0, 0, 5, 0])
    consumers = np.array([0, 2, 0, 3])
    cost = np.array([
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]])

    # producers = np.array([20, 11, 18, 27])
    # consumers = np.array([11, 4, 10, 12, 8, 9, 10, 4])
    # cost = np.array([
    #     [-3, 6, 7, 12, 6, -3, 2, 16],
    #     [4, 3, 7, 10, 0, 1, -3, 7],
    #     [19, 3, -2, 7, 3, 7, 8, 15],
    #     [1, 4, -7, -3, 9, 13, 17, 22]])

    producers, consumers, cost = balance(producers, consumers, cost)

    begining_plan, basic_indices = begining_plan(producers, consumers)

    result_flag, plan, ind = method_of_potentials(cost, begining_plan, basic_indices)

    print(plan)
    print("money", eval_common_cost(cost, plan))

