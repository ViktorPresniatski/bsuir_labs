import numpy as np
import argparse
import scipy.linalg as sla


def dual_simplex(A, c, b, y, Jb):
    m, n = A.shape
    Jn = list(set(range(n)) - set(Jb))

    B = np.linalg.inv(A[:, list(Jb)])
    delta = y @ A - c

    while True:
        pseudo_plan = B @ b
        
        if (pseudo_plan >= 0).all():
            result = np.zeros(n)
            i = 0
            for j in Jb:
                result[j] = pseudo_plan[i]
                i += 1
            return {'success': True, 'result': result, 'basic_indices': Jb}

        for j, i in enumerate(Jb):
            if pseudo_plan[j] < 0:
                jk = i
                k = j
                break

        mu = B[k, :] @ A
       
        steps = [[-1 * (delta[j] / mu[j]), j] for j in Jn if mu[j] < 0]
        if len(steps) == 0:
            return {'success': False}

        min_step, j0 = min(steps)

        y = y + min_step * B[k, :]
        delta = delta + min_step * mu

        save_index = Jb.index(jk)
        Jb.remove(jk)
        Jn.remove(j0)

        Jb = Jb[:save_index] + [j0] + Jb[save_index:]
        Jn.append(jk)

        M = np.identity(B.shape[0])

        z = B @ A[:, j0]
        zs = z[k]
        z[k] = -1
        z = -1 / zs * z
        M[:, k] = z
        B = np.dot(M, B)
        print("="*50)


if __name__ == '__main__':
    A = np.array([[-2, -1, 10, -7, 1, 0, 0, 2],
                  [-4, 2, 3, 0, 5, 1, -1, 0],
                  [1, 1, 0, 1, -4, 3, -1, 1]])
    b = np.array([-2, -5, 2])
    c = np.array([10, -2, -38, 16, -9, -9, -5, -7])

    y = np.array([-3, -2, -1])
    Jb = [1, 7, 4]

    result = dual_simplex(A, c, b, y, Jb)
    if result['success']:
        print(result['result'])
        print(result['basic_indices'])
    else:
        print ('No solution')

