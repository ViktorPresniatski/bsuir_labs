import numpy as np
import argparse
import scipy.linalg as sla


def quadratic_solver(A, c, b, D, x, Jb, J_star):
    skip = False
    
    while True:
        if not skip:
            c_x = np.dot(D, x) + c
            
            print("c", c_x)

            B = np.linalg.inv(A[:, Jb])
            
            u = -1 * np.dot(c_x[Jb], B)
            
            print("u", u)

            delta = np.dot(u, A) + c_x
            
            print("delta", delta) 

            if (abs(delta) < 1e-5).all():
                return {'success': True, 'result': (x, Jb, J_star)}

            for idx, value in enumerate(delta):
                if value < 0 and (idx not in J_star):
                    j0 = idx
                    value0 = value
                    break

        skip = False

        print("j0", j0)
        
        H_hight = np.hstack((D[J_star][:, J_star], A[:, J_star].T))
        H_low = np.hstack((A[:, J_star], np.zeros((A[:,J_star].shape[0], A[:,J_star].shape[0]))))
        H = np.vstack((H_hight, H_low))

        bb = np.hstack((D[j0][J_star], A[:, j0]))

        coeff = -1 * np.dot(np.linalg.inv(H), bb)
        l = np.zeros_like(x)
        l[j0] = 1
        j = 0
        for i in J_star:
            l[i] = coeff[j]
            j += 1

        print("l", l)

        steps = [(-1 * float(value[0]) / value[1], index) for index, value in enumerate(zip(x, l)) if (value[1] < 0 and index in J_star)]

        d = np.dot(np.dot(l.T, D), l)
        if d != 0:
            steps.append((float(abs(value0)) / d, j0))
        print("steps", steps)
        if len(steps) == 0:
            return {'success': False}
        step0, j_star = min(steps) 
        print(step0, j_star)

        for i in range(len(x)):
            if i in J_star:
                x[i] += float(step0) * float(l[i])
            elif i == j0:
                x[i] += step0
            else:
                x[i] = 0

        print("x", x)

        if j_star == j0:
            J_star.append(j0)
        elif j_star in (set(J_star) - set(Jb)):
            J_star.remove(j_star)
            skip = True        
            value0 += step0 * d
        else:
            s = Jb.index(j_star)
            print(s, Jb)

            is_remove = False
            for i in (set(J_star) - set(Jb)):
                print(i, s,  A[:, i])
                if (np.dot(B, A[:, i]))[s] != 0:
                    print("j+", i)
                    Jb.remove(j_star)
                    Jb.append(i)
                    
                    J_star.remove(j_star)
                    is_remove = True
                    skip = True
                    
                    value0 += step0 * d

                    break

            if not is_remove:
                Jb.remove(j_star)
                Jb.append(j0)

                J_star.remove(j_star)
                J_star.append(j0)


        print("value0", value0)
        print("J_star", J_star)
        print("Jb", Jb)
        print("=" * 40)

def eval_func(c, x, D):
    c_x = np.dot(c, x)
    D_x = 0.5 * np.dot(np.dot(x.T, D), x)
    return c_x + D_x

if __name__ == '__main__':
    A = np.array([[0., 2., 1., 4., 3., 0., -5., -10.],
                  [-1., 3., 1., 0., 1., 3., -5., -6.],
                  [1., 1., 1., 0., 1., -2., -5., 8.]])
    b = np.array([6., 4., 14.])
    c = np.array([1., 3., -1., 3., 5., 2., -2., 0.])

    D = np.array([[1,0,0,0,0,0,0,0],
                  [0,1,0,0,0,0,0,0],
                  [0, 0, 0, 0, 0, 0, 0, 0],
                  [0,0,0,1,0,0,0,0],
                  [0, 0, 0, 0, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 1, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 1]])


    x = np.array([0., 2., 0., 0., 4., 0., 0. ,1.])
    Jb = [1, 4, 7]
    J_star = [1, 4, 7]

    # A = np.array([[11., 0., 0., 1., 0., -4., -1., 1.],
    #               [1., 1., 0., 0., 1., -1., -1., 1.],
    #               [1., 1., 1., 0., 1., 2., -2., 1.]])
    # b = np.array([8., 2., 5.])
    # d = np.array([6., 10., 9.])

    # B = np.array([[1., -1., 0., 3., -1., 5., -2., 1.],
    #               [2., 5., 0., 0., -1., 4., 0., 0.],
    #               [-1., 3., 0., 5., 4., -1., -2., 1.]])

    # D = np.dot(B.T, B)
    # c = -1* np.dot(d.T, B)

    # x = np.array([0.7273, 1.2737, 3., 0, 0, 0., 0. ,0.])
    # Jb = [0, 1, 2]
    # J_star = [0, 1, 2]


    # A = np.array([[2., -3., 1., 1., 3., 0., 1., 2.],
    #               [-1., 3., 1., 0., 1., 4., 5., -6.],
    #               [1., 1., -1., 0., 1., -2., 4., 8.]])
    # b = np.array([8., 4., 14.])

    # B = np.array([[1., 0., 0., 3., -1., 5., 0., 1.],
    #               [2., 5., 0., 0., 0., 4., 0., 0.],
    #               [-1., 9., 0., 5., 2., -1., -1., 5.]])

    # D = np.dot(B.T, B)
    # c = np.array([-13., -217., 0., -117., -27., -71., 18., -99.])
    
    # x = np.array([0., 2., 0., 0, 4., 0., 0. ,1.])
    # Jb = [1, 4, 7]
    # J_star = [1, 4, 7]

    result = quadratic_solver(A, c, b, D, x, Jb, J_star)
    if result['success']:
        print("X", result['result'][0])
        print("func", eval_func(c, x, D))
    else:
        print ('No solution')

