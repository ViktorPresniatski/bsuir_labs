import numpy as np
import scipy.optimize as optimizer


def eval_derivative(C, D, x):
    return C + np.dot(D,x)


def eval_function(C, D, x, alpha = 0):
    a = np.dot(C, x)
    b = 0.5 * np.dot(x.T, np.dot(D, x))
    return a + b + alpha


def check_all(C, D, old_x, new_x, c, d, alpha):
    if eval_function(C, D, new_x) >= eval_function(C, D, old_x):
        return False

    for i in range(len(c)):
        if eval_function(c[i], d[i], new_x, alpha[i]) > -10**-3:
            return False

    return True


def get_conditions_for_linear_problem(c, x_star, alpha, d):
    A = []
    b = []

    for i in range(len(c)):
        linear_part = np.sum(np.dot(c[i], x_star))
        quadratic_part = 0.5 * np.dot(x_star.T, (np.dot(d[i], x_star)))
        value = linear_part + quadratic_part + alpha[i]

        if np.abs(value) < 10**-3:
            A.append(eval_derivative(c[i], d[i], x_star))
            b.append(0.)

    if A:
        A = np.array(A)
        b = np.array(b).T
    else:
        A = None
        b = None

    return A, b


def eval_alpha_step(x_base, x_star, function_to_optimize, result):
    tmp = np.dot((x_base - x_star).T, function_to_optimize)

    if tmp > 0:
        alpha_step = -result.fun / 2 * tmp
    else:
        alpha_step = 1

    return alpha_step


def optimize(C, D, d, c, alpha, x_base, x_star, bounds):
    function_to_optimize = eval_derivative(C, D, x_star)

    A, b = get_conditions_for_linear_problem(c, x_star, alpha, d)

    result = optimizer.linprog(function_to_optimize,
                               A_ub=A,
                               b_ub=b,
                               bounds=bounds)

    if (abs(result.fun) <= 10**-3):
        return result.x
    else:
        alpha_step = eval_alpha_step(x_base, x_star, function_to_optimize, result)
        t_step = 0.5
        while True:
            x_new = x_star + t_step * result.x + alpha_step * t_step * (x_base - x_star)
            x_old = x_star

            if not check_all(C, D, x_old, x_new, c, d, alpha):
                t_step *= 0.5
            else:
                return x_new


if __name__ == '__main__':

    B_0 = np.array([[2, 1, 0, 4, 0, 3, 0, 0],
                    [0, 4, 0, 3, 1, 1, 3, 2],
                    [1, 3, 0, 5, 0, 4, 0, 4]],
                   dtype=float)
    B_1 = np.array([[0, 0, 0.5, 2.5, 1, 0, -2.5, -2],
                    [0.5, 0.5, -0.5, 0, 0.5, -0.5, -0.5, -0.5],
                    [0.5, 0.5, 0.5, 0, 0.5, 1, 2.5, 4]],
                   dtype=float)
    B_2 = np.array([[1, 2, -1.5, 3, -2.5, 0, -1, -0.5],
                    [-1.5, -0.5, -1, 2.5, 3.5, 3, -1.5, -0.5],
                    [1.5, 2.5, 1, 1, 2.5, 1.5, 3, 0]],
                   dtype=float)
    B_3 = np.array([[0.75, 0.5, -1, 0.25, 0.25, 0, 0.25, 0.75],
                    [-1., 1, 1, 0.75, 0.75, 0.5, 1, -0.75],
                    [0.5, -0.25, 0.5, 0.75, 0.5, 1.25, -0.75, -0.25]],
                   dtype=float)
    B_4 = np.array([[1.5, -1.5, -1.5, 2, 1.5, 0, 0.5, -1.5],
                    [-0.5, -2.5, -0.5, -1, -2.5, 2.5, 1, 2],
                    [-2.5, 1, -2, -1.5, -2.5, 0.5, 2.5, -2.5]],
                   dtype=float)
    B_5 = np.array([[1, 0.25, -0.5, 1.25, 1.25, -0.5, 0.25, -0.75],
                    [-1, -0.75, -0.75, 0.5, -0.25, 1.25, 0.25, -0.5],
                    [0, 0.75, 0.5, -0.5, -1, 1, -1, 1]],
                   dtype=float)
    C = np.array([-1, -1, -1, -1, -2, 0, -2, -3],
                   dtype=float)

    c = np.array([[0, 60, 80, 0, 0, 0, 40, 0],
                  [2, 0, 3, 0, 2, 0, 3, 0],
                  [0, 0, 80, 0, 0, 0, 0, 0],
                  [0, -2, 1, 2, 0, 0, -2, 1],
                  [-4, -2, 6, 0, 4, -2, 60, 2]],
                   dtype=float)
    alpha = np.array([-51.75, -436.75, -33.7813, -303.375, -41.75],
                   dtype=float)

    x_star = np.array([1, 0, 0, 2, 4, 2, 0, 0],
                   dtype=float)
    x_base = np.array([0, 0, 0, 0, 0, 0, 0, 0],
                   dtype=float)

    D = np.dot(B_0.T, B_0)
    d = np.array([np.dot(B_1.T, B_1),
                  np.dot(B_2.T, B_2),
                  np.dot(B_3.T, B_3),
                  np.dot(B_4.T, B_4),
                  np.dot(B_5.T, B_5)],
                   dtype=float)
    bounds = list(zip([-1, 0, -1, -1, -1, -1, 0, 0], [1, 1, 1, 1, 1, 1, 1, 1]))

    x_optimal = optimize(C, D, d, c, alpha, x_base, x_star, bounds)
    print("x", x_optimal)
    print("f(x) =", eval_function(C, D, x_optimal))

