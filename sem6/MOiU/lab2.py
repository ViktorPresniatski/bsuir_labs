import numpy as np
import numpy.linalg as la
from numpy.linalg import LinAlgError
from sys import exit

INF = float('inf')

def basis_cost(c, J_b):
    return np.take(c, J_b)

def basis_plan(x, J_b):
    x_ = [x[j] for j in J_b]
    return np.array(x_, dtype=np.float)

def vector_of_potentials(c_b, B):
    return np.dot(c_b, B)

def estimates(U, A, c, J_n, J):
    delta = []
    for j in J:
        if j in J_n:
            delta.append(np.dot(U, A[:, j]) - c[j])
        else:
            delta.append(0)
    return delta


def inverse_basis_matrix(A_b):
    try:
        la.inv(A_b)
    except LinAlgError:
        print "matrix can't have inverted one"
        exit()
    except Exception:
        exit()
    return la.inv(A_b)


def calc_new_basis_indexes(J_b, j_0, s):
    J_b[s] = j_0
    return J_b

def nonbasis_indexes(J, J_b):
    return [j for j in J if j not in J_b]

def basis_matrix(A, J_b):
    return np.take(A, J_b, axis=1)

def check_delta(delta, J_n, x, d_l, d_r):
    for i in J_n:
        if x[i] == d_l[i] and delta[i] >= 0 or x[i] == d_r[i] and delta[i] <= 0:
            pass
        else:
            return False, i
    return True, -1

def calc_steps(Z, x, m, d_l, d_r, J_b):
    theta = []
    for i in range(m):
        if Z[i] == 0:
            theta.append(INF)
        else:
            if Z[i] > 0:
                theta.append((d_r[J_b[i]] - x[J_b[i]]) / Z[i])
            else:
                theta.append((d_l[J_b[i]] - x[J_b[i]]) / Z[i])
    return theta

def calc_new_plan(x, J_b, J_n, j_0, Z, theta_0, m, Z_0):
    for i in range(m):
        x[J_b[i]] += Z[i] * theta_0
    x[j_0] += theta_0*Z_0
    return x

n = 7
m = 3
J = [i for i in range(n)]
c = np.array([1,2,3,4,5,6,7], dtype=float)
b = np.array([-12, 13, 29], dtype=float)
A = np.array([[1, 2, 0, -1, 0, -2, 1],
             [0, -1, 1, -1, 0, 3, 2],
             [0, 4, 0, -1, 1, 6, -4]], dtype=float)
x = np.array([0.0,0.40540541,4.0,4.0,5.0,4.43243243, 0.05405405], dtype=float)
J_b = [0, 2, 4]
d_l = [0, 0, 0, 0, 0 , 0, 0]
d_r = [5, 6, 4, 4, 5, 5, 6]

J_n = nonbasis_indexes(J, J_b)
A_b = basis_matrix(A, J_b)
B = inverse_basis_matrix(A_b)

while True:
    x_b = basis_plan(x, J_b)
    print "x_b", x_b
    c_b = basis_cost(c, J_b)
    print "c_b", c_b
    U = vector_of_potentials(c_b, B)
    print "U", U
    delta = estimates(U, A, c, J_n, J)
    print "delta", delta
    flag, j_0 = check_delta(delta, J_n, x, d_l, d_r)
    print "j_0", j_0

    if flag:
        print "\n"
        print "Optimal plan - {}".format(x)
        print "c -", np.dot(c, x)
        print "b - {}".format(np.dot(A, x))
        print "J_b - {}".format(J_b)
        break
    else:
        if x[j_0] == d_l[j_0]:
            Z_0 = 1
        else:
            Z_0 = -1
        Z = -1*Z_0*np.dot(B, A[:, j_0])
        print "Z", Z
        theta = calc_steps(Z, x, m, d_l, d_r, J_b)
        theta_0 = min(theta)
        print "theta", theta
        print "theta_0", theta_0 
        if theta_0 == INF:
            print('task hasn\'t got any solution - the objective function is not bounded from above by the set of plans.')
            break
        else:
            s = theta.index(theta_0)
            print "s", s
            x = calc_new_plan(x, J_b, J_n, j_0, Z, theta_0, m, Z_0)
            print "new plan", x
            print (np.dot(c, x))
            J_b = calc_new_basis_indexes(J_b, j_0, s)
            J_n = nonbasis_indexes(J, J_b)
            A_b = basis_matrix(A, J_b)
            B = inverse_basis_matrix(A_b)