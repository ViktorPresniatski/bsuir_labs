import numpy as np


def balance(a, b, cost):
    a_sum = np.sum(a)
    b_sum = np.sum(b)

    if a_sum == b_sum:
        return a, b, cost

    if a_sum > b_sum:
        b = np.append(b, a_sum - b_sum)
        new_cost = np.zeros((cost.shape[0], cost.shape[1] + 1))
        new_cost[:, :-1] = cost
    else:
        a = np.append(a, b_sum - a_sum)
        new_cost = np.vstack([cost, np.zeros_like(b)])

    return a, b, new_cost


a = np.array([100, 300, 300])
b = np.array([300, 200, 200])
c = np.array([[8, 4, 1],
              [8, 4, 3],
              [9, 7, 5]])

# a = np.array([20, 30, 25])
# b = np.array([10, 10, 10, 10, 10])
# c = np.array([[2, 8, -5, 7, 10],
#               [11, 5, 8, -8, -4],
#               [1, 3, 7, 4, 2]])

# a = np.array([20, 11, 18, 27])
# b = np.array([11, 4, 10, 12, 8, 9, 10, 4])
# c = np.array([
#             [-3, 6, 7, 12, 6, -3, 2, 16],
#             [4, 3, 7, 10, 0, 1, -3, 7],
#             [19, 3, -2, 7, 3, 7, 8, 15],
#             [1, 4, -7, -3, 9, 13, 17, 22]])

a = np.array([15,12,18,20])
b = np.array([5,5,10,4,6,20,10,5])
c = np.array([[-3,10,70,-3,7,4,2,-20],
              [3,5,8,8,0,1,7,-10],
              [-15,1,0,0,13,5,4,5],
              [1,-5,9,-3,-4,7,16,25]])

a, b, c = balance(a, b, c)

M = c.shape[0]
N = c.shape[1]
x = np.zeros((M, N))
J_b = []


def J_not(J_b):
    return [(i, j) for i in range(M) for j in range(N) if (i, j) not in J_b]


def solve_system(J_b):
    u = {0: 0}
    v = {}
    J = J_b[:] # clone
    for i, j in J:
        if u.get(i) is not None:
            v[j] = c[i][j] - u[i]
        elif v.get(j) is not None:
            u[i] = c[i][j] - v[j]
        else:
            J.append((i, j))

    return u, v
    

def find_cycle(J, j0):
    axles = [range(M), range(N)]
    Jb = J[:]

    def counter(axis=0):
        ans = {}
        for j in Jb:
            ans[j[axis]] = ans.get(j[axis], 0) + 1               
        return ans

    def remove_from_Jb(i, axis=0):
        for j in Jb:
            if j[axis] == i: Jb.remove(j)

    prev_deleted = True
    next_axis = 0
    while True:
        deleted = False
        cnt = counter(axis=next_axis)

        for i in axles[next_axis][:]:
            if cnt[i] == 1:
                axles[next_axis].remove(i)
                remove_from_Jb(i, axis=next_axis)
                deleted = True

        if not axles[next_axis]:
            return list()

        if not deleted and not prev_deleted:
            break

        prev_deleted = deleted
        next_axis ^= 1

    cycle = {}
    sign = 1
    j = j0
    while any(Jb):
        cycle[j] = sign
        Jb.remove(j)
        pair = filter(lambda jj: j[sign] == jj[sign], Jb)
        if not pair:
            break
        j = pair[0]
        sign ^= 1

    return cycle

# first phase

i = j = 0
while True:
    J_b.append((i, j))
    delta = min(a[i], b[j])
    x[i][j] = delta
    a[i] -= delta
    b[j] -= delta

    if a[i] == 0 and i < a.size - 1:
        i += 1
    elif b[j] == 0 and j < b.size - 1:
        j += 1
    else:
        break


#second phase

while True:
    print x
    print "J_b begin - {}".format(J_b)

    u, v = solve_system(J_b)
    print "u, v - {}, {}".format(u, v)

    J_n = J_not(J_b)

    delta = {(i, j): c[i][j] - u[i] - v[j] for i, j in J_n}
    if all([d >= 0 for d in delta.values()]):
        print 'Answer:\n{}'.format(x)
        break

    j0 = None
    j0_value = min(delta.values())
    for k, v in delta.iteritems():
        if v == j0_value:
            j0 = k

    # j0 = min(J_n)
    J_b.append(j0)
    print "J_b added - {}".format(J_b)
    cycle = find_cycle(J_b, j0)
    print "Cycle - {}".format(cycle)

    # G = build_graph(J_b)
    # print "Graph - {}".format(G)
    # cycle = find_cycle(G, j0)
    # print "Cycle - {}".format(cycle)
    # exit(0)
    
    minuses = [k for k, v in cycle.iteritems() if v == 0]
    print "Minuses - {}".format(minuses)
    j_star = min(sorted(minuses), key=lambda el: x[el[0]][el[1]])
    theta = x[j_star[0]][j_star[1]]

    for (i, j), sign in cycle.iteritems():
        if sign == 1:
            x[i][j] += theta
        else:
            x[i][j] -= theta

    # min_zero_in_cycle = min([(i, j) for i, j in cycle if x[i][j] == 0])
    J_b.remove(j_star)
