import numpy as np
from inverse import inverse


INFINITY = 99999999999


A = np.array([[1,2,0,-1,0,-2,1],
              [0,-1,1,-1,0,3,2],
              [0,4,0,-1,1,6,-4]])

b = np.array([-13,13,29])
C = np.array([1,1,1,1,1,1,1])
x = np.array([1,0,2,4,3,5,0], dtype=np.float)
d_min = np.array([0,0,0,0,0,0,0], dtype=np.float)
d_max = np.array([5,6,4,4,5,5,6], dtype=np.float)
J = [1,3,5]

# A = np.array([[1,1,-1,1,1,0],
#               [1,14,10,-10,0,1]])

# b = np.array([2,24])
# C = np.array([0,0,0,0, -1, -1])
# x = np.array([0,0,0,0,2,24])
# J = [5,6]

def second_phase(A, b, C, x, J):
    def J_not():
        return filter(lambda j: j not in J, range(len(C) + 1)[1:])

    def check_delta(x, delta):
        for i in map(lambda j: j - 1, J_not()):
            if x[i] == d_min[i] and delta[i] >= 0 or x[i] == d_max[i] and delta[i] <= 0:
                pass
            else:
                return False, i + 1
        return True, -1

    A_b = A[:,map(lambda j: j - 1, J)]
    A_b_inv = np.linalg.inv(A_b)

    while True:
        print "\nITERATION"
        print "J - {}".format(J)
        print "x - {}".format(x)
        C_b = C[map(lambda j: j - 1, J)]

        u_t = np.dot(C_b, A_b_inv)
        delta = np.dot(u_t, A) - C
        print "delta - {}".format(delta)

        #condition of exiting
        # print J_not()
        # arr1 = filter(lambda j: abs(x[j] - d_min[j]) < 0.00001, map(lambda j: j - 1, J_not()))
        # arr2 = filter(lambda j: abs(x[j] - d_max[j]) < 0.00001, map(lambda j: j - 1, J_not()))
        # delta_for_min = delta[arr1]
        # delta_for_max = delta[arr2]
        # print arr1
        # print delta_for_min
        # print '\n'
        # print arr2
        # print delta_for_max

        # delta_not_basis = np.take(delta, map(lambda j: j - 1, J_not()))
        # delta_min = filter(lambda d: d < 0, delta_for_min)
        # delta_max = filter(lambda d: d > 0, delta_for_max)
        flag, j0 = check_delta(x, delta)
        if flag:
            print "{} - optimal plan for second lab\n{} - J_b\n".format(x, J)
            print "Goal function - {}".format(sum(C * x))
            return x, J, A_b_inv

        # indices = filter(lambda j: delta[j - 1] < 0, J_not())
        # mapped = map(lambda j: abs(sum(A[:, j - 1])), indices)
        # j0 = indices[mapped.index(max(mapped))]
        if x[j0 - 1] == d_min[j0 - 1]:
            z0 = 1.0
            # j0 = filter(lambda j: delta[j] < -0.00001, arr1)[0] + 1
        else:
            z0 = -1.0
            # j0 = filter(lambda j: delta[j] > 0.00001, arr2)[0] + 1
        # j0 = filter(lambda j: delta[j - 1] < 0, J_not())[0]
        
        # import pdb; pdb.set_trace()
        z = -1.0 * z0 * np.dot(A_b_inv, A[:, j0 - 1])
        print "j0 - {}".format(j0)
        print "z - {}".format(z)


        def count_teta(z_i, i):
            if abs(z_i) < 0.00001:
                return INFINITY
            elif z_i > 0.000001:
                return 1.0 * (d_max[J[i] - 1] - x[J[i] - 1]) / z_i
            elif z_i < -0.0000001:
                return 1.0 * (d_min[J[i] - 1] - x[J[i] - 1]) / z_i

        teta = [count_teta(z_i, i) for i, z_i in enumerate(z)]
        # teta.insert(0, )
        print "theta - {}".format(teta)
        teta_0 = min(teta)
        print "theta_0 - {}".format(teta_0)
        # import pdb; pdb.set_trace()
        if teta_0 == INFINITY:
            print "The objective function is not bounded above on the set of feasible plans"
            return None

        i = teta.index(teta_0)

        # x[j0 - 1] = teta_0
        # for j in J_not():
            # x[j - 1] = 
       
        J[i] = j0
        # z.insert(0, z0)
        # print "z - {}".format(list(enumerate(z)))
        x[j0 - 1] += teta_0 * z0
        for i, z_i in enumerate(z):
            # import pdb; pdb.set_trace()
            x[J[i] - 1] += teta_0 * z_i




        # A_b_old = A_b[:]
        # A_b_inv_old = A_b_inv[:]
        A_b = A[:,map(lambda j: j - 1, J)]
        A_b_inv = np.linalg.inv(A_b)
        # A_b_inv = inverse(A_b_old, A_b_inv_old, A[:, j0 - 1], J.index(j0) + 1)


if __name__ == '__main__':
    second_phase(A, b, C, x, J)