import numpy as np
import logging


logger = logging.getLogger('inv')
logger.addHandler(logging.StreamHandler())


def inverse(a, b, x, i, log=False):
    if log:
        logger.setLevel(logging.INFO)
    l = np.dot(b, x)
    logger.info("l = {}".format(l))

    l_i = l[i - 1]
    if l_i == 0:

        logger.info("Not reversible")
        return None
    else:
        logger.info("Reversible")

        l[i - 1] = -1
        ln = (1.0 * (-1) / l_i) * l
        logger.info("ln = {}\n".format(ln))

        e = np.eye(len(b))
        e[i - 1] = ln
        q = e.transpose()
        logger.info("Q: \n{}\n".format(q))

        ans = np.dot(q, b)
        logger.info("Answer: \n{}".format(ans))
        return ans


if __name__ == '__main__':
    a = np.array([[1,-1, 0],
                [0, 1, 0],
                [0, 0, 1]])

    b = np.array([[1, 1, 0],
                [0, 1, 0],
                [0, 0, 1]])

    x = np.array([1, 0, 1])
    i = 3

    inverse(a, b, x, i, log=True)