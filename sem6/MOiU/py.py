
def solve_system(indices):
    u = np.zeros(c.shape[0])
    v = np.zeros(c.shape[1])

    u_found = [False for _ in range(c.shape[0])]
    v_found = [False for _ in range(c.shape[1])]

    u[0] = 0
    u_found[0] = True

    while not (all(u_found) and all(v_found)):
        print(u_found, v_found)
        print(u, v)
        for i, j in indices:
            if u_found[i] and not v_found[j]:
                v[j] = c[i, j] - u[i]
                v_found[j] = True

            if v_found[j] and not u_found[i]:
                u[i] = c[i, j] - v[j]
                u_found[i] = True

    return u, v


def build_graph(J):
    g = {}
    for i0, j0 in J:
        g[(i0, j0)] = [(i, j) for i, j in J if (i == i0 and j != j0) or (j == j0 and i != i0)]
        children = [(i, j) for i, j in g[(i0, j0)] if i == i0 and j > j0]
        if children:
            m = min(children, key=lambda x: x[1])[1]
            for j in [j for i, j in children if j > m]:
                g[(i0, j0)].remove((i0, j))

        children = [(i, j) for i, j in g[(i0, j0)] if i == i0 and j < j0]
        if children:
            m = max(children, key=lambda x: x[1])[1]
            for j in [j for i, j in children if j < m]:
                g[(i0, j0)].remove((i0, j))

        children = [(i, j) for i, j in g[(i0, j0)] if i > i0 and j == j0]
        if children:
            m = min(children, key=lambda x: x[0])[0]
            for i in [i for i, j in children if i > m]:
                g[(i0, j0)].remove((i, j0))

        children = [(i, j) for i, j in g[(i0, j0)] if i < i0 and j == j0]
        if children:
            m = max(children, key=lambda x: x[0])[0]
            for i in [i for i, j in children if i < m]:
                g[(i0, j0)].remove((i, j0))

    return g


def find_cycle(g, j0):
    color = {}
    parents = {}
    cycle_edges = [None, None]

    def dfs(node, parent=None):
        color[node] = 'gray'
        for to in g[node]:
            if to == parent:
                continue 
            if color.get(to) is None:
                parents[to] = node
                if dfs(to, node):
                    return True
            elif color[to] == 'gray':
                cycle_edges[0] = to
                cycle_edges[1] = node
                return True
        color[node] = 'black'
        return False

    dfs(j0)

    print "=============================="

    print cycle_edges
    print parents

    cycle_cycle = [cycle_edges[0]]    
    v = cycle_edges[1]
    while v != cycle_edges[0]:
        cycle_cycle.append(v)
        if parents.get(parents[v]) and \
        (parents[parents[v]][0] == v[0] or parents[parents[v]][1] == v[1]):
            os = parents[parents[v]][0] == v[0]
            check = lambda ver: parents[ver][0] == v[0] if os else parents[ver][1] == v[1]
            ver = parents[v]
            while parents.get(ver) and check(ver):
                ver = parents[ver]
            v = ver
        else:
            v = parents[v]

    print cycle_cycle
    
    print "=============================="

    cycle = {}
    sign = 0
    for c in cycle_cycle:
        sign ^= 1
        cycle[c] = sign

    return cycle