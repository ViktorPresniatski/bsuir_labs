import numpy as np
from second_phase import second_phase
from simplex import simplex

def J_not(J_b, J):
    return [ i for i in J if i not in J_b]


# A_org = np.array([[1,3,3,1],
#               [2,0,3,-1]])

# b_org = np.array([3,4])
# C_org = np.array([1,-5,-1,1])

A_org = np.array([[1,2,3],
                  [2,4,6]])
b_org = np.array([0,0])

# A_org = np.array([[1,1,-1,1],
#                   [1,14,10,-10]])

# b_org = np.array([2,24])
# C_org = np.array([1,2,3,-4])

# A_org = np.array([[-2, -1, 1, -7, 0, 0, 0, 2],
#               [4,2,-1,0,1,5,-1,-5],
#               [1,11,0,1,0,3,1,1]])

# b_org = np.array([-2,14,4])
# C_org = np.array([6, -9, 5, -2, 6, 0, -1, 3])

while True:
    n = A_org.shape[1]
    m = A_org.shape[0]

    A_serv = np.hstack((A_org, np.eye(m)))
    C_serv = np.array([0.0] * n + [-1.0] * m)
    x = np.array([0.0] * (n + m))
    J_b = [0.0] * m

    for i in range(len(b_org)):
        x[n + i] = b_org[i]
        J_b[i] = n + i + 1

    print A_serv, b_org, C_serv, x, J_b
    x, J_b, A_inv = second_phase(A_serv, b_org, C_serv, x, J_b)
    # print x, J_b -

    if any(x[n:]):  # case 1
        print "Tasks is incompatible"
        exit(0)
    else:  # case 2
        x_bas = x[:n]
        if not any(filter(lambda j: j > n, J_b)):  # case 2.1
            print "{} - answer for third lab \n{} - J_b\n".format(x_bas, J_b)
            break
        else:  # case 2.2
            i = filter(lambda j: j > n, J_b)[0] - n
            l = list()
            j_not_basis = []
            for j_not in J_not(J_b, range(1, len(x_bas) + 1)):
                j_not_basis.append(j_not)
                # A_inv = np.linalg.inv(A_serv[:, map(lambda j: j - 1, J_b)])
                l.append(np.dot(A_inv, A_serv[:, j_not - 1]))
            
            indices = filter(lambda ind: l[ind][i - 1] != 0, range(len(l)))
            if any(indices):  # case 2.2.1
                J_b[J_b.index(n + i)] = j_not_basis[indices[0]]
            else:  # case 2.2.2
                b_org = np.delete(b_org, i - 1)
                A_org = np.delete(A_org, i - 1, axis=0)

# second_phase(A_org, b_org, C_org, x_bas, J_b)