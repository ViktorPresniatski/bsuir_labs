import numpy as np
import random


def J_not(J_b):
    return [i for i in range(len(C)) if i not in J_b]


# A = np.array([[-2, -1, -4, 1, 0],
#               [-2, -2, -2, 0, 1]])
# C = np.array([-4, -3, -7, 0, 0])

# b = np.array([-1, -1.5])
# J_b = [3, 4]

# A = np.array([[-2,-1,1,-7,0,0,0,2],
#               [4,2,1,0,1,5,-1,-5],
#               [1,1,0,-1,0,3,-1,1]])

# C = np.array([2, 2, 1, -10, 1, 4, -2, -3])
# b = np.array([-2, 4, 3])
# J_b = [2,5,7]

# A = np.array([[-2,-1,1,-7,1,0,0,2],
#               [-4,2,1,0,5,1,-1,5],
#               [1,1,0,-1,0,3,-1,1]])

# C = np.array([-12,2,2,-6,10,-1,-9,8])
# b = np.array([-2,4,-2])
# J_b = [2,4,6]

# A = np.array([[-2, -1, 10, -7, 1, 0, 0, 2],
#                   [-4, 2, 3, 0, 5, 1, -1, 0],
#                   [1, 1, 0, 1, -4, 3, -1, 1]])
# b = np.array([-2, -5, 2])
# C = np.array([10, -2, -38, 16, -9, -9, -5, -7])

# y = np.array([-3, -2, -1])
# J_b = [2, 8, 5]

A = np.array([[3,-1,10,-7,1,0,0,2],
              [7,-2,14,8,0,12,-11,0],
              [1,1,0,1,-4,3,-1,1]])
b = np.array([2, 5, -2])
C = np.array([36, -12, 66,76,-5,77,-76,-7])

# y = np.array([-3, -2, -1])
J_b = [7,8,4]


J_b = [j - 1 for j in J_b]
A_b = A[:, J_b]
A_b_inv = np.linalg.inv(A_b)

C_b = C[J_b]
y = np.dot(C_b, A_b_inv) # first time, than we will count by formula
delta = np.dot(y, A) - C

while True:
    
    capa_B = np.dot(A_b_inv, b)
    if all([c >= 0 for c in capa_B]):
        result = np.zeros(len(C))
        result[J_b] = capa_B
        print "x = {}\nJ_b = {}".format(result, [j + 1 for j in J_b])
        break

    for j, i in enumerate(J_b):
        if capa_B[j] < 0:
            j_k = i
            k  = j
            break

    mu = np.dot(A_b_inv[k, :], A)
    sigma = [[-1.0 * delta[j] / mu[j], j] for j in J_not(J_b) if mu[j] < 0]
    
    if len(sigma) == 0:
        print "Tasks is incompatible"
        break

    sigma_0, j_0 = min(sigma)

    y = y + np.dot(sigma_0, A_b_inv[k, :])
    delta = delta + np.dot(sigma_0, mu)
    J_b[k] = j_0
    A_b = A[:, J_b]
    A_b_inv = np.linalg.inv(A_b)
