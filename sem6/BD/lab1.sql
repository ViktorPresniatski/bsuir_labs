DROP TABLE MyTable;

CREATE TABLE MyTable (
  id number, 
  val number
);
/

BEGIN
  FOR i IN 1..10000
  LOOP
    INSERT INTO MyTable VALUES(i, round(DBMS_RANDOM.RANDOM()));
  END LOOP;
END;
/

CREATE OR REPLACE FUNCTION even_odd_function RETURN VARCHAR2 AS 
 count_even number;
 count_all number;
BEGIN 
    SELECT COUNT(*) INTO count_even FROM MYTABLE WHERE MOD(val,2)=0;
    SELECT COUNT(*) INTO count_all FROM MYTABLE;
    IF count_even = (count_all - count_even) THEN
        RETURN 'EQUAL';
    END IF;
   
    IF count_even > (count_all - count_even) THEN
        RETURN 'TRUE';
    ELSE
        RETURN 'FALSE';
    END IF;
END;
/

CREATE OR REPLACE FUNCTION generate_operator_insert(id IN number)
RETURN varchar2 AS res varchar(100);
BEGIN
  res := 'INSERT INTO MyTable VALUES (' || to_char(id) || ', ROUND(DBMS_RANDOM.RANDOM()));';
  RETURN res;
END;
/

CREATE OR REPLACE PROCEDURE insert_procedure(record_id IN number, record_value IN number) AS
BEGIN
  INSERT INTO MyTable VALUES (record_id, record_value);
END;
/

CREATE OR REPLACE PROCEDURE update_procedure(record_id IN number, record_value IN number) AS
BEGIN
  UPDATE MyTable SET val = record_value WHERE id = record_id;
END;
/

CREATE OR REPLACE PROCEDURE delete_procedure(record_id IN number) AS
BEGIN
  DELETE FROM MyTable WHERE id = record_id;
END;
/

BEGIN
  DBMS_OUTPUT.PUT_LINE(even_odd_function());
--  DBMS_OUTPUT.PUT_LINE(generate_operator_insert(1));
--  insert_procedure(10001, 12);
--  update_procedure(10001, 13);
--  delete_procedure(10001);
END;

