CREATE TABLE STUDENTS (
	ID NUMBER,
	NAME NUMBER,
	GROUP_ID NUMBER
);
/

select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='STUDENTS';


CREATE OR REPLACE PROCEDURE insert_into_table(table_name in VARCHAR, time_limit IN NUMBER) IS
    query_string VARCHAR(32000);
    cols VARCHAR;
    values_int VARCHAR;
    value_int NUMBER;
BEGIN
  
  cols := '';
  values_int := '';
  FOR column_name IN (
    select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME=table_name
  )
  LOOP
    cols := cols || ',' || column_name;
    value_number := ROUND(DBMS_RANDOM.VALUE(1,1000)); 
    values_int := values_int || ',' || (value_number);
  END LOOP;

  query_string := 'INSERT INTO' || table_name || '(' || cols || ')' || 'VALUES (' || values_int || ');';
  EXECUTE IMMEDIATE query_string;
END;
/

BEGIN
  insert_into_table('STUDENTS', 3);
END;
/
