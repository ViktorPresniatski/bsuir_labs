$stdout = File.open("output.txt", "a")
threads = []
lock = Mutex.new

def put_string(str)
  str.each_char do |ch|
    sleep(0.01)
    print ch;
    $stdout.flush
  end
  print "\n"
end

loop do
  input = gets.chomp
  input == 'q' and exit(0)

  if input == '+'
    threads << Thread.new do 
      str = ('a'.ord + rand(26)).chr * rand(10..20)
      loop do
        lock.synchronize { put_string(str) }
      end
    end
  elsif input == '-'
    threads.pop.kill
  end  
end