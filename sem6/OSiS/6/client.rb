require 'socket'


class UDPClient
  attr_reader :socket

  def initialize(host, port)
    @host = host
    @port = port
  end

  def start
    socket = UDPSocket.new
    socket.connect(@host, @port)
    puts "Type 'g' to get datetime or 'q' to exit"
    loop do
      msg = gets.chomp
      msg == 'q' and break
      socket.send(msg, 0)
      message, sender = socket.recvfrom(1024)
      puts message
    end
  end

  def stop
    socket.close
  end
end


if __FILE__ == $0
  print "Enter port: "
  port = gets.chomp.to_i
  client = UDPClient.new('localhost', port)
  client.start
end