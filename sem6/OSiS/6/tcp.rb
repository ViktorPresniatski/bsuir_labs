require 'socket'

server = TCPServer.new(1234)
exit if fork
Process.setsid
exit if fork
begin
  while connection = server.accept
    while line = connection.gets
      break if line =~ /quit/
      message = line.chomp == 'g' ? "#{Time.now}\n" : "Unknown request"
      connection.puts  message
    end
    connection.puts "Closing the connection. Bye!\n"
    connection.close
  end
rescue Errno::ECONNRESET, Errno::EPIPE => e
  puts e.message
  retry
end   