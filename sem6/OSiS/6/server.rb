require 'socket'
require 'date'

class UDPServer
  attr_reader :socket

  def initialize(host, port)
    @host = host
    @port = port
    @clients = []
  end

  def start
    @socket = UDPSocket.new
    begin
      @socket.bind(@host, @port)
      connected = true
      p @port
    rescue Errno::EACCES
      @port += 1
      retry
    end  
    exit if fork
    Process.setsid
    exit if fork
    loop do
      message, sender = @socket.recvfrom(1024)
      full_message = message.chomp == 'g' ? "#{Time.now}\n" : "Unknown request\n" 
      @socket.send(full_message, 0, @host, sender[1])
    end
  end
end


if __FILE__ == $0
  server = UDPServer.new('localhost', 13)
  server.start
end