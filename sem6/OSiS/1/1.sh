#!/bin/bash

whoami > output.txt
date >> output.txt
pwd >> output.txt
ps -e | wc -l >> output.txt
uptime | perl -n -e '/up (.+?),/ && print "$1\n"' >> output.txt