#/bin/bash

result=()

start() {
  local fullname="$1"
  local filename=`basename "$1"`
  if [[ "$filename" =~ $2 ]]; then
    result+=($fullname)
  fi
}

scan() {
  local x;
  for e in "$1"/*; do
    if [ -d "$e" ]
    then
      scan "$e" "$2"
    else
      start "$e" "$2" 
    fi
  done
}

dir=`pwd`

for temp in $@; do
  scan "$dir" "$temp"
done

for index in ${!result[*]}; do
  printf "%d\t%s\n" "$(($index + 1))" ${result[$index]}
done

read -p "Select file: " number

while [[ $number != 'q' ]]
do
  less -NM ${result[$((number - 1))]}
  read -p "Select file: " number
done
