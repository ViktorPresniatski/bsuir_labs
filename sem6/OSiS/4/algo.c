#include <string.h>

void inverse(char str[]) {
  int len = strlen(str) - 1;

  for (int i = 0; i < len / 2; i++) {
    char temp = str[len - i - 1];
    str[len - i - 1] = str[i];
    str[i] = temp;
  }
}