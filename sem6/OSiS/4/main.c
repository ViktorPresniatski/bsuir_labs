#include <stdio.h>
#include "algo.h"

int main() {
  FILE *in, *out;
  char str[50];
  char *estr;
  
  printf("Открытие файлов: ");
  in = fopen("input.txt", "r");
  out = fopen("output.txt", "w");

  if (in == NULL || out == NULL) { 
    printf ("ошибка\n");
    return -1; 
  } else {
    printf("выполнено\n");
  }

  while (1) {
    estr = fgets(str, sizeof(str), in);

    if (estr == NULL) {
      if (feof(in)) {
        printf("Чтение закончено\n");
        break;
      } else {
        printf ("Ошибка чтения из файла\n");
        break;
      }
    }

    inverse(str);
    fputs(str, out);
  }

  printf ("Закрытие файлов: ");
  if (fclose(in) == EOF || fclose(out) == EOF) printf ("ошибка\n");
  else printf ("выполнено\n");


  return 0;
}