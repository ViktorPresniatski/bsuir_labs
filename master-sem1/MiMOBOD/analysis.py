import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx


df = pd.read_csv('users.csv')

folowers_more = df.loc[df['follower_count'] >= df['following_count']]
folowers_less = df.loc[df['follower_count'] < df['following_count']]
folowers_more_count = folowers_more.index.size / df.index.size
folowers_less_count = folowers_less.index.size / df.index.size
data_names = ['Подписчиков больше чем подписок', 'Подписчиков меньше чем подписок']
fig1, ax1 = plt.subplots()
ax1.pie([folowers_more_count, folowers_less_count], labels=data_names, autopct='%1.2f%%')
plt.show()


plt.hist(df['posts'], bins=20, range=(0, 50))
plt.ylabel('Количество фотографий')
plt.xlabel('Колиество записей на стене')
plt.show()


x_axis = 'follower_count'
y_axis = 'wall_comments_count'
followers = df[:100].sort_values(by=x_axis)[x_axis]
wall_comments = df[:100].sort_values(by=y_axis)[y_axis]
plt.ylabel('Количество комментариев на стене')
plt.xlabel('Колиество подписчиков')
plt.plot(followers, wall_comments, marker='o')
plt.show()


names_mapping = {'User_ID1': 'source',
                 'User_ID2': 'target'}
friendship = pd.read_csv('users_combined.csv') \
    .rename(columns=names_mapping)
G_vk = nx.from_pandas_edgelist(friendship,
                               create_using=nx.Graph())
spring_pos = nx.spring_layout(G_vk)
plt.axis("off")
nx.draw_networkx(G_vk, pos=spring_pos,
                 with_labels=False,
                 node_size=35, node_color='r',
                 linewidths=2.0)


from multiprocessing import Pool
import itertools

def partitions(nodes, n):
    "Partitions the nodes into n subsets"
    nodes_iter = iter(nodes)
    while True:
        partition = tuple(itertools.islice(nodes_iter,n))
        if not partition:
            return
        yield partition

def btwn_pool(G_tuple):
    return nx.betweenness_centrality_source(*G_tuple)

def between_parallel(G, processes = None):
    p = Pool(processes=processes)
    part_generator = 4*len(p._pool)
    node_partitions = list(partitions(G.nodes(), int(len(G)/part_generator)))
    num_partitions = len(node_partitions)
 
    bet_map = p.map(btwn_pool,
                        zip([G]*num_partitions,
                        [True]*num_partitions,
                        [None]*num_partitions,
                        node_partitions))
 
    bt_c = bet_map[0]
    for bt in bet_map[1:]:
        for n in bt:
            bt_c[n] += bt[n]
    return bt_c

bt = between_parallel(G_vk, 4)
top = 10

max_nodes =  sorted(bt.items(), key=lambda v: -v[1])[:top]
bt_values = [5]*len(G_vk.nodes())
bt_colors = [0]*len(G_vk.nodes())
for max_key, max_val in max_nodes:
    bt_values[max_key], bt_colors[max_key] = 150, 2
 
plt.axis("off")
nx.draw_networkx(G_vk, pos=spring_pos, cmap=plt.get_cmap("rainbow"), node_color=bt_colors,
                 node_size=bt_values, with_labels=False)


import community

parts = community.best_partition(G_vk)
values = [parts.get(node) for node in G_vk.nodes()]
plt.axis("off")
nx.draw_networkx(G_vk, pos=spring_pos, cmap=plt.get_cmap("jet"), node_color=values,
                 node_size=35, with_labels=False)
