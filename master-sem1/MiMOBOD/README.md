# MiMOBOD labs

## Prerequisite: install jupyter notebook
https://jupyter.readthedocs.io/en/latest/install.html

## Create and activate virtual environment
`python3 -m venv venv | source ./venv/bin/activate`

## Run celery worker with scraping
`celery worker -A vk_scraper --loglevel=INFO`

## Run analytics
`python analysis.py`
