import json
from locust import HttpLocust, TaskSet, task, between


class UserBehavior(TaskSet):
    @task(1)
    def translate(self):
        data = {
            'text': 'AWS Lambda is a compute service that lets you run code '
                'without provisioning or managing servers. AWS Lambda executes '
                'your code only when needed and scales automatically, from '
                'a few requests per day to thousands per second. You pay only '
                'for the compute time you consume - there is no charge when '
                'your code is not running. With AWS Lambda, you can run code '
                'for virtually any type of application or backend service - '
                'all with zero administration. AWS Lambda runs your code '
                'on a high-availability compute infrastructure and performs '
                'all of the administration of the compute resources, including '
                'server and operating system maintenance, capacity provisioning '
                'and automatic scaling, code monitoring and logging. All you '
                'need to do is supply your code in one of the languages that '
                'AWS Lambda supports.',
            'source': 'english',
            'destination': 'russian'
        }
        self.client.post('/production', data=json.dumps(data), headers={
            'Content-Type': 'application/json',
            'Host': '0eirm8ghmi.execute-api.eu-north-1.amazonaws.com'
        })


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5, 9)
