## Create and activate virtual environment
`python3 -m venv venv`
`source ./venv/bin/activate`

## Install AWS SDK
`pip install aws-xray-sdk`

## Function archiving
`zip -g function.zip function.py`

## Vocabulary archiving
`zip -g function.zip vocabulary.json `

## Venv archiving
`zip -r9 /path/to/function.zip /path/to/venv/lib/python3.7/site-packages`

## Install AWS CLI and login with your credentials
https://docs.aws.amazon.com/en_us/cli/latest/userguide/install-bundle.html

## Upload AWS Lambda
`aws lambda update-function-code --function-name translator --zip-file fileb://function.zip`

## Run stress test server
`locust`
Go to http://127.0.0.1:8089 and run test
