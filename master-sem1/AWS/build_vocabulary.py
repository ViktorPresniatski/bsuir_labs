import re
import json

words = []
with open('vocab.txt') as f:
    words = [line for line in f.readlines()]

vocab = {}
for word_pair in words:
    s = word_pair.split('=')
    translation = re.findall(r'\w+', s[1])[0]
    vocab[s[0].lower()] = translation.lower()

with open('vocabulary.json', 'w') as f:
    json.dump(vocab, f, indent=4)
