import os
import re
import json
import logging
from aws_xray_sdk.core import xray_recorder

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def load_vocabulary():
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    file = os.path.join(cur_dir, 'vocabulary.json')

    with open(file) as f:
        vocabulary = json.load(f)

    return vocabulary


def in_segment(segment_name):
    def wrapper(func):
        def wrapped_func(*args, **kwargs):
            xray_recorder.begin_subsegment(segment_name)
            result = func(*args, **kwargs)
            xray_recorder.end_subsegment()
            return result

        return wrapped_func
    return wrapper


@in_segment('translator_segment')
def translate(text):
    response = []
    vocabulary = load_vocabulary()

    for raw_word in text.split(' '):
        processed_word = re.search(r'\w+', raw_word)
        if processed_word is None:
            response.append(raw_word)
            continue

        processed_word = processed_word.group()

        new_word = raw_word
        for key in vocabulary:
            if processed_word.lower() == key:
                translated_word = vocabulary[key]
                new_word = raw_word.replace(processed_word, translated_word)
                break

        response.append(new_word)

    return ' '.join(response)


def lambda_handler(event, context):
    params = json.loads(event.get('body'))

    try:
        logger.info('## Started translation')
        result = translate(params['text'])
        logger.info('## Ended translation')

        response = {'result': result}
        return {
            'statusCode': 200,
            'body': json.dumps(response)
        }
    except ValueError as e:
        response = {'error': str(e)}
        logger.exception('Error:, %s', e)
        return {
            'statusCode': 400,
            'body': json.dumps(response)
        }
