import numpy as np
import pulp as pl
from collections import Counter


def solve_integer_LP_problem(c, A_ub, b_ub, A_eq, b_eq):
    problem = pl.LpProblem("Printed_Circuit_Board", pl.LpMinimize)

    x_names = [f'{str(i)}' for i in range(len(c))]
    X = pl.LpVariable.dicts("X", x_names, 0, 1, cat='Integer')

    problem += pl.lpSum([c[ind] * X[e] for ind, e in enumerate(x_names)])

    for j, a_j in enumerate(A_ub):
        problem += pl.lpSum([a_j[ind] * X[e] for ind, e in enumerate(x_names)]) <= b_ub[j]

    for j, a_j in enumerate(A_eq):
        problem += pl.lpSum([a_j[ind] * X[e] for ind, e in enumerate(x_names)]) == b_eq[j]

    problem.solve()
    optimal_plan = [int(v.varValue) for v in problem.variables()]
    print(problem)
    return optimal_plan, problem.objective.value()


def solve_printed_circuit_board_task(points_set):
    # build objective function and E graph
    E = []
    c = []
    n_points = len(points_set)
    for point in range(n_points):
        for next_point in range(point + 1, n_points):
            src_coord, dest_coord = points_set[point], points_set[next_point]
            distance = max(abs(src_coord[0] - dest_coord[0]), abs(src_coord[1] - dest_coord[1]))
            E.append((point, next_point))
            c.append(distance)

    # build A ubound matrix and b ubound vector
    # the idea is that one point can have only 1 or 2 incident edges
    E = np.array(E)
    n_rows = n_points * 2
    n_vars = len(c)
    A_ub = np.zeros((n_rows, n_vars), dtype=int)
    b_ub = np.zeros(n_rows)
    for point in range(n_points):
        # get rows that needed for constraint of specific point
        rows = np.argwhere(E == point)[:,0]

        # point can have no more then 2 incident edges
        cur_row = point * 2
        A_ub[cur_row, rows] = 1
        b_ub[cur_row] = 2

        # point can have no less then 1 incident edge
        A_ub[cur_row + 1, rows] = -1
        b_ub[cur_row + 1] = -1

    # build A equality matrix and b euality vector
    # number of all edges equals to number of points minus 1
    A_eq = np.ones((1, n_vars), dtype=int)
    b_eq = np.array([n_points - 1])

    lp_result = solve_integer_LP_problem(c, A_ub, b_ub, A_eq, b_eq)
    optimal_plan, objective_func = lp_result
    print(f'Optimal plan = {optimal_plan}')
    print(f'Objective function = {objective_func}')

    result_edges = E[np.array(optimal_plan) == 1]
    result_edges = list(map(tuple, result_edges))
    print(f'Edges in result route = {result_edges}')

    # build the shortest route
    counter = Counter([p for e in result_edges for p in e])
    single_points = [p for p, count in counter.items() if count == 1]
    start_point = min(single_points)
    finish_point = max(single_points)

    route = [start_point]
    used_edges = []
    cur_point = start_point
    while cur_point != finish_point:
        for edge in result_edges:
            if cur_point in edge and edge not in used_edges:
                next_point = [p for p in edge if p != cur_point][0]
                used_edges.append(edge)
                route.append(next_point)
                cur_point = next_point

    return route


if __name__ == '__main__':
    # c = np.array([3, 4, 3, 2, 4, 2])
    # A_ub = np.array([[1, 1, 0, 0, 0, 0],
    #                  [0, 0, 1, 1, 0, 0],
    #                  [0, 0, 0, 0, 1, 1],
                     
    #                  [1, 0, 1, 0, 0, 0],
    #                  [0, 1, 0, 0, 1, 0],
    #                  [0, 0, 0, 1, 0, 1],
                     
    #                  [1, 0, 0, 0, 0, 1],
    #                  [0, 1, 0, 1, 0, 0],
    #                  [0, 0, 1, 0, 1, 0]])
    # b_ub = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1])
    # A_eq = np.array([[1, 1, 1, 1, 1, 1]])
    # b_eq = np.array([2])

    # c = np.array([2, 2, 3, 2, 5, 3])
    # A_ub = np.array([[ 1, 1, 1, 0, 0, 0],
    #                  [-1,-1,-1, 0, 0, 0],

    #                  [ 1, 0, 0, 1, 1, 0],
    #                  [-1, 0, 0,-1,-1, 0],

    #                  [ 0, 1, 0, 1, 0, 1],
    #                  [ 0,-1, 0,-1, 0,-1],
                     
    #                  [ 0, 0, 1, 0, 1, 1],
    #                  [ 0, 0,-1, 0,-1,-1]])
    # b_ub = np.array([2,-1, 2,-1, 2,-1, 2,-1])
    # A_eq = np.array([[1, 1, 1, 1, 1, 1]])
    # b_eq = np.array([3])

    # result = solve_integer_LP_problem(c, A_ub, b_ub, A_eq, b_eq)
    # print("Optimal plan = {result[0]}")
    # print("Objective function = {result[1]}")

    # points_set = [(1, 1), (3, 4), (5, 2)]
    points_set = [(1, 3), (2, 5), (3, 3), (3, 0)]
    route = solve_printed_circuit_board_task(points_set)
    print(' -> '.join(map(str, route)))
