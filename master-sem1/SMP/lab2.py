import numpy as np
from scipy import linalg, optimize

eps = 1e-7


def solve_subtask(A, B, u_left, u_right, c_, b_):
    c = np.dot(u_left, A) - c_
    res = optimize.linprog(c, A_eq=B, b_eq=b_)

    if not res.success:
        raise Exception('Subtask solution not found')

    return res.x, res.fun + u_right


def solve(A1, A2, B1, B2, b0, b1, b2, c1, c2, Ab, db):
    b_ext = np.append(b0, [1, 1])
    i = 0
    while True:
        i += 1
        print(f"Iteration - {i}")

        # step 1
        print(f'A basis = \n{Ab}')
        Ab_inv = linalg.inv(Ab)
        print(f'A basis inversed = \n{Ab_inv}')

        # step 2
        u = np.dot(db, Ab_inv)
        print(f"u = {u}")
        _u = u[:-2]

        second_plan_chosen = False  # will be cheked which plan was chosen
        res_subtask_1, alpha = solve_subtask(A1, B1, _u, u[-2], c1, b1)
        print(f"alpha = {alpha}")
        if alpha > -eps:
            second_plan_chosen = True  # will be cheked which plan was chosen
            res_subtask_2, beta = solve_subtask(A2, B2, _u, u[-1], c2, b2)
            print(f"beta = {beta}")
            if beta > -eps:
                optimum_plan = np.dot(u, b_ext)
                print(f"Stop, solution found: {optimum_plan}")
                return optimum_plan

        # step 3
        if second_plan_chosen:
            A_new = np.append(np.dot(A2, res_subtask_2), np.array([0, 1]))
            d_new = np.dot(c2, res_subtask_2)
        else:
            A_new = np.append(np.dot(A1, res_subtask_1), np.array([1, 0]))
            d_new = np.dot(c1, res_subtask_1)
        print(f"A_new = {A_new}")
        print(f"d_new = {d_new}")

        # step 4
        z = np.dot(Ab_inv, A_new)
        print(f"z = {z}")
        theta = np.array([np.dot(Ab_inv[i, :], b_ext) / z_i
                          if z_i > eps else np.inf
                          for i, z_i in enumerate(z)])
        j = np.argmin(theta)
        print(f"Theta = {theta}")
        print(f"Index of column to replace = {j}")
        if theta[j] == np.inf:
            print("The objective function is unbounded above on the set of admissible plans")
            return None

        # step 5
        Ab[:, j] = A_new
        db[j] = d_new
        print()


if __name__ == '__main__':
    A1 = np.array([[1, 1, 0],
                   [1, 0, 0]])
    A2 = np.array([[0, 0, -1],
                   [0, -1, 0]])
    B1 = np.array([[1, 2, 1]])
    B2 = np.array([[1, 1, 1]])
    
    c1 = np.array([1, 1, 0])
    c2 = np.array([0, 0, 0])
    
    b0 = np.array([1, 1])
    b1 = np.array([6])
    b2 = np.array([4])

    Ab = np.array([[0., 6., -4., 0.],
                   [0., 6.,  0., 0.],
                   [1., 1.,  0., 0.],
                   [0., 0.,  1., 1.]])
    db = np.array([0, 6, 0, 0])

    result = solve(A1, A2, B1, B2, b0, b1, b2, c1, c2, Ab, db)
