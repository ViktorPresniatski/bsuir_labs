import numpy as np
import functools


def solve_nash(game_matrix):
    all_best_strategies = []
    n_players = len(game_matrix.shape) - 1

    for i in range(n_players):
        players_win = game_matrix[:,:,i]
        max_win = players_win.max(axis=i, keepdims=True)
        best_strategy = np.argwhere(players_win == max_win)
        set_strategies = set(map(tuple, best_strategy))
        all_best_strategies.append(set_strategies)

    return list(functools.reduce(lambda x, y: x & y, all_best_strategies))


if __name__ == '__main__':
    game_matrix = np.array([[(1, 1), (2, 0)],
                            [(0, 2), (1, 1)]])
    result = solve_nash(game_matrix)
    print(f"Nash result = {result}")
