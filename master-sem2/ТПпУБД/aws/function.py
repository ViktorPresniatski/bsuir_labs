import os
import dis
import boto3
import json
import logging
import importlib
from aws_xray_sdk.core import xray_recorder
from io import StringIO

import sys
sys.path.insert(1, "/tmp")

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3_client = boto3.client('s3')
bucket = 'aws-bucket-content'


def in_segment(segment_name):
    def wrapper(func):
        def wrapped_func(*args, **kwargs):
            xray_recorder.begin_subsegment(segment_name)
            result = func(*args, **kwargs)
            xray_recorder.end_subsegment()
            return result

        return wrapped_func
    return wrapper


@in_segment('translator_segment')
def translate(filepath):
    input_path = f'input/{filepath}'
    output_filename = f'dis_{filepath}'.replace('.py', '.txt')
    output_path = f'output/{output_filename}'

    os.chdir('/tmp')
    s3_client.download_file(bucket, input_path, filepath)

    with StringIO() as out:
        module_name = filepath.split('.')[0]
        mod = importlib.import_module(module_name)
        dis.dis(mod, file=out)
        output = out.getvalue()

    with open(output_filename, 'w') as f:
        f.write(output)

    s3_client.upload_file(output_filename, bucket, output_path)

    return output_path


def lambda_handler(event, context):
    params = json.loads(event.get('body'))

    try:
        logger.info('## Started translation')
        result = translate(params['filepath'])
        logger.info('## Ended translation')

        response = {'result': result}
        return {
            'statusCode': 200,
            'body': json.dumps(response)
        }
    except ValueError as e:
        response = {'error': str(e)}
        logger.exception('Error:, %s', e)
        return {
            'statusCode': 400,
            'body': json.dumps(response)
        }
