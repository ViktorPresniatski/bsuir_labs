def go():
    ds_location = DataSourceLocation.objects.get(name='Centsai')
    documents = Document.objects.filter(data_source_location=ds_location)
    documents.update(is_pipelined=False)
    doc_ids = list(documents.values_list('id', flat=True))
    nlp_services = [PipelineService.ServicesNames.GOAL_MINER_SERVICE]
    job_id = Job.start_job(Job.OperationTypes.PUBLIC_CONTENT_PIPELINE, None)
    if not job_id:
        print('Already run')
        return

    manager = PublicPipelineManager(ds_location.id, doc_ids, job_id, nlp_services=nlp_services)
    manager.start_pipeline()
    Job.finish_job(job_id)


def get_results():
    import json
    from bs4 import BeautifulSoup

    documents = Document.objects.filter(data_source_location__name='Centsai')
    data = list(DocumentMeta.objects.filter(document__in=documents).values('document_id', 'goals', 'document__raw_text'))
    for item in data:
        soup = BeautifulSoup(item['document__raw_text'], 'html.parser')
        item['text'] = soup.get_text()
        del item['document__raw_text']

def main():    
    with open('centsai_results.txt', 'w') as f:
        # json.dump(data, f, indent=4)
        for item in data:
            f.write(f'Document ID: {item["document_id"]}\n')
            f.write(f'Goals: {", ".join(item["goals"])}\n')
            f.write(f'Text: {item["text"]}\n')
            f.write('\n')
            f.write('=' * 100)
            f.write('\n')
