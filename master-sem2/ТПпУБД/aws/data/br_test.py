def main():
    br = BroadIndexOfTopClientFundHoldingWithRecentLargeMoveService()
    weight = 6
    params = {
        'days_back': 60,
        'num_top_funds': 3,
        'performance_window_days': 60,
        'std_deviation': 0.1,
    }
    br.set_config(weight, params=params)

    results = {}
    for client in Client.objects.all():
        results[client.id] = br.execute_for_client(client.id)
