import json


def generate_report(name, create_file=True):
    logs = list(CrawlLog.objects.filter(crawl__name__contains=name).annotate(
        date_day=TruncDate('created_at'),
        day=Cast('date_day', output_field=CharField())
    ).values('day').annotate(
        web_pages_crawled_count=Sum('pages_crawled_this_round'),
        documents_processed_count=Sum('pages_processed_this_round'),
        documents_saved_count=Sum('saved_count'),
    ).order_by('day'))

    if create_file:
        with open('diffbot_report.json', 'w') as f:
            json.dump(logs, f, indent=4)

    return logs
