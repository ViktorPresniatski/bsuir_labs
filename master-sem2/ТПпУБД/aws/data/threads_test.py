# from concurrent.futures import ThreadPoolExecutor
# from core.models import DataSource
# from datetime import datetime, timedelta
#
#
# data_source = DataSource.objects.last()
# now = datetime.now() - timedelta(days=1)
# def get_naviga_document(args):
#     file, ds_id = args
#     data_source = DataSource.objects.get(id=ds_id)
#     fil = data_source.storage.open(file)
#     return fil.read()
#
# files = {
#     '201909230000GALE____PUB_ALL_600588895.xml': now,
#     '201909230000GALE____PUB_ALL_600588896.xml': now,
#     '201909230000GALE____PUB_ALL_600588897.xml': now,
#     '201909230000GALE____PUB_ALL_600588898.xml': now,
#     '201909230000GALE____PUB_ALL_600588899.xml': now,
#     '201909230000GALE____PUB_ALL_600588900.xml': now,
#     '201909230000GALE____PUB_ALL_600588901.xml': now,
#     '201909230000GALE____PUB_ALL_600588902.xml': now,
#     '201909230000GALE____PUB_ALL_600588903.xml': now,
#     '201909230000GALE____PUB_ALL_600588904.xml': now,
#     '201909230000GALE____PUB_ALL_600588905.xml': now,
#     '201909230000GALE____PUB_ALL_600588906.xml': now,
#     '201909230000GALE____PUB_ALL_600588907.xml': now,
#     '201909230000GALE____PUB_ALL_600588908.xml': now,
#     '201909230000GALE____PUB_ALL_600588909.xml': now,
#     '201909230000GALE____PUB_ALL_600588910.xml': now,
#     '201909230000GALE____PUB_ALL_600588911.xml': now,
#     '201909230000GALE____PUB_ALL_600588912.xml': now,
#     '201909230000GALE____PUB_ALL_600588913.xml': now,
#     '201909230000GALE____PUB_ALL_600588914.xml': now,
# }
# start_time = datetime.now()
# args = [(file, data_source.id) for file in files.keys()]
# with ThreadPoolExecutor(20) as executor:
#     naviga_documents = executor.map(get_naviga_document, args)
#
# arr = list(naviga_documents)
# print(datetime.now() - start_time)




def get_storage():
    return ExtendedFTPStorage(location='ftp://ftpforwardlane:ftpf0rw4rdl4n3@ftp2.acquiremedia.com:21', base_url='/')


def delete_document(file):
    storage = get_storage()
    storage._start_connection()
    storage._connection.delete(file)


def main():
    storage = get_storage()
    dirs, files = storage.listdir('/')
    print(f"Length of files {len(files)}")
    start_time = datetime.now()

    with ThreadPoolExecutor(50) as executor:
        executor.map(delete_document, files)

    print(datetime.now() - start_time)
