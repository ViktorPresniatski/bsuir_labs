import numpy as np
import pandas as pd
from analysis import train, test

num_train = train.shape[0]
num_test = test.shape[0]
print(num_train, num_test)

data = pd.concat((train.loc[:, "Id" : "P37"],
                  test.loc[:, "Id" : "P37"]), ignore_index=True)

distinct_cities = train.loc[:, "City"].unique()

means = []
for col in train.columns[5:42]:
    temp = []
    for city in distinct_cities:
        temp.append(train.loc[train.City == city, col].mean())     
    means.append(temp)
    
city_pvars = pd.DataFrame(columns=["city_var", "means"])
for i in range(37):
    for j in range(len(distinct_cities)):
        city_pvars.loc[i+37*j] = ["P"+str(i+1), means[i][j]]

plt.rcParams['figure.figsize'] = (18.0, 6.0)
sns.boxplot(x="city_var", y="means", data=city_pvars)


from sklearn import cluster


def adjust_cities(data, train, k):
    relevant_pvars =  ["P1", "P2", "P11", "P19", "P20", "P23", "P30"]
    train = train.loc[:, relevant_pvars]
    
    kmeans = cluster.KMeans(n_clusters=k)
    kmeans.fit(train)
    
    data['City Cluster'] = kmeans.predict(data.loc[:, relevant_pvars])
    del data["City"]
    
    return data


def one_hot_ecoding(data,col,pref):
    data = data.join(pd.get_dummies(data[col], prefix=pref))
    data = data.drop([col], axis=1)
    return data


data = adjust_cities(data, train, 20)
data = one_hot_ecoding(data,'City Group',"CG")
data = one_hot_ecoding(data,'Type',"T")

train_processed = data[:num_train]
test_processed = data[num_train:]
print("Train :",train.shape)
print("Test:",test.shape)

sav_train = pd.DataFrame()
sav_train["rev_save"] = train["revenue"].copy()

train["revenue"] = [np.log(num) for num in train["revenue"]]
print(len(train["revenue"]))

train_processed["revenue"] = train["revenue"].values
train = train_processed
test = test_processed
print("Train :",train.shape)
print("Test:",test.shape)

import time
from datetime import datetime as dt

## prepartion function 
def prepare_data_frame(dataframe, target):
    df = dataframe.copy()

    df['Open Date Year']  = df['Open Date'].dt.year
    df['Open Date Month']  = df['Open Date'].dt.month
    df['Open Date Day']  = df['Open Date'].dt.day
 
    all_diff = []
    for date in df["Open Date"]:
        diff = dt.now() - date
        all_diff.append(int(diff.days/1000))
    df['Days_from_open'] = pd.Series(all_diff)

    df = df.drop(['Open Date'], axis=1)
    # drop target column
    if target in df.columns:
        tar = df[target]
        df = df.drop([target], axis=1)
    else:
        tar = None

    return (df,tar)

X_train,y_train = prepare_data_frame(train,'revenue')
X_test,y_test = prepare_data_frame(test,'revenue')


def get_reg_mse(model,in_parameters):
    my_model_1 = model
    clf = GridSearchCV(my_model_1, in_parameters, cv=2, scoring='neg_mean_squared_error')
    clf.fit(X_train, y_train)
    print('best_params_',clf.best_params_)
    mae_1 = clf.best_score_ * -1
    return mae_1


def baseline_model():
    dim = len(X_train.columns.tolist())
    if not isinstance(dim, int):
        return 0
    model = tf.keras.Sequential([
        layers.Dense(dim, input_dim=dim, kernel_initializer='normal', activation='relu'),
        layers.Dense(int(round(dim/2)), kernel_initializer='normal', activation='relu'), ## 
        layers.Dense(int(round(dim/4)), kernel_initializer='normal', activation='relu'), ## 
        layers.Dense(int(round(dim/8)), kernel_initializer='normal', activation='relu'), ## 
        layers.Dense(1, kernel_initializer='normal')
    ])
    model.compile(optimizer='adam',
                  loss='mse',
                  metrics=['mse'])
    return model


from sklearn.model_selection import cross_val_score, KFold
from sklearn.preprocessing import StandardScaler

def get_reg_keras_mse(in_parameters):
    my_model_1 = tf.keras.wrappers.scikit_learn.KerasRegressor(build_fn=baseline_model, verbose=1)
    reg = GridSearchCV(my_model_1, in_parameters, cv=2, scoring='neg_mean_squared_error')
    X_ktrain = StandardScaler().fit_transform(X_train.values)  
    arr = sav_train['rev_save'].values 
    y_ktrain = StandardScaler().fit_transform(arr[:, np.newaxis])
    
    reg.fit(X_ktrain, y_train.values)
    print('best_params_',reg.best_params_)
    mse_1 = reg.best_score_ * -1
    return mse_1


c = list(np.arange(1, 10, 1))
cache_size=list(range(100, 1001, 100))
parameters = {'kernel':('linear', 'rbf'), 'C':c,'cache_size':cache_size}
mse_1 = get_reg_mse(SVR(gamma='scale'),parameters)
print("Mean Squared Error:" , mse_1)

n_estimators=list(range(100, 501, 100))
max_depth = list(range(1, 5, 1))
parameters = {'n_estimators':n_estimators,'max_depth':max_depth}
mse_1 = get_reg_mse(RandomForestRegressor(random_state=400),parameters)
print("Mean Squared Error:" , mse_1)

random_state=list(range(0, 100, 1))
parameters = {'random_state':random_state}
mse_1 = get_reg_mse(LassoCV(cv=2),parameters)
print("Mean Squared Error:" , mse_1)

seed = 1
epochs=list(range(100, 101, 100))
batch_size = [1,5]
parameters = {'epochs':epochs,'batch_size':batch_size}
mse_1 = get_reg_keras_mse(parameters)
print("Mean Squared Error:" , mse_1)

X_test['Days_from_open'] = X_test['Days_from_open'].fillna(int(round(X_test['Days_from_open'].mean())))
reg = RandomForestRegressor(random_state=400, max_depth=2, n_estimators=100)
reg.fit(X_train, y_train)
print(pd.DataFrame(reg.predict(X_test), columns=['predicted_revenue']))
