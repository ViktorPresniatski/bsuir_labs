# TPpUBD labs

## Prerequisite: install jupyter notebook
https://jupyter.org/install

## Create and activate virtual environment
`python3 -m venv venv | source ./venv/bin/activate`

## Install requirements
`pip install -r requirements.txt`

## Run jupiter lab
`jupyter lab`

## Run analytics
`python analysis.py`

## Run predictions
`python prediction.py`
