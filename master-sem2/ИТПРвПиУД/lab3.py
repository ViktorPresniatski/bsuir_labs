import numpy as np
from collections import Counter


def print_matrix(C, A, underlined, marked):
    m, n = C.shape
    for i in range(m):
        row = []
        for j in range(n):
            str_el = str(C[i][j])
            if j in A:
                str_el = f'*{str_el}*'

            if underlined[i] == j:
                str_el = f'_{str_el}_'
            elif marked.get(j) == i:
                str_el = f'[{str_el}]'

            flag = False
            while len(str_el) < 6:
                if flag:
                    str_el = ' ' + str_el
                    flag = False
                else:
                    str_el += ' '
                    flag = True

            row.append(str_el)

        str_row = '  '.join(row)
        print(str_row)


def solve(test_C):
    C = test_C.copy()
    A = []
    underlined = C.argmin(axis=1)  # ror number => underlined column number
    marked = {}  # column number => marked row number

    def print_m():
        print_matrix(C, A, underlined, marked)

    if len(underlined) == len(set(underlined)):
        return underlined

    while True:
        print('----------------------------------------')
        print('Step 1')
        print('Select any column which contains more than one underlined element')
        multiple_work = [k for k, v in Counter(underlined).items() if v > 1]

        A.append(multiple_work[0])
        print(f'A = {set([a + 1 for a in A])}')
        print_m()
        print('----------------------------------------')

        while True:
            print('Step 2')
            rows = sorted([i for a in A for i, u in enumerate(underlined) if u == a])
            print(f'Find min elements in rows {",".join([str(r+1) for r in rows])} and not in A columns')

            d = list()  # (row number, column number, d_i)
            for r in rows:
                item = (r, *min([(i, el) for i, el in enumerate(C[r] - C[r][underlined[r]]) if i not in A], key=lambda x: x[1]))
                d.append(item)

            r, s, d_min = min(d, key=lambda x: x[2])
            k = underlined[r]

            print(f'Min elements: {", ".join([f"C[{i[0]+1}][{i[1]+1}] = {C[i[0]][i[1]]}" for i in d])}')
            print(f'Differences: {", ".join([f"d[{i[0]+1}] = {i[2]}" for i in d])}')
            print(f'd_min = {d_min}')
            print(f'Crs = C[{r+1}][{s+1}] = {C[r][s]}, Crk = C[{r+1}][{k+1}] = {C[r][k]}')
            print('----------------------------------------')

            print('Step 3')
            print(f'Add d_min = {d_min} to all elements in A columns')
            C[:, A] += d_min
            print_m()
            print('----------------------------------------')

            print('Step 4')
            print(f'Mark element: Crs = C[{r+1}][{s+1}] = {C[r][s]}')
            marked[s] = r
            print_m()
            print('----------------------------------------')

            print('Step 5')
            if not any(underlined == s):
                print(f'Work {s+1} is free - new performer can be assigned')
                print('----------------------------------------')
                break

            print(f'Work {s+1} is busy - new performer cannot be assigned')
            print(f'Add column {s+1} to set A. Go to step 2')
            A.append(s)
            print('----------------------------------------')

        while True:
            print('Step 6')
            print(f'Unmark element Crs = C[{r+1}][{s+1}] = {C[r][s]} and underline it')
            print(f'Remove underline from element Crk = C[{r+1}][{k+1}] = {C[r][k]}')
            del marked[s]
            underlined[r] = s
            print(f'New assigment: Performer {r+1} => Work {s+1}')
            print_m()
            print('----------------------------------------')

            print('Step 7')
            if any(underlined == k):
                print(f'Work {k+1} has a performer. Go to next step')
                print('----------------------------------------')
                break
    
            print(f'Work {k+1} doesn\'t have a performer. Go to step 6')
            i = marked[k]
            r, s = i, k
            print(f'Found marked element in column {k+1}. New Crs = C[{r+1}][{s+1}] = {C[r][s]}')
            print('----------------------------------------')

        print('Step 8')
        if len(underlined) == len(set(underlined)):
            print('Solution found')
            print_matrix(C, [], underlined, {})
            print(", ".join([f'P{r + 1} => W{c + 1}' for r, c in enumerate(underlined)]))
            print(f'Total cost = {sum([test_C[r][c] for r, c in enumerate(underlined)])}')
            return underlined

        free_works = [i+1 for i in range(len(underlined)) if i not in underlined]
        print(f'Detected work(s) without performer: {", ".join([f"Work {w}" for w in free_works])}.')
        print('Clear set A, unmark all elements. Go to step 1')
        A = []
        marked = {}


def solve_extension(C, mandatory=None):
    if mandatory is None:
        mandatory = []

    if not isinstance(mandatory, list):
        mandatory = [mandatory]

    total_sum = C.sum()
    m, n = C.shape
    if m < n:
        new_rows = np.zeros((n - m, n))
        new_rows[:, mandatory] = total_sum
        new_C = np.vstack([C, new_rows])
    elif m > n:
        new_columns = np.zeros((m, m - n))
        new_columns[mandatory, :] = total_sum
        new_C = np.hstack([C, new_columns])
    else:
        return solve(C)

    new_C = new_C.astype(int)
    print('Prepared cost matrix:')
    print(new_C)
    return solve(new_C)


if __name__ == '__main__':
    # test_C_example = np.array([[10, 5, 9, 18, 11],
    #                    [13, 14, 7, 12, 14],
    #                    [11, 12, 10, 12, 13],
    #                    [18, 9, 12, 17, 15],
    #                    [11, 8, 14, 19, 10]])
    # solution = solve(test_C)

    print('========================================')
    print('=============== TASK 1 =================')
    print('========================================')
    print()
    test_C_task_1 = np.array([[12, 5, 10, 18, 11],
                       [14, 15, 7, 12, 14],
                       [10, 12, 10, 12, 13],
                       [18, 9, 12, 17, 15],
                       [10, 8, 15, 19, 10]])
    solve(test_C_task_1)
    print()
    print('========================================')
    print('=============== TASK 2 =================')
    print('========================================')
    print()
    test_C_task_2 = np.array([[12, 5, 10, 18, 11, 7],
                              [14, 15, 7, 12, 14, 6],
                              [10, 12, 10, 12,13,10],
                              [18, 9, 12, 17, 15,11],
                              [10, 8, 15, 19, 10, 9]])
    solve_extension(test_C_task_2, mandatory=5)
