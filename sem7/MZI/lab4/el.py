import random


def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

def is_prime(num):
    if num == 2:
        return True
    if num < 2 or num % 2 == 0:
        return False
    for n in xrange(3, int(num**0.5) + 2, 2):
        if num % n == 0:
            return False
    return True

def get_prime(max_num=2000):
    num = random.randint(500, max_num)

    while not is_prime(num):
        num = random.randint(500, max_num)

    return num


def primitive_root(modulo):
    required_set = set(num for num in range (1, modulo) if gcd(num, modulo) == 1)
    for g in range(1, modulo):
        actual_set = set(pow(g, powers) % modulo for powers in range (1, modulo))
        if required_set == actual_set:
            return g

def generate_keys():
    p = get_prime()
    g = primitive_root(p)

    x = random.randint(2, p - 2)

    y = (g ** x) % p
    return p, g, x, y

def encrypt(p, g, y, plaintext):
    k = random.randint(2, p - 2)

    cipher = [((g ** k) % p, (ord(char) * y ** k) % p) for char in plaintext]
    
    return cipher

def decrypt(x, p, ciphertext):
    plain = [chr((b * a **(p - 1 - x)) % p) for a, b in ciphertext]

    return ''.join(plain)
    

if __name__ == '__main__':
    print(generate_keys())
    p, g, x, y = generate_keys()

    with open('input.txt') as f:
        message = f.read()

    encrypted_msg = encrypt(p, g, y, message)
    print "Your encrypted message is: "
    print ''.join(map(lambda x: str(x), encrypted_msg))
    print '=' * 20
    print "Your message is:"
    print decrypt(x, p, encrypted_msg)