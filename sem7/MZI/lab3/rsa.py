import random


def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def extended_gcd(a, b):
    x = 0
    y = 1
    lx = 1
    ly = 0
    oa = a
    ob = b
    while b != 0:   
        q = a // b
        (a, b) = (b, a % b)
        (x, lx) = ((lx - (q * x)), x)
        (y, ly) = ((ly - (q * y)), y)
    if lx < 0:
        lx += ob
    if ly < 0:
        ly += oa
    return a, lx, ly


def is_prime(num):
    if num == 2:
        return True
    if num < 2 or num % 2 == 0:
        return False
    for n in xrange(3, int(num**0.5) + 2, 2):
        if num % n == 0:
            return False
    return True

def get_prime(max_num=1000):
    num = random.randint(10, max_num)

    while not is_prime(num):
        num = random.randint(10, max_num)

    return num


def find_p_q():
    p = get_prime()
    q = get_prime()

    while p == q:
        q = get_prime()

    return max(p, q), min(p, q)

def generate_keypair(p, q):
    n = p * q

    phi = (p-1) * (q-1)

    while True:
        e = random.randrange(1, phi)
        g = gcd(e, phi)
        if g == 1:
            break

    d = extended_gcd(e, phi)[1]
    
    return ((e, n), (d, n))

def encrypt(pk, plaintext):
    e, n = pk
    cipher = [(ord(char) ** e) % n for char in plaintext]

    return cipher

def decrypt(pk, ciphertext):
    d, n = pk
    plain = [chr((char ** d) % n) for char in ciphertext]

    return ''.join(plain)
    

if __name__ == '__main__':
    print "RSA Encrypter/ Decrypter"
    p, q = find_p_q()
    public, private = generate_keypair(p, q)
    print "Your public key is ", public ," and your private key is ", private
    
    with open('input.txt') as f:
        message = f.read()

    encrypted_msg = encrypt(private, message)
    print "Your encrypted message is: "
    print ''.join(map(lambda x: str(x), encrypted_msg))
    print '=' * 20
    print "Your message is:"
    print decrypt(public, encrypted_msg)