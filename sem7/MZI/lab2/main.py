import sys

class Algorithm:
    H = [
        [177, 148, 186, 200, 10,  8,   245, 59,  54,  109, 0,   142, 88,  74,  93,  228],
        [133, 4,   250, 157, 27,  182, 199, 172, 37,  46,  114, 194, 2,   253, 206, 13],
        [91,  227, 214, 18,  23,  185, 97,  129, 254, 103, 134, 173, 113, 107, 137, 11],
        [92,  176, 192, 255, 51,  195, 86,  184, 53,  196, 5,   174, 216, 224, 127, 153],
        [225, 43,  220, 26,  226, 130, 87,  236, 112, 63,  204, 240, 149, 238, 141, 241],
        [193, 171, 118, 56,  159, 230, 120, 202, 247, 198, 248, 96,  213, 187, 156, 79],
        [243, 60,  101, 123, 99,  124, 48,  106, 221, 78,  167, 121, 158, 178, 61,  49],
        [62,  152, 181, 110, 39,  211, 188, 207, 89,  30,  24,  31,  76,  90,  183, 147],
        [233, 222, 231, 44,  143, 12,  15,  166, 45,  219, 73,  244, 111, 115, 150, 71],
        [6,   7,   83,  22,  237, 36,  122, 55,  57,  203, 163, 131, 3,   169, 139, 246],
        [146, 189, 155, 28,  229, 209, 65,  1,   84,  69,  251, 201, 94,  77,  14,  242],
        [104, 32,  128, 170, 34,  125, 100, 47,  38,  135, 249, 52,  144, 64,  85,  17],
        [190, 50,  151, 19,  67,  252, 154, 72,  160, 42,  136, 95,  25,  75,  9,   161],
        [126, 205, 164, 208, 21,  68,  175, 140, 165, 132, 80,  191, 102, 210, 232, 138],
        [162, 215, 70,  82,  66,  168, 223, 179, 105, 116, 197, 81,  235, 35,  41,  33],
        [212, 239, 217, 180, 58,  98,  40,  117, 145, 20,  16,  234, 119, 108, 218,	29]
    ]

    PLAIN_MODE = 'plain_mode'
    BLOCK_COUPLED_MODE = 'block_coupled_mode'
    GAMMING_WITH_FEEDBACK_MODE = 'gamming_with_feedback_mode'
    COUNTER_MODE = 'counter_mode'

    def __init__(self, key, mode=COUNTER_MODE):
        d = self.get_bit_frames(key)
        self.tetas = []
        for i in range(d):
            self.tetas.append(key & 0xFFFF)
            key >>= 32
        if d == 4:
            self.tetas.extend(self.tetas[:])
        elif d == 6:
            self.tetas.extend([
                self.tetas[0] ^ self.tetas[1] ^ self.tetas[2],
                self.tetas[3] ^ self.tetas[4] ^ self.tetas[5]
            ])

        self.K = []
        for _ in range(8):
            self.K.extend(self.tetas[:])

        self.mode = mode

    @staticmethod
    def get_bit_frames(key):
        l = len(bin(key)[2:])
        l &= (1 << 256) - 1
        if 256 >= l > 192:
            return 8
        elif 192 >= l > 128:
            return 6
        elif l <= 128:
            return 4
        else:
            raise ValueError()

    @staticmethod
    def lambd(u, n):
        mask = (1 << n) - 1
        if u < 1 << 31:
            return (2 * u) % (2 ** 32)
        else:
            return (2 * u + 1) % (2 ** 32)

    @staticmethod
    def lambd_r(u, n, r):
        result = u
        for i in range(r):
            result = Algorithm.lambd(result, n)
        return result

    @staticmethod
    def square_plus(u, v):
        n = 32
        mask = (1 << n) - 1
        return (u + v) % (2 ** 32)

    @staticmethod
    def square_minus(u, v):
        n = 32
        mask = (1 << n) + u
        return (mask - v) % (1 << n)

    @staticmethod
    def G(r, word):
        mask = (1 << 8) - 1
        final = 0
        for i in range(4):
            part = word & mask
            word >>= 8
            r = part & 0x0F
            l = (part & 0xF0) >> 4
            result = Algorithm.H[l][r]
            result <<= 8 * i
        return Algorithm.lambd_r(final, 32, r)

    def encrypt_block(self, X):
        if self.get_bit_frames(X) != 4:
            raise ValueError()
        d = X & 0xFFFFFFFF
        X >>= 32
        c = X & 0xFFFFFFFF
        X >>= 32
        b = X & 0xFFFFFFFF
        X >>= 32
        a = X

        for i in range(1, 9):
            b = b ^ self.G(5, self.square_plus(a, self.K[7*i - 7]))
            c = c ^ self.G(21, self.square_plus(d, self.K[7*i - 6]))
            a = self.square_minus(a, self.G(13, self.square_plus(b, self.K[7*i - 5])))
            e = self.G(21, self.square_plus(self.square_plus(b, c), self.K[7*i - 4] )) ^ (i % (2 ** 32))
            b = self.square_plus(b, e)
            c = self.square_minus(c, e)
            d = self.square_plus(d, self.G(13,self.square_plus(c, self.K[7*i - 3])))
            b = b ^ self.G(21, self.square_plus(a, self.K[7*i - 2]))
            c = c ^ self.G(5, self.square_plus(d, self.K[7*i - 1]))
            a, b = b, a
            c, d = d, c
            b, c = c, b

        return (b << 96) + (d << 64) + (a << 32) + c

    def decrypt_block(self, X):
        if self.get_bit_frames(X) != 4:
            raise ValueError()
        d = X & 0xFFFFFFFF
        X >>= 32
        c = X & 0xFFFFFFFF
        X >>= 32
        b = X & 0xFFFFFFFF
        X >>= 32
        a = X

        for i in range(8, 0, -1):
            b = b ^ self.G(5, self.square_plus(a, self.K[7*i - 1]))
            c = c ^ self.G(21, self.square_plus(d, self.K[7*i - 2]))
            a = self.square_minus(a, self.G(13, self.square_plus(b, self.K[7*i - 3])))
            e = self.G(21, self.square_plus(self.square_plus(b, c), self.K[7*i - 4] )) ^ (i % (2 ** 32))
            b = self.square_plus(b, e)
            c = self.square_minus(c, e)
            d = self.square_plus(d, self.G(13,self.square_plus(c, self.K[7*i - 5])))
            b = b ^ self.G(21, self.square_plus(a, self.K[7*i - 6]))
            c = c ^ self.G(5, self.square_plus(d, self.K[7*i - 7]))
            a, b = b, a
            c, d = d, c
            a, d = d, a

        return (c << 96) + (a << 64) + (d << 32) + b

    def split_message(self, message):
        chunks = []
        chunk = 0
        while message:
            chunk = message & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            chunks.append(chunk)
            message >>= 128
        return chunks

    def join_answer(self, chunks):
        answer = 0
        for chunk in chunks:
            answer <<= 128# self.len_map[chunk]
            answer += chunk
        return answer

    def encrypt_block_plain(self, chunks):
        results = []

        for X in chunks:
            Y = self.encrypt_block(X)
            results.append(Y)

        return results

    def decrypt_block_plain(self, chunks):
        results = []

        for X in chunks:
            Y = self.decrypt_block(X)
            results.append(Y)

        return results

    def encrypt_block_coupled(self, chunks, S):
        results = []

        S = self.encrypt_block(S)
        for X in chunks:
            Y = self.encrypt_block(X ^ S)
            results.append(Y)
            S = Y

        return results

    def decrypt_block_coupled(self, chunks, S):
        results = []

        S = self.encrypt_block(S)
        for X in chunks:
            Y = self.decrypt_block(X) ^ S
            results.append(Y)
            S = X

        return results

    def L(self, X, Y):
        l = len(bin(X)) - 2
        res = int(bin(Y)[2:l+2], base=2)
        return Y

    def encrypt_block_gamming(self, chunks, S):
        results = []

        for X in chunks:
            Y = X ^ self.L(X, self.encrypt_block(S))
            results.append(Y)
            S = Y

        return results

    def decrypt_block_gamming(self, chunks, S):
        results = []

        for X in chunks:
            Y = X ^ self.L(X, self.encrypt_block(S))
            results.append(Y)
            S = X

        return results

    def encrypt_counter(self, chunks, S):
        results = []

        s = self.encrypt_block(S)
        for X in chunks:
            s = self.square_plus(s, 1)
            Y = X ^ self.L(X, self.encrypt_block(s))
            results.append(Y)

        return results

    def decrypt_counter(self, chunks, S):
        return self.encrypt_counter(chunks, S)

    def encrypt(self, message: str, **kwargs):
        plain_msg = int.from_bytes(message.encode(), 'big')
        chunks = self.split_message(plain_msg)

        S = kwargs.get('S', 12)

        #print(chunks)

        if self.mode == self.BLOCK_COUPLED_MODE:
            results = self.encrypt_block_coupled(chunks, S)
        elif self.mode == self.PLAIN_MODE:
            results = self.encrypt_block_plain(chunks)
        elif self.mode == self.GAMMING_WITH_FEEDBACK_MODE:
            results = self.encrypt_block_gamming(chunks, S)
        elif self.mode == self.COUNTER_MODE:
            results = self.encrypt_counter(chunks, S)
        else:
            raise ValueError("Mode not set")

        #print(results)

        answer = self.join_answer(results)

        return answer.to_bytes((answer.bit_length() + 7) // 8, 'big')

    def decrypt(self, message: bytes, **kwargs):
        plain_msg = int.from_bytes(message, 'big')
        chunks = reversed(self.split_message(plain_msg))

        S = kwargs.get('S', 12)

        #print(chunks)

        if self.mode == self.BLOCK_COUPLED_MODE:
            results = self.decrypt_block_coupled(chunks, S)
        elif self.mode == self.PLAIN_MODE:
            results = self.decrypt_block_plain(chunks)
        elif self.mode == self.GAMMING_WITH_FEEDBACK_MODE:
            results = self.decrypt_block_gamming(chunks, S)
        elif self.mode == self.COUNTER_MODE:
            results = self.decrypt_counter(chunks, S)
        else:
            raise ValueError("Mode not set")

        #print(results)

        answer = self.join_answer(reversed(results))
        return answer.to_bytes((answer.bit_length() + 7) // 8, 'big').decode()


#k = 1155847461956425427909450641555173251002589277076845721198


def main(argv=None):
    filename = sys.argv[1]
    key = int.from_bytes(sys.argv[2].encode(), 'big')
    action = sys.argv[3]
    mode = sys.argv[4]
    try:
        S = int(sys.argv[5])
    except:
        S = 12

    if action == 'e':
        with open(filename) as f:
            text = f.read()
        s = Algorithm(key, mode=mode)
        with open("encrypted.txt", "wb") as f:
            f.write(s.encrypt(text, S=S))

    else:
        with open(filename, 'rb') as f:
            data = f.read()
        s = Algorithm(key, mode=mode)
        with open("decrypted.txt", "w") as f:
            f.write(s.decrypt(data, S=S))


if __name__ == "__main__":
    main()
