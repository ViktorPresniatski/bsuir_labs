import math
import scipy.stats
from matplotlib import pyplot
from datetime import datetime
import random

N = 10 ** 6 // 20


def t(l, n):
    return scipy.stats.t.ppf(l, n - 1)


def chi(l, n):
    return scipy.stats.chi2.ppf(l, n - 1)


class BRVGenerator:
    T = 13
    Q = 37
    b = 6

    @classmethod
    def random(cls):
        return next(cls.generate_iter(N))

    @classmethod
    def generate(cls, n):
        return list(cls.generate_iter(n))

    @classmethod
    def generate_iter(cls, n):
        cur_A = cls.get_A0()
        k = cls.calculate_k()
        m = 10 ** cls.b

        for i in range(n):
            A_i = (k * cur_A) % (10 ** cls.b)
            yield A_i / m
            cur_A = A_i

    @classmethod
    def calculate_k(cls):
        return 200 * cls.T - cls.Q

    @classmethod
    def get_A0(cls):
        a = datetime.now().microsecond
        return (a & 1) + a + 1

    @classmethod
    def distribution_func(cls, x):
        return x


class NormGenerator:
    e = 0.001
    brv_generator = random

    @classmethod
    def generate(cls, n, method='polar'):
        return list(getattr(cls, f"generate_iter_{method}")(n))

    @classmethod
    def generate_iter_polar(cls, n):
        for i in range(n):
            while True:
                u = 2 * cls.brv_generator.random() - 1
                v = 2 * cls.brv_generator.random() - 1
                s = u * u + v * v

                if 0 <= s <= 1:
                    break

            r = math.sqrt(-2 * math.log(s) / s)
            x = r * u
            yield x

    @classmethod
    def generate_iter_zig(cls, n):
        for i in range(n):
            while True:
                u = cls.brv_generator.random()
                e = -math.log(u)
                if u < e ** (-(e - 1) ** 2):
                    x = e
                else:
                    continue

                yield -abs(x) if cls.brv_generator.random() < 0.5 else abs(x)
                break

    @classmethod
    def generate_iter_zig_optimized(cls, n):
        for i in range(n):
            while True:
                e1 = -math.log(cls.brv_generator.random())
                e2 = -math.log(cls.brv_generator.random())
                if e2 > (e1 - 1) ** 2 / 2:
                    x = e1
                else:
                    continue

                yield -abs(x) if cls.brv_generator.random() < 0.5 else abs(x)
                break

    @classmethod
    def distribution_func(cls, x):
        return scipy.stats.norm.cdf(x)


class Tester:
    def __init__(self, generator, n=10000):
        self.generator = generator
        self.z = generator.generate(n)

    def test_independency(self):
        step = 1
        cor = []
        for step in range(1, 5):
            n = 5 * 10 ** step
            cor.append(abs(self.calculate_correlation(n)))

        print(f"Correlation - {cor}")

    def calculate_correlation(self, n=1000):
        x = self.generator.generate(n)
        y = self.generator.generate(n)
        M_xy = self.calculate_math_exp([x[i] * y[i] for i in range(n)])
        M_x = self.calculate_math_exp(x)
        M_y = self.calculate_math_exp(y)
        D_x = self.calculate_variance(x)
        D_y = self.calculate_variance(y)
        return (M_xy - M_x * M_y) / math.sqrt(D_x * D_y)

    def calculate_math_exp(self, z):
        return sum(z) / len(z)

    def calculate_variance(self, z):
        m_star = self.calculate_math_exp(z)
        n = len(z)
        return sum([(z[i] - m_star) ** 2 for i in range(n)]) / (n - 1)

    def test_point_estimate(self):
        m = []
        d = []
        step = 1
        for step in range(1, 5):
            n = 5 * 10 ** step
            z = self.generator.generate(n)
            m.append(self.calculate_math_exp(z))
            d.append(self.calculate_variance(z))

        print(f"math exp - {m}")
        print(f"variance - {d}")

    def test_hypothesis_by_kolmogorov(self, f_x, z):
        n = len(z)
        lambda_x = 1.38
        z.sort()
        F_x = [f_x(z_i) for z_i in z]
        F0_x = [(i + 1) / n for i, z_i in enumerate(z)]
        max_delta = max([F0_x[i] - F_x[i] for i in range(n)])
        lamb = math.sqrt(n) * max_delta
        print(f"kolmogorov - {(lamb, lambda_x)}")
        return lamb < lambda_x

    def test_mean_interval_estimate(self, z):
        l = 0.99
        n = len(z)
        koef = t(l, n)
        m_x = self.calculate_math_exp(z)
        d = self.calculate_variance(z)
        temp = (d * koef) / math.sqrt(n - 1)
        left = m_x - temp
        right = m_x + temp
        print(f"interval math exp - {(left, right)}")
        return (left, right)

    def test_var_interval_estimate(self, z):
        n = len(z)
        l = 0.99
        chi_left = chi(1 - (1 - l)/2, n)
        chi_right = chi(1 - (1 + l)/2, n)
        d = self.calculate_variance(z)
        left = n * d / chi_left
        right = n * d / chi_right
        print(f"interval variance - {(left, right)}")
        return (left, right)

    def test_all(self):
        self.test_point_estimate()
        self.test_independency()
        pyplot.hist(self.z, 30, density=True)
        pyplot.show()

        self.test_mean_interval_estimate(self.z)
        self.test_var_interval_estimate(self.z)
        self.test_hypothesis_by_kolmogorov(self.generator.distribution_func, self.z)



tester = Tester(BRVGenerator)
tester.test_all()


# z = NormGenerator.generate(10000, method='polar')
# pyplot.hist(z, 30)
# pyplot.show()
# z = NormGenerator.generate(10000, method='inv')
# pyplot.hist(z, 30)
# pyplot.show()
# z = NormGenerator.generate(10000, method='inv_optimized')
# pyplot.hist(z, 30)
# pyplot.show()

tester = Tester(NormGenerator)
tester.test_all()