import math
import logging

from functools import reduce
from numpy.random import exponential
from heapq import heappush, heappop
import matplotlib.pyplot as plt
from numpy import inf


class QueuingSystem:
    REQUEST = 'REQUEST'
    HANDLING = 'HANDLING'
    LEAVING = 'LEAVING'

    def __init__(self, n, m, lamb, mu, v, end_time, log=False):
        self.n, self.m, self.lamb, self.mu, self.v = n, m, lamb, mu, v
        self.time_line = []
        self.cur_time = 0
        self.end_time = end_time
        self.state = 0
        self.rejection = 0
        self.request_count = 0
        self.p_vector = {}
        self.demonstration = {'times': [], 'states': []}
        self.action_mapping = {
            self.REQUEST: self.make_request,
            self.HANDLING: self.handle_request,
            self.LEAVING: self.leave_queue,
        }
        self.requests = []
        self.logger = logging.getLogger('spam_application')
        self.logger.addHandler(logging.StreamHandler())
        self.logger.setLevel(logging.INFO if log else logging.ERROR)

    def run(self):
        self.generate_new_request()

        while True:
            prev_time = self.cur_time
            prev_state = self.state
            self.demonstration['times'].append(prev_time)
            self.demonstration['states'].append(prev_state)
            self.cur_time, cur_action, *_ = heappop(self.time_line)

            if self.cur_time > self.end_time:
                return

            self.action_mapping[cur_action]()
            self.p_vector[prev_state] = self.p_vector.get(prev_state, 0) + (self.cur_time - prev_time)

    def make_request(self):
        self.generate_new_request()

        if self.state >= self.n + self.m:
            self.logger.info('REJECTION - {}'.format(self.cur_time))
            self.rejection += 1
            return

        self.increase_state()
        self.logger.info('MADE REQUEST - {}'.format(self.cur_time))
        if self.state <= self.n:
            self.accept_request()
        elif self.state <= self.n + self.m:
            self.put_in_queue()

    def accept_request(self):
        hadling_time = self.cur_time + exponential(1 / self.mu)
        heappush(self.time_line, (hadling_time, self.HANDLING))

    def handle_request(self):
        self.decrease_state()
        self.logger.info('HANDLE REQUEST - {}'.format(self.cur_time))
        if self.state >= self.n:
            self.delete_leaving_action()
            self.accept_request()

    def put_in_queue(self):
        leaving_time = self.cur_time + exponential(1 / self.v)
        heappush(self.time_line, (leaving_time, self.LEAVING, self.request_count))

    def leave_queue(self):
        self.decrease_state()
        self.logger.info('LEVING - {}'.format(self.cur_time))

    def generate_new_request(self):
        new_request_time = self.cur_time + exponential(1 / self.lamb)
        heappush(self.time_line, (new_request_time, self.REQUEST))
        self.requests.append(self.cur_time)
        self.request_count += 1

    def delete_leaving_action(self):
        min_leaving_action = min([t for t in self.time_line if len(t) == 3], key=lambda x: x[2])
        self.time_line.remove(min_leaving_action)

    def increase_state(self):
        self.state += 1
        self.logger.info("State - {}".format(self.state))

    def decrease_state(self):
        self.state -= 1
        self.logger.info("State - {}".format(self.state))

    def compare_characteristics(self):
        p_theory = self.theory_final_probability()
        theory_rejection_probability = p_theory[-1]
        theory_service_probabilty = 1 - theory_rejection_probability
        theory_absolute_bandwidth = self.lamb * theory_service_probabilty
        theory_L_smo = self.avg_service(p_theory)
        theory_L_queue = self.avg_queue(p_theory)
        theory_T_queue = theory_L_queue / self.lamb
        theory_T_smo = self.avg_time_in_smo(theory_T_queue, theory_service_probabilty)

        p_exp = self.emp_final_probability()
        emp_rejection_probability = self.rejection / self.request_count
        emp_service_probabilty = 1 - emp_rejection_probability
        emp_absolute_bandwidth = self.lamb * emp_service_probabilty
        emp_L_smo = self.avg_service(p_exp)
        emp_L_queue = self.avg_queue(p_exp)
        emp_T_queue = emp_L_queue / self.lamb
        emp_T_smo = self.avg_time_in_smo(emp_T_queue, emp_service_probabilty)

        print("\nP:\n{}\n{}".format(p_theory, p_exp))
        print("P_otk: {} - {}".format(theory_rejection_probability, emp_rejection_probability))
        print("P_obs: {} - {}".format(theory_service_probabilty, emp_service_probabilty))
        print("A: {} - {}".format(theory_absolute_bandwidth, emp_absolute_bandwidth))
        print("L_queue: {} - {}".format(theory_L_queue, emp_L_queue))
        print("T_queue: {} - {}".format(theory_T_queue, emp_T_queue))
        print("L_smo: {} - {}".format(theory_L_smo, emp_L_smo))
        print("T_smo: {} - {}".format(theory_T_smo, emp_T_smo))

    def theory_final_probability(self):
        p = self.lamb / self.mu
        beta = self.v / self.mu
        p0 = sum([p ** i / math.factorial(i) for i in range(self.n + 1)])

        def mult(j):
            return reduce(lambda x, y: x * y, [self.n + l * beta for l in range(1, j + 1)])

        if self.m == inf:
            self.m = len(self.p_vector) - self.n - 1

        p0 += (p ** self.n / math.factorial(self.n)) * sum([p ** i / mult(i) for i in range(1, self.m + 1)])
        p0 = 1 / p0

        p_theory = [p0]
        for k in range(1, self.n + 1):
            p_theory.append(p0 * p ** k / math.factorial(k))

        pn = p_theory[-1]
        for i in range(1, self.m + 1):
            p_theory.append(pn * p ** i / mult(i))

        return p_theory

    def emp_final_probability(self):
        return [val / self.end_time for k, val in self.p_vector.items()]

    def avg_queue(self, p_vector):
        return sum([i * el for i, el in enumerate(p_vector) if i > self.n])

    def avg_service(self, p_vector):
        channels = sum([i * el for i, el in enumerate(p_vector) if i <= self.n])
        queue = sum([self.n * el for i, el in enumerate(p_vector) if i > self.n])
        return channels + queue

    def avg_time_in_smo(self, T_queue, Q):
        return T_queue + Q / self.mu


def run_quering_system(n, m, lamb, mu, v, end_time, log=False):
    system = QueuingSystem(n, m, lamb, mu, v, end_time, log=log)
    system.run()
    return system


def test_stationarity():
    n = 4
    m = 2
    lamb = 3
    mu = 1
    v = 2

    times = []
    p_otk = []
    for time in range(1000, 10001, 100):
        system = QueuingSystem(n, m, lamb, mu, v, time)
        system.run()
        times.append(time)
        p_otk.append(system.rejection / system.request_count)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(times, p_otk)
    plt.show()


def demonstrate():
    n = 2
    m = 4
    lamb = 2
    mu = 1
    v = 2
    end_time = 50

    system = run_quering_system(n, m, lamb, mu, v, end_time, log=True)
    fig = plt.figure()
    bx = fig.add_subplot(111)
    system.demonstration['times'].sort()
    bx.plot(system.demonstration['times'], system.demonstration['states'])
    plt.show()


def test_stationarity_via_intervals(system, count):
    dist = []
    iterator = iter(system.requests)
    i = 0
    delta = system.end_time / count
    for i in range(count):
        cur = []
        next_el = next(iterator)
        while next_el < (i + 1) * delta:
            cur.append(next_el)
            try:
                next_el = next(iterator)
            except:
                break
        dist.append(len(cur))

    fig = plt.figure()
    cx = fig.add_subplot(111)
    cx.plot(dist)
    plt.show()


def run_system_with_comparison_characteristics(log=False):
    n = 2
    m = 3
    lamb = 1
    mu = 0.8
    v = 0.5
    end_time = 10000
    system = QueuingSystem(n, m, lamb, mu, v, end_time, log=log)
    system.run()
    system.compare_characteristics()

    test_stationarity_via_intervals(system, 500)
    test_stationarity_via_intervals(system, 1000)
    test_stationarity_via_intervals(system, 2000)


if __name__ == '__main__':
    # run_system_with_comparison_characteristics(False)
    demonstrate()
    # test_stationarity()
