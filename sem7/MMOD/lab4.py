import math
import random
import numpy as np
import scipy.fftpack
from matplotlib import pyplot as plt

from functools import reduce
from itertools import islice



def calculate_math_exp(z):
    return np.array(z).mean()


def calculate_variance(z):
    return np.array(z).var()


def corr_func(deviation, a):
    def wrapped(dt):
        mult = a * abs(dt)
        return deviation ** 2 * math.exp(-mult) * (1 + mult)
    return wrapped


def h_func(deviation, a):
    def wrapped(dt):
        k = deviation * math.sqrt(1 / (a * math.pi))
        result = k * (a ** 2) * dt * math.exp(-a * dt)
        return result
    return wrapped


def autocorrelation_exp(y):
    n = len(y)
    mean = calculate_math_exp(y)
    x = [y_i - mean for y_i in y]
    r = np.correlate(x, x, mode='full')[-n:]
    r /= r[r.argmax()]
    return r


def autocorrelation_theory(corr, time_line):
    r = np.array([corr(time) for time in time_line])
    r /= r[r.argmax()]
    return r


def build_spectr(corr, dt):
    spectr = scipy.fftpack.fft(corr)
    return np.abs(spectr[:spectr.size // 2])


def generate_random_process(deviation, a, dt, noise_size, process_size):
    time_line = np.arange(0, process_size, dt)
    h = h_func(deviation, a)
    corr = corr_func(deviation, a)
    x = generate_input_signal(deviation, len(time_line))
    y = generate_output_signal(x, noise_size, h, dt)
    mean = calculate_math_exp(y)
    variance = calculate_variance(y)
    show_results(time_line, x, y, corr, a)


def generate_input_signal(deviation, size):
    # return [random.normalvariate(0, deviation) for _ in range(size)]
    return [np.random.uniform(-1.1, 1.1) for _ in range(size)]


def generate_output_signal(input_signal, noise_size, h, dt):
    y = []
    for j, x_j in enumerate(input_signal):
        y_j = 0

        for i in islice(range(noise_size + 1), j + 1):
            y_j += input_signal[j - i] * h(i * dt)

        y.append(y_j)

    return y


def show_results(time_line, input_signal, output_signal, corr, alfa):
    fig1 = plt.figure(figsize=(20,10))
    show_stationarity_proof(fig1, output_signal, alfa)
    plt.show()

    fig = plt.figure(figsize=(20,10))
    show_input_signal_chart(fig, time_line, input_signal)
    show_output_signal_chart(fig, time_line, output_signal)
    show_correlation_comparison(fig, time_line, corr, output_signal)
    show_spectr_comparison(fig, time_line, corr, output_signal)
    plt.show()


def show_stationarity_proof(fig, output_signal, alfa):
    ax = fig.add_subplot(111)
    bx = fig.add_subplot(111)
    plt.ylim(-10, 10)
    plt.title('Statioinarity comparison')
    means = []
    variances = []
    size = len(output_signal)
    batches_count = 10
    batch_size = size // batches_count
    for i in range(0, batch_size * batches_count, batch_size):
        batch = output_signal[i:i+batch_size]
        mean = calculate_math_exp(batch)
        variance = calculate_variance(batch)
        means.append(mean)
        variances.append(4 * variance / alfa)

    plt1 = ax.plot(range(batches_count), means)
    plt2 = bx.plot(range(batches_count), variances)
    plt.legend((plt1[0], plt2[0]), ('Mean', 'Variance'))


def show_input_signal_chart(fig, time_line, input_signal):
    ax = fig.add_subplot(221)
    plt.title('Input signal')
    ax.plot(time_line, input_signal)


def show_output_signal_chart(fig, time_line, output_signal):
    ax = fig.add_subplot(222)
    plt.title('Proccess')
    ax.plot(time_line, output_signal)


def show_correlation_comparison(fig, time_line, corr, output_signal):
    ax = fig.add_subplot(223)
    plt.title('Autocorrelation comparison')
    theory_corr = autocorrelation_theory(corr, time_line)
    exp_corr = autocorrelation_exp(output_signal)
    plt1 = plt.plot(time_line[:50], theory_corr[:50])
    plt2 = plt.plot(time_line[:50], exp_corr[:50])


def show_spectr_comparison(fig, time_line, corr, output_signal):
    ax = fig.add_subplot(224)
    plt.title('Spectr')
    limit = 50
    theory_corr = autocorrelation_theory(corr, time_line)
    exp_corr = autocorrelation_exp(output_signal)
    theory_spectr = build_spectr(theory_corr[:limit], time_line[1])
    exp_spectr = build_spectr(exp_corr[:limit], time_line[1])
    axis_x = np.linspace(0.0, 1.0, limit / 2)
    plt1 = plt.plot(axis_x, theory_spectr)
    plt2 = plt.plot(axis_x, exp_spectr)


if __name__ == '__main__':
    deviation = 1
    a = 2
    dt_max = 6.64 / a
    noise_size = 50
    process_size = 300
    dt = dt_max / noise_size
    generate_random_process(deviation, a, dt, noise_size, process_size)
