import math
import scipy.stats
import numpy as np
from matplotlib import pyplot
from datetime import datetime
from random import random
from scipy.stats import poisson
from collections import Counter
from mpl_toolkits.mplot3d import Axes3D


def t(l, n):
    return scipy.stats.t.ppf(l, n - 1)


def chi(l, n):
    return scipy.stats.chi2.ppf(l, n - 1)

class Tester:
    def __init__(self, generator, n=10000):
        self.generator = generator
        self.z = generator.generate(n)

    def t(self, l, n):
        return scipy.stats.t.ppf(l, n - 1)

    def chi(self, l, n):
        return scipy.stats.chi2.ppf(l, n - 1)

    def test_independency(self):
        step = 1
        cor = []
        for step in range(1, 5):
            n = 5 * 10 ** step
            cor.append(abs(self.calculate_correlation(n)))

        print(f"Correlation - {cor}")

    def calculate_correlation(self, n=1000):
        x = self.generator.generate(n)
        y = self.generator.generate(n)
        M_xy = self.calculate_math_exp([x[i] * y[i] for i in range(n)])
        M_x = self.calculate_math_exp(x)
        M_y = self.calculate_math_exp(y)
        D_x = self.calculate_variance(x)
        D_y = self.calculate_variance(y)
        return (M_xy - M_x * M_y) / math.sqrt(D_x * D_y)

    def calculate_math_exp(self, z):
        return sum(z) / len(z)

    def calculate_variance(self, z):
        m_star = self.calculate_math_exp(z)
        n = len(z)
        return sum([(z[i] - m_star) ** 2 for i in range(n)]) / (n - 1)

    def test_point_estimate(self):
        m = []
        d = []
        step = 1
        for step in range(1, 5):
            n = 5 * 10 ** step
            z = self.generator.generate(n)
            m.append(self.calculate_math_exp(z))
            d.append(self.calculate_variance(z))

        print(f"math exp - {m}")
        print(f"variance - {d}")

    def test_hypothesis_by_kolmogorov(self, f_x, z):
        n = len(z)
        lambda_x = 1.38
        z.sort()
        F_x = [f_x(z_i) for z_i in z]
        F0_x = [(i + 1) / n for i, z_i in enumerate(z)]
        max_delta = max([F0_x[i] - F_x[i] for i in range(n)])
        lamb = math.sqrt(n) * max_delta
        print(f"kolmogorov - {(lamb, lambda_x)}")
        return lamb < lambda_x

    def test_mean_interval_estimate(self, z):
        l = 0.99
        n = len(z)
        koef = self.t(l, n)
        m_x = self.calculate_math_exp(z)
        d = self.calculate_variance(z)
        temp = (d * koef) / math.sqrt(n - 1)
        left = m_x - temp
        right = m_x + temp
        print(f"interval math exp - {(left, right)}")
        return (left, right)

    def test_var_interval_estimate(self, z):
        n = len(z)
        l = 0.99
        chi_left = self.chi(1 - (1 - l) / 2, n)
        chi_right = self.chi(1 - (1 + l) / 2, n)
        d = self.calculate_variance(z)
        left = n * d / chi_left
        right = n * d / chi_right
        print(f"interval variance - {(left, right)}")
        return (left, right)

    def test_all(self):
        self.test_point_estimate()
        pyplot.hist(self.z, 25, density=True)
        pyplot.show()

        self.test_mean_interval_estimate(self.z)
        self.test_var_interval_estimate(self.z)
        self.test_hypothesis_by_kolmogorov(self.generator.distribution_func, self.z)


class DRVPoissonGenerator:
    eps = 0.000001

    def __init__(self, lamb, p):
        self.lamb, self.p = lamb, p

    def random(self):
        k = 0
        n = round(self.lamb / self.p) + 1
        for _ in range(n):
            u = random()
            if u < p:
                k += 1

        return k

    def distribution_func(self, k):
        f = lambda x: (self.lamb ** x) * math.exp(-self.lamb) / math.factorial(x)
        return sum([f(i) for i in range(k + 1)])


class DRVPoissonKnuthGenerator:
    def __init__(self, lamb):
        self.lamb = lamb

    def random(self):
        exp_lamb_inv = math.exp(-self.lamb)
        k, mult = 0, random()
        while mult > exp_lamb_inv:
            mult *= random()
            k += 1

        return k

    def distribution_func(self, k):
        f = lambda x: (self.lamb ** x) * math.exp(-self.lamb) / math.factorial(x)
        return sum([f(i) for i in range(k + 1)])


class CustomGenerator:
    def __init__(self, p_list, X=None):
        self.p_list = p_list or [0.5, 0.2, 0.3]
        self.X = X or list(range(len(self.p_list)))

    def random(self):
        p_iter = iter(self.p_list)
        p_sum = 0
        k = 0
        u = random()
        while u > p_sum:
            p_sum += next(p_iter)
            k += 1

        return self.X[k - 1]


class DRVGenerator:
    def __init__(self, generator):
        self.generator = generator

    def random(self):
        return self.generator.random()

    def generate_iter(self, n):
        return (self.random() for _ in range(n))

    def generate(self, n):
        return list(self.generate_iter(n))

    def distribution_func(self, x):
        return self.generator.distribution_func(x)


class DDRVGenerator:
    def __init__(self, X, Y, p_matrix):
        self.X, self.Y, self.p_matrix = X, Y, p_matrix

    def random(self):
        rows = [sum(row) for row in self.p_matrix]
        u = random()
        p_sum = 0
        x1 = 0
        while u > p_sum:
            p_sum += rows[x1]
            x1 += 1

        x1_row = p_matrix[x1 - 1]
        y_rows = [el / rows[x1 - 1] for el in x1_row]
        u = random()
        y_sum = 0
        x2 = 0
        while u > y_sum:
            y_sum += y_rows[x2]
            x2 += 1

        return (self.X[x1 - 1], self.Y[x2 - 1])

    def calculate_theory_math_exp(self):
        x_rows = [sum(row) for row in self.p_matrix]
        mx = sum([self.X[i] * r for i, r in enumerate(x_rows)])
        p_matrix_t = zip(*self.p_matrix)
        y_rows = [sum(row) for row in p_matrix_t]
        my = sum([self.Y[i] * r for i, r in enumerate(y_rows)])
        return (mx, my)

    def calculate_theory_variance(self):
        mx, my = self.calculate_theory_math_exp()
        x_rows = [sum(row) for row in self.p_matrix]
        dx = sum([(self.X[i] - mx) ** 2 * r for i, r in enumerate(x_rows)])
        p_matrix_t = zip(*self.p_matrix)
        y_rows = [sum(row) for row in p_matrix_t]
        dy = sum([(self.Y[i] - my) ** 2 * r for i, r in enumerate(y_rows)])
        return (dx, dy)


class TwoDimensionalTester:
    def __init__(self, generator, n=10000):
        self.generator = generator
        self.random = generator.generator
        self.z = generator.generate(n)

    def calculate_math_exp(self, z):
        mx = [i[0] for i in z]
        my = [i[1] for i in z]
        mx = sum(mx) / len(mx)
        my = sum(my) / len(my)
        return (mx, my)

    def calculate_variance(self, z):
        mx_star, my_star = self.calculate_math_exp(z)
        x = [i[0] for i in z]
        y = [i[1] for i in z]
        n_x, n_y = len(x), len(y)
        dx = sum([(x[i] - mx_star) ** 2 for i in range(n_x)]) / (n_x - 1)
        dy = sum([(y[i] - my_star) ** 2 for i in range(n_y)]) / (n_y - 1)
        return (dx, dy)

    def calculate_emp_matrix(self, z=None):
        z = z or self.z
        n = len(z)
        counter = Counter(z or self.z)
        X, Y = self.random.X, self.random.Y
        p_matrix = [[0 for j in range(len(Y))] for i in range(len(X))]
        for key, value in counter.items():
            p_matrix[X.index(key[0])][Y.index(key[1])] = value / n
        return p_matrix

    def calculate_correlation(self, n=10000, log=True):
        z = self.generator.generate(n)
        mx, my = self.calculate_math_exp(self.z)
        dx, dy = self.calculate_variance(self.z)
        X, Y = self.random.X, self.random.Y
        p_matrix = self.calculate_emp_matrix(z)
        K_xy = sum([x_i * y_j * p_matrix[i][j] for i, x_i in enumerate(X) for j, y_j in enumerate(Y)]) - mx * my
        coef = K_xy / math.sqrt(dx * dy)
        log and print(f"corr - {coef}")
        return coef

    def test_point_estimate(self, log=True):
        m = []
        d = []
        step = 1
        for step in range(1, 6):
            n = 5 * 10 ** step
            z = self.generator.generate(n)
            m.append(self.calculate_math_exp(z))
            d.append(self.calculate_variance(z))

        theory_math_exp = self.random.calculate_theory_math_exp()
        theory_variance = self.random.calculate_theory_variance()
        log and print(f"math exp - {m} => {theory_math_exp}")
        log and print(f"variance - {d} => {theory_variance}")
        return (theory_math_exp, theory_variance)

    def show_exp_hist(self, z):
        X, Y = self.random.X, self.random.Y
        p_matrix = self.calculate_emp_matrix(z)
        fig = pyplot.figure()
        ax1 = fig.add_subplot(111, projection='3d')

        x = []
        for j in range(len(X)):
            x.extend([j] * len(Y))
        y = list(range(len(Y))) * len(X)

        z = np.zeros(len(X) * len(Y))    
        dx = 1 
        dy = 1
        dz = [col for row in p_matrix for col in row]
        ax1.bar3d(x, y, z, dx, dy, dz, color='#00ceaa')
        pyplot.show()

    def show_hist(self, z):
        x = [i[0] for i in z]
        y = [i[1] for i in z]
        pyplot.hist(x, density=True)
        pyplot.show()
        pyplot.hist(y, density=True)
        pyplot.show()

    def test_mean_interval_estimate(self, z):
        l = 0.99
        n = len(z)
        koef = t(l, n)
        mx, my = self.calculate_math_exp(z)
        dx, dy = self.calculate_variance(z)
        temp_x = (dx * koef) / math.sqrt(n - 1)
        temp_y = (dy * koef) / math.sqrt(n - 1)
        left_x, right_x = mx - temp_x, mx + temp_x
        left_y, right_y = my - temp_y, my + temp_y
        print(f"interval math exp - {(left_x, right_x), (left_y, right_y)}")
        return (left_x, right_x), (left_y, right_y)

    def test_var_interval_estimate(self, z):
        n = len(z)
        l = 0.99
        chi_left = chi(1 - (1 - l)/2, n)
        chi_right = chi(1 - (1 + l)/2, n)
        dx, dy = self.calculate_variance(z)
        left_x, right_x = n * dx / chi_left, n * dx / chi_right
        left_y, right_y = n * dy / chi_left, n * dy / chi_right
        print(f"interval variance - {(left_x, right_x), (left_y, right_y)}")
        return (left_x, right_x), (left_y, right_y)

    def test_hypothesis_by_pirson(self, z):
        l = 0.01
        n = len(z)
        p_matrix = self.random.p_matrix
        rows = len(p_matrix)
        cols = len(p_matrix[0])
        p_matrix_emp = self.calculate_emp_matrix(z)
        arr = []
        for i, row in enumerate(p_matrix_emp):
            for j, col in enumerate(row):
                temp = (p_matrix_emp[i][j] - p_matrix[i][j]) ** 2 / p_matrix[i][j]
                arr.append(temp)
        chi2 = sum(arr) * n
        chi2_theory = scipy.stats.chi2.ppf(1 - l, (rows - 1) * (cols - 1))
        # import pdb;pdb.set_trace()
        print(f"pirson - {(chi2, chi2_theory)}")
        return chi2 < chi2_theory

    def test_hypothesis_point_estimate(self, z):
        l = 0.05
        n = len(z)
        Z0 = 1.96  # F_laplace(Z0) = (1 - l) / 2
        m, d = self.test_point_estimate(log=False)
        dx, dy = self.calculate_variance(z)
        mx, my = self.calculate_math_exp(z)
        Zx, Zy = (mx - m[0]) / (math.sqrt(dx / n)), (my - m[1]) / (math.sqrt(dy / n))
        print(f"Test hypothesis for mean point estimate: {abs(Zx)} < {Z0}, {abs(Zy)} < {Z0}")

        chi2_x, chi2_y = (dx / d[0]) * (n - 1), (dy / d[1]) * (n - 1)
        chi_l, chi_r = chi(l, n), chi(1 - l, n)
        print(f"Test hypothesis for variance point estimate: {chi_l} < {chi2_x, chi2_y} < {chi_r}")

        corr = self.calculate_correlation(log=False)
        T = corr * math.sqrt(n - 2) / math.sqrt(1 - corr ** 2)
        t_kr = t(1 - l, n - 1)
        print(f"Test hypothesis for correlation: {-t_kr} < {abs(T)} < {t_kr}")


    def test_all(self):
        self.test_point_estimate()
        self.calculate_correlation()
        self.show_exp_hist(self.z)
        self.show_hist(self.z)
        self.test_mean_interval_estimate(self.z)
        self.test_var_interval_estimate(self.z)
        self.test_hypothesis_by_pirson(self.z)
        self.test_hypothesis_point_estimate(self.z)


if __name__ == '__main__':    
    lamb = 10
    # generator = DRVPoissonGenerator(lamb=lamb, p=0.01)
    generator = DRVPoissonKnuthGenerator(lamb=lamb)
    list_generator = DRVGenerator(generator)
    tester = Tester(list_generator)
    tester.test_all()


    X = [4, 7, 9]
    Y = [1, 3, 5]
    p_matrix = [[0.1, 0.2, 0.1],
                [0.05, 0.3, 0.05],
                [0.05, 0.1, 0.05]]

    generator = DDRVGenerator(X, Y, p_matrix)
    list_generator = DRVGenerator(generator)
    tester = TwoDimensionalTester(list_generator)
    tester.test_all()
