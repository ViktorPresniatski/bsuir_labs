import numpy as np
from simplex_method import get_basic_indices

# example 1
# A = np.array([
#     [2, 1, -1, 0, 0, 1],
#     [1, 0, 1, 1, 0, 0],
#     [0, 1, 0, 0, 1, 0]
# ])
# b = np.array([2, 5, 0])
# c = np.array([3, 2, 0, 3, -2, -4])
# d_min = np.array([0, -1, 2, 1, -1, 0])
# d_max = np.array([2, 4, 4, 3, 3, 5])

# task 1
A = np.array([
    [1, -5, 3, 1, 0, 0],
    [4, -1, 1, 0, 1, 0],
    [2, 4, 2, 0, 0, 1]
])
b = np.array([-7, 22, 30])
c = np.array([7, -2, 6, 0, 5, 2])
d_min = np.array([2, 1, 0, 0, 1, 1])
d_max = np.array([6, 6, 5, 2, 4, 6])

# task 4
# A = np.array([
#     [1, -3, 2, 0, 1, -1, 4, -1, 0],
#     [1, -1, 6, 1, 0, -2, 2, 2, 0],
#     [2, 2, -1, 1, 0, -3, 8, -1, 1],
#     [4, 1, 0, 0, 1, -1, 0, -1, 1],
#     [1, 1, 1, 1, 1, 1, 1, 1, 1]
# ])

# b = np.array([3, 9, 9, 5, 9])
# c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3])
# d_min = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
# d_max = np.array([5, 5, 5, 5, 5, 5, 5, 5, 5])

# task 7
# A = np.array([
#     [2, 1, 0, 3, -1, -1],
#     [0, 1, -2, 1, 0, 3],
#     [3, 0, 1, 1, 1, 1],
# ])
# b = np.array([2, 2, 5])
# c = np.array([0, -1, 1, 0, 4, 3])
# d_min = np.array([2, 0, -1, -3, 2, 1])
# d_max = np.array([7, 3, 2, 3, 4, 5])


# J_a = sorted(SimplexMethod(A, b, c).solve().basic_indexes)

class DualSimplex:
    def __init__(self, A, b, c, d_min, d_max, log=False):
        self.A = A
        self.b = b
        self.c = c
        self.d_min = d_min
        self.d_max = d_max
        self.J_b = sorted(get_basic_indices(A, b, c))
        self.m = A.shape[0]
        self.n = A.shape[1]
        self.J = range(self.n)
        self.log = print if log else self.log_null

    def solve(self):
        A, b, c, d_min, d_max, J, J_b = self.A, self.b, self.c, self.d_min, self.d_max, self.J, self.J_b
        # step 1
        J_n = self.J_not(J_b)
        A_b = A[:, J_b]
        B = np.linalg.inv(A_b)
        y_ = np.dot(c[J_b], B)
        deltas = [y_.dot(A[:, j]) - c[j] for j in J]
        J_n_plus = [j for j in J_n if deltas[j] >= 0]
        J_n_minus = [j for j in J_n if j not in J_n_plus]

        while True:
            b_copy = np.copy(b)

            # step 2
            for j in J_n:
                temp = d_min[j] if j in J_n_plus else d_max[j]
                b_copy -= A[:, j] * temp
            pseudoplan_bas = B.dot(b_copy)

            # step 3
            if self.is_optimum_plan(pseudoplan_bas, J_b):
                x = self.create_answer(pseudoplan_bas, J_b, J_n, J_n_plus)
                cx = c.dot(x)
                self.log(f"Optimum plan - {x}")
                self.log(f"c'x - {cx}")
                return {'x': x, 'cx': cx, 'J_b': J_b}

            # step 4
            k = self.find_index_k(pseudoplan_bas, J_b)
            j_k = J_b[k]

            # step 5
            mu_jk = 1 if pseudoplan_bas[k] < d_min[J_b[k]] else -1
            es = np.zeros(len(J_b))
            es[k] = 1
            delta_y = mu_jk * es.dot(B)
            mu_list = []
            for j in J_n:
                m_j = delta_y.dot(A[:, j])
                mu_list.append((j, m_j))

            # step 6
            sigma0 = np.inf
            j0 = -1
            for mu in mu_list:
                if (mu[0] in J_n_plus and mu[1] < 0) or (mu[0] in J_n_minus and mu[1] > 0):
                    sigma = -deltas[mu[0]] / mu[1]  # searching for steps
                    if sigma < sigma0:
                        sigma0 = sigma
                        j0 = mu[0]

            if np.isinf(sigma0):
                self.log("No solution")
                return {'x': None, 'cx': None}

            # self.log(f"sigma0 - {sigma0}")
            # self.log(f"j0 - {j0}")

            # step 7
            for j in J:
                if j in J_b and j_k != j:
                    deltas[j] = 0.0
                else:
                    delta = sigma0 * j_k if j == j_k else sigma0 * self.get_mu_j(mu_list, j)
                    deltas[j] += delta

            # step 8
            J_b[k] = j0
            J_b.sort()
            A_b = A[:, J_b]
            B = np.linalg.inv(A_b)

            # step 9
            J_n = self.J_not(J_b)
            if mu_jk == 1:
                if j0 in J_n_plus:
                    J_n_plus.remove(j0)
                J_n_plus.append(j_k)
            else:
                if j0 in J_n_plus:
                    J_n_plus.remove(j0)

            J_n_minus = [j for j in J_n if j not in J_n_plus]

    def J_not(self, J_b):
        return [j for j in self.J if j not in J_b]

    def is_optimum_plan(self, pseudo_bas, J):
        is_optimum = [self.d_min[J[index]] <= pseudo_bas[index] <= self.d_max[J[index]] for index, value in enumerate(pseudo_bas)]
        return all(is_optimum)

    def find_index_k(self, pseudo_bas, J):
        for index, value in enumerate(pseudo_bas):
            if not (self.d_min[J[index]] <= pseudo_bas[index] <= self.d_max[J[index]]):
                return index
        raise Exception("ERROR: index 'k' wasn't found")

    def get_mu_j(self, mu_list, j):
        return next(mu[1] for mu in mu_list if mu[0] == j)

    def create_answer(self, pseudoplan_bas, J_b, J_n, J_n_plus):
        x = [0] * self.n
        for index, value in enumerate(pseudoplan_bas):
            x[J_b[index]] = pseudoplan_bas[index]
        for j in J_n:
            x[j] = self.d_min[j] if j in J_n_plus else self.d_max[j]
        return x

    def log_null(self, string):
        return


if __name__ == '__main__':
    DualSimplex(A, b, c, d_min, d_max, log=True).solve()
