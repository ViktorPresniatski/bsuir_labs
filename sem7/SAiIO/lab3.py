import numpy as np
from simplex_method import SimplexMethod

# A = np.array([
#     [5, -1, 1, 0, 0],
#     [-1, 2, 0, 1, 0],
#     [-7, 2, 0, 0, 1]
# ])
# b = np.array([15, 6, 0], dtype=float)
# c = np.array([-3.5, 1, 0, 0, 0], dtype=float)

# A = np.array([
#     [1, -5, 3, 1, 0, 0],
#     [4, -1, 1, 0, 1, 0],
#     [2, 4, 2, 0, 0, 1]
# ])
# b = np.array([-8, 22, 30], dtype=float)
# c = np.array([7, -2, 6, 0, 5, 2], dtype=float)

A = np.array([
    [1, 0, 0, 12, 1, -3, 4, -1],
    [0, 1, 0, 11, 12, 3, 5, 3],
    [0, 0, 1, 1, 0, 22, -2, 1]
])
b = np.array([40, 107, 61], dtype=float)
c = np.array([2, 1, -2, -1, 4, -5, 5, 5], dtype=float)

# A = np.array([
#     [0, 7, 1, -1, -4, 2, 4],
#     [5, 1, 4, 3, -5, 2, 1],
#     [2, 0, 3, 1, 0, 1, 5]
# ])
# b = np.array([12, 27, 19], dtype=float)
# c = np.array([10, 2, 1, 7, 6, 3, 1], dtype=float)

A = np.array([
    [0, 7, -8, -1, 5, 2, 1],
    [3, 2, 1, -3, -1, 1, 0],
    [1, 5, 3, -1, -2, 1, 0],
    [1, 1, 1, 1, 1, 1, 1]
])
b = np.array([6, 3, 7, 7], dtype=float)
c = np.array([2, 9, 3, 5, 1, 2, 4], dtype=float)

# A = np.array([
#     [1, 0, -1, 3, -2, 0, 1],
#     [0, 2, 1, -1, 0, 3, -1],
#     [1, 2, 1, 4, 2, 1, 1],
# ])
# b = np.array([4, 8, 24], dtype=float)
# c = np.array([-1, -3, -7, 0, -4, 0, -1], dtype=float)

# A = np.array([
#     [1, -3, 2, 0, 1, -1, 4, -1, 0],
#     [1, -1, 6, 1, 0, -2, 2, 2, 0],
#     [2, 2, -1, 1, 0, -3, 2, -1, 1],
#     [4, 1, 0, 0, 1, -1, 0, -1, 1],
#     [1, 1, 1, 1, 1, 1, 1, 1, 1]
# ])
# b = np.array([3, 9, 9, 5, 9], dtype=float)
# c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3], dtype=float)


def is_integer(number):
    e = 0.001
    return abs(number - round(number)) < e


def get_fraction(number):
    return number - np.floor(number)


init_m = A.shape[0]
init_n = A.shape[1]
iter_count = 0
while True:
    iter_count += 1
    print(f"Iteration - {iter_count}")
    print(f"A - \n{A}")
    print(f"b - {b}")
    print(f"c - {c}")
    print(f"cur_m - {A.shape[0]}")
    print(f"cur_n - {A.shape[1]}")

    cur_result = SimplexMethod(A.copy(), b.copy(), c.copy()).solved()
    x = cur_result.x
    J_b = cur_result.get_basic_indices()
    J_b.sort()

    rows_count = A.shape[0]
    cols_count = A.shape[1]
    if rows_count > init_n:
        artificial_cols = sorted([j for j in J_b if j >= init_n])

        for aritificial_col in artificial_cols:
            artificial_row = init_m + aritificial_col - init_n
            for row in range(artificial_row + 1, rows_count):
                el_to_remove = A[row, aritificial_col]
                A[row, :] -= el_to_remove * A[artificial_row, :]
                b[row] -= el_to_remove * b[artificial_row]

        artificial_rows = [init_m + col - init_n for col in artificial_cols]
        A = np.delete(A, artificial_cols, 1)
        A = np.delete(A, artificial_rows, 0)
        b = np.delete(b, artificial_rows)
        c = np.delete(c, artificial_cols)
        x = np.delete(x, artificial_cols)
        J_b = [j for j in J_b if j < init_n]


    m = A.shape[0]
    A_b = A[:, J_b]
    print(f"x0 - {x}")
    print(f"J_b - {J_b}")
    print(f"A_b - \n{A_b}")
    try:
        j_k, cur_x = next(x for x in enumerate(x) if not is_integer(x[1]))
    except StopIteration:
        print(f"Optimum plan - {x}")
        cx = c.dot(x)
        print(f"cx - {cx}")
        break

    k = J_b.index(j_k)

    B = np.linalg.inv(A_b)

    # get truncation bound
    eDotB = np.eye(A.shape[0])[:, k].dot(B)
    new_b = eDotB.dot(b)
    bounds = eDotB.dot(A)

    truncating_bound = np.zeros(len(bounds) + 1)
    truncating_bound[-1] = 1
    for i, bound in enumerate(bounds):
        if not is_integer(bound):
            truncating_bound[i] = -1.0 * get_fraction(bound)

    new_b = -1.0 * get_fraction(new_b)

    # resize all
    A = np.hstack((A, np.zeros((m, 1))))
    A = np.vstack((A, truncating_bound))
    b = np.append(b, new_b)
    c = np.append(c, 0)
