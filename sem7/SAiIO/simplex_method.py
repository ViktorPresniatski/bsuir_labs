from numpy import *
from dual_simplex import dual_simplex


class SimplexMethod(object):
    def __init__(self, A, b, c, x = None, basic_indices = None, non_basic_indices = None):
        self.A = A
        self.b = b
        self.c = c
        self.x = x
        self.m, self.n = A.shape
        self.basic_indices = basic_indices
        self.non_basic_indices = non_basic_indices
        self.eps = finfo(float).eps

    def solve(self):
        if self.x is None:
            self.first_phase()
        self.x = self.second_phase(self.A, self.b, self.c, self.x, self.basic_indices, self.non_basic_indices)
        return self

    def is_zero(self, value):
        return abs(value) < self.eps

    def normalize(self):
        if (self.b < 0).any():
            indexes = self.b < 0
            self.b[indexes] *= -1
            self.A[indexes, :] *= -1

    def first_phase(self):
        self.normalize()
        new_A = append(self.A.transpose(), eye(self.m, dtype=float64), axis = 0).transpose()
        x = append(zeros(self.n, dtype=float64), self.b)
        c = append(zeros(self.n, dtype=float64), -ones(self.m, dtype=float64))

        non_basic_indices = [i for i in range(self.n)]
        basic_indices = [i for i in range(self.n, self.n + self.m)]

        J = set(non_basic_indices)
        Ju = set(basic_indices)

        result_x = self.second_phase(new_A, self.b, c, x, basic_indices, non_basic_indices)

        if not (abs(result_x[-self.m:]) < self.eps).all():
            raise Exception("This task has no solution, because her restrictions is not compatible")

        B = linalg.inv(new_A[:, basic_indices])
        lost_indexes = list(J - set(basic_indices))

        while True:
            if not set(basic_indices) & set(Ju):
                lJ = list(J)
                self.A = new_A[:, lJ]
                self.c = self.c[lJ]
                self.x = result_x[lJ]
                self.basic_indices = [lJ.index(el) for el in basic_indices]
                self.non_basic_indices = [i for i in lJ if i not in self.basic_indices]
                return self

            jk = (set(basic_indices) & set(Ju)).pop()
            k = jk - self.n
            ek = eye(self.m)[:, k]

            tmp = dot(ek, B)
            alpha = dot(tmp, new_A[:, lost_indexes])

            if not (abs(alpha) < self.eps).all():
                s = list(abs(alpha) > self.eps).index(True)
                js = lost_indexes[s]
                basic_indices[k] = js
            else:
                del basic_indices[k]
                Ju.remove(jk)
                new_A = delete(new_A, k, axis=0)
                self.b = delete(self.b, k)
                B = delete(B, k, axis=0)
                B = delete(B, k, axis=1)
                self.m -= 1

    def get_index(self, a, el):
        try:
            return a.index(el)
        except ValueError:
            return -1

    def get_basic_indices(self):
        return self.basic_indices

    def change_B(self, z, s, m):
        zk = z[s]
        z[s] = -1
        z /= -zk
        M = eye(m)
        M[:, s] = z
        self.B = dot(M, self.B)

    def second_phase(self, A, b, c, x, basic_indices, non_basic_indices):
        m, n = A.shape
        basic_a = A[:, basic_indices]
        self.B = linalg.inv(basic_a)
        while True:
            basic_c = array([c[i] for i in basic_indices])

            # Create potential and estimate vectors
            u = dot(basic_c, self.B)
            delta = array(subtract(dot(u, A), c))

            k = self.get_index([delta[j] < 0 and not self.is_zero(delta[j]) for j in non_basic_indices], True)
            if not ~k:
                return x
            j0 = non_basic_indices[k]

            z = dot(self.B, A[:, j0])

            if all(z <= self.eps):
                raise ValueError("This task has no solution, because her target function is not limited at plans set")

            basic_x = x[basic_indices]
            rangem = [i for i in range(m)]
            tetta = [basic_x[j] / z[j] if z[j] > 0 and not self.is_zero(z[j]) else inf for j in rangem]
            tetta0 = min(tetta)
            s = self.get_index(tetta, tetta0)
            index_tetta0 = basic_indices[s]

            for i, j in enumerate(basic_indices):
                x[j] = x[j] - tetta0 * z[i]
            x[j0] = tetta0

            basic_indices[s] = j0
            basic_a[:, s] = A[:, j0]
            self.change_B(z, s, m)

            non_basic_indices[k] = index_tetta0

    def solved(self):
        cols_count = self.A.shape[1]
        d_min = array([0.0] * cols_count)
        d_max = array([9999999999.0] * cols_count)
        self.x, self.basic_indices = dual_simplex(self.A, self.b, self.c, d_min, d_max)
        basic_a = self.A[:, self.basic_indices]
        self.B = linalg.inv(basic_a)
        return self

    def solvedm(self):
        cols_count = self.A.shape[1]
        d_min = array([0.0] * cols_count)
        d_max = array([9999999999.0] * cols_count)
        d_min[-1] = -9999999999.0
        self.x, self.basic_indices = dual_simplex(self.A, self.b, self.c, d_min, d_max)
        # import pdb;pdb.set_trace()
        basic_a = self.A[:, self.basic_indices]
        self.B = linalg.inv(basic_a)
        return self


def get_basic_indices(A, b, c):
    return SimplexMethod(A, b, c).first_phase().basic_indices
