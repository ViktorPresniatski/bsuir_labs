# Example 1
# U = [(0, 1), (0, 3), (1, 3), (1, 4), (2, 4), (2, 5), (3, 2), (3, 5), (4, 5), (4, 6), (5, 6)]
# D = [4, 9, 2, 4, 1, 10, 1, 6, 1, 2, 9]
# S = 0
# T = 6

# Example 2
# U = [(1, 2), (1, 6), (2, 3), (3, 4), (3, 5), (5, 4), (5, 7), (6, 2), (6, 5), (6, 7), (7, 2), (7, 3)]
# D = [12, 6, 2, 1, 5, 15, 2, 10, 5, 8, 2, 6]
# S = 1
# T = 4

# Task 3
# U = [(0, 1), (0, 2), (0, 4), (1, 3), (1, 4), (2, 4), (2, 5), (3, 6), (3, 7), (4, 5),
#      (4, 6), (4, 7), (4, 8), (5, 7), (5, 8), (6, 7), (6, 9), (7, 8), (7, 9), (8, 9)]
# D = [2, 1, 2, 2, 1, 3, 2, 1, 2, 1, 4, 3, 3, 2, 2, 5, 3, 1, 4, 3]
# S = 0
# T = 9

# Task 4
# U = [(0, 1), (0, 2), (0, 3), (0, 4), (1, 2), (1, 3), (1, 4), (2, 6), (2, 7), (3, 2),
#      (3, 6), (3, 7), (4, 5), (5, 3), (5, 6), (6, 7)]
# D = [3, 6, 3, 2, 4, 1, 4, 3, 2, 1, 1, 2, 1, 3, 1, 4]
# S = 0
# T = 7

# Task 7
U = [(0, 4), (0, 5), (0, 6), (1, 0), (1, 2), (1, 4), (1, 6), (3, 1), (3, 2), (3, 4),
     (3, 6), (3, 8), (4, 5), (5, 8), (6, 5), (7, 2), (7, 3), (7, 6), (7, 8), (8, 2)]
D = [4, 6, 2, 3, 2, 7, 4, 3, 5, 2, 4, 2, 1, 7, 1, 3, 6, 4, 1, 1]
S = 0
T = 8

# Hungarian algo
# U = [(0, 1), (0, 2), (0, 3), (0, 4), (1, 5), (1, 7), (2, 6), (3, 7), (3, 8), (4, 5), (5, 9), (6, 9), (7, 9), (8, 9)]
# D = [1, 1, 1, 1, 999999999, 999999999, 999999999, 999999999, 999999999, 999999999, 1, 1, 1, 1]
# S = 0
# T = 9


class FordFulkerson:
    def __init__(self, U, D, s, t, log=False):
        self.U, self.D, self.s, self.t, self.log = U, D, s, t, log

    def print(self, string):
        self.log and print(string)

    def get_value(self, arr, pair):
        i = self.U.index(pair)
        return arr[i]

    def set_value(self, arr, pair, value):
        i = self.U.index(pair)
        arr[i] = value

    def get_increasing_path(self, x):
        plus = 1
        minus = -1
        I_c = 1
        I_t = 1
        L = [self.s]
        g = {self.s: (0, plus)}
        i = self.s
        p = {self.s: 1}

        while True:
            direct = [pair for pair in self.U if pair[0] == i]
            reverse = [pair for pair in self.U if pair[1] == i]

            signed = [pair[1] for pair in direct if self.get_value(x, pair) < self.get_value(self.D, pair) and pair[1] not in L]
            for j in signed:
                # print(f"g - {j} - ({i}, {plus})")
                g[j] = (i, plus)
                I_t += 1
                p[j] = I_t
                L.append(j)

            signed = [pair[0] for pair in reverse if self.get_value(x, pair) > 0 and pair[0] not in L]
            for j in signed:
                # print(f"g - {j} - ({i}, {minus})")
                g[j] = (i, minus)
                I_t += 1
                p[j] = I_t
                L.append(j)

            if self.t in L:
                # print(f"L - {L}")
                # print(f"g - {g}")
                # print(f"p - {p}")
                return self.restore_path(x, g)

            I_c += 1
            try:
                j0 = next(iter([k for k, v in p.items() if v == I_c]))
            except StopIteration:
                return [L, None]

            i = j0

    def restore_path(self, x, g):
        path = {}
        alpha = None
        cur = self.t
        self.print(f"g - {g}")
        while cur != self.s:
            i, sign = g[cur]
            if sign == 1:
                pair = (i, cur)
                value = self.get_value(self.D, pair) - self.get_value(x, pair)
            else:
                pair = (cur, i)
                value = self.get_value(x, pair)

            path[pair] = sign
            alpha = min(alpha, value) if alpha is not None else value 
            # print(alpha, value)
            cur = i

        return path, alpha

    def increase_flow(self, flow, path, alpha):
        x = flow[:]
        for pair in path:
            value = self.get_value(x, pair) + path[pair] * alpha 
            self.set_value(x, pair, value)

        return x

    def print_answer(self, x):
        answer = {}
        cost = 0
        for index, x_i in enumerate(x):
            i, j = self.U[index]
            answer[f"x{i}{j}"] = x_i

        self.print(answer)

    def build_answer(self, x):
        return {self.U[index]: x_i for index, x_i in enumerate(x)}

    def check_min_slice(self, L):
        min_slice = [pair for pair in self.U if pair[0] in L and pair[1] not in L]
        self.print(f"Min slice - {min_slice}")
        return sum([self.get_value(self.D, pair) for pair in min_slice])    

    def solve(self):
        v = 0
        x = [0] * len(self.U)
        iteration = 0
        self.print('=================')
        self.print('Initial data')
        self.print(f'U - {self.U}')
        self.print(f'D - {self.D}')
        self.print('=================')

        while True:
            iteration += 1
            self.print(f"\nIteration - {iteration}")
            self.print_answer(x)
            # print(f"x - {x}")
            self.print(f"v - {v}")

            path, alpha = self.get_increasing_path(x)

            if not path or not alpha:
                self.print(f'Rest path - {path}')
                self.print(f"Check v - {self.check_min_slice(path)}")
                max_flow = {'value': v, 'rest_path': path, 'flow': self.build_answer(x)}
                return max_flow

            self.print(f"Increasing path - {path}")
            self.print(f"Alpha - {alpha}")

            x = self.increase_flow(x, path, alpha)
            v += alpha


if __name__ == '__main__':
    FordFulkerson(U, D, S, T, log=True).solve()
