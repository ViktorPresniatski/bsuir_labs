import copy
import itertools
from pprint import pprint
from lab8 import FordFulkerson
from numpy import inf

# Lecture example
# C = [
#     [2,  10, 9,  7 ],
#     [15, 4,  14, 8 ],
#     [13, 14, 16, 11],
#     [4,  15, 13, 19],
# ]

# Example 1
# C = [
#     [2, -1,  9, 4],
#     [3,  2,  5, 1],
#     [13, 0, -3, 4],
#     [5,  6,  1, 2],
# ]

# Task 1
# C = [
#     [6, 4, 13, 4, 19, 15, 11, 8],
#     [17, 15, 18, 14, 0, 7, 18, 7],
#     [3, 5, 11, 9, 7, 7, 18, 16],
#     [17, 10, 16, 19, 9, 6, 1, 5],
#     [14, 2, 10, 14, 11, 6, 4, 10],
#     [17, 11, 17, 12, 1, 10, 6, 19],
#     [13, 1, 4, 2, 2, 7, 2, 14],
#     [12, 15, 19, 11, 13, 1, 7, 8],
# ]

# Task 5
# C = [
#     [7,4,5,3,8,9,6,5,5,3,2],
#     [5,6,9,4,9,0,0,4,4,7,2],
#     [8,8,3,2,7,3,7,6,7,4,6],
#     [7,4,9,9,3,7,3,8,1,5,8],
#     [5,2,4,3,3,9,6,2,5,1,3],
#     [9,4,5,8,6,3,3,1,7,6,5],
#     [9,1,0,3,1,2,7,6,9,4,6],
#     [5,6,8,0,9,9,1,9,3,0,8],
#     [4,6,5,6,4,7,5,3,8,0,1],
#     [2,3,7,8,4,9,5,0,2,8,0],
#     [7,6,7,1,9,5,7,4,2,3,0],
# ]

# Task 7
# C = [
#     [2, 6, 5, -1, 6, 1, 8, 4, 6],
#     [2, 1, 2, 7, 9, -2, 8, 2, 0],
#     [0, 6, 0, 5, 1, 3, 4,3, 5],
#     [7, 0, 8, 9, 2, 4, 1, 6, 7],
#     [-1, 1, 0, -3, 0, 2, 2, 2, 1],
#     [3, 0, 6, 6, 1, -2, 2, 4, 0],
#     [1, 7, 1, 9, 4, 8, 2, 6,8],
#     [5, 1, 5, 2, 2, 6, -1, 5, 4],
#     [3, 6, 0, 6, 3, 0, 9, 1, 2],
# ]

# Task 8
C = [
    [2, 4, 0, 3, 8, -1, 6, 5],
    [8, 6, 3, 4, 2, 0, 0, 4],
    [8, -4, 3, 2, 7, 3, 1, 0],
    [2, 4, 9, 5, 3, 0, 3, 8],
    [5, 2, 7, 3, -1, 0, 3, 2],
    [3, 2, 5, 1, 5, 3, 0, 1],
    [2, 1, 0, -3, 1, 2, 7, 0],
    [1, 6, 4, 0, 0, 9, 1, 7],
]


def transpose_matrix(matrix):
    n = len(matrix)
    matrix_T = copy.deepcopy(matrix)

    for i, row in enumerate(matrix):
        for j, col in list(enumerate(row))[i:]:
            matrix_T[i][j], matrix_T[j][i] = matrix_T[j][i], matrix_T[i][j]

    return matrix_T


class HungarianAlgorithm:

    def __init__(self, C, log=False):
        self.initial_C = copy.deepcopy(C)
        self.C = C
        self.n = len(C)
        self.log = log

    def solve(self):
        self.reduce_matrix()
        self.print('Reduced matrix:')
        self.pprint(self.C)
        iteration = 0

        while True:
            iteration += 1
            self.print(f'\nIteration - {iteration}')
            max_flow = self.determine_assignments()
            self.print(f'Max flow - {max_flow["value"]}')
            self.print(f'Rest path - {max_flow["rest_path"]}')

            if max_flow['value'] == self.n:
                optimal_assignment, cost = self.build_answer(max_flow['flow'])
                self.print('Optimal assignment:')
                route = self.build_route(optimal_assignment, cost)
                self.pprint(route)
                self.print(f'Cost - {cost}')
                return {'optimal_assignment': optimal_assignment, 'cost': cost, 'route':route}

            self.modify_reduced_matrix(max_flow['rest_path'])
            self.print('Modified matrix:')
            self.pprint(self.C)

    def reduce_matrix(self):
        self._reduce_rows()
        self._reduce_cols()

    def determine_assignments(self):
        U, D, s, t = self._build_network()
        max_flow = FordFulkerson(U, D, s, t).solve()
        return max_flow

    def modify_reduced_matrix(self, rest_path):
        N = list(range(1, self.n + 1))
        N_1 = [i for i in N if i in rest_path]
        N_2 = [i for i in N if i + self.n in rest_path]

        NN = itertools.product(N_1, list(set(N) - set(N_2)))
        alpha = min([self.C[i - 1][j - 1] for i, j in NN])
        self.print(f'Alpha - {alpha}')

        for i, row in enumerate(self.C):
            for j, col in enumerate(row):
                if i + 1 in N_1 and j + 1 not in N_2:
                    self.C[i][j] -= alpha
                elif i + 1 not in N_1 and j + 1 in N_2:
                    self.C[i][j] += alpha

    def build_answer(self, flow):
        cost = 0
        x = [[0 for __ in range(self.n)] for _ in range(self.n)]
        for i, j in self.U0:
            if flow[(i, j)] == 1:
                x[i - 1][j - self.n - 1] = 1
                cost += self.initial_C[i - 1][j - self.n - 1]

        return x, cost

    def _reduce_rows(self):
        for row in self.C:
            min_elem = min(row)            
            for i, elem in enumerate(row):
                row[i] -= min_elem

    def _reduce_cols(self):
        C_T = transpose_matrix(self.C)
        min_elems = [min(row) for row in C_T]

        for row in self.C:
            for j, elem in enumerate(row):
                row[j] -= min_elems[j]

    def _build_network(self):
        s = 0
        N = list(range(1, self.n + 1))
        N_star = list(range(self.n + 1, 2 * self.n + 1))
        t = 2 * self.n + 1

        U1 = list(itertools.product([s], N))
        U_star = list(itertools.product(N_star, [t]))
        U0 = [(i, j) for i, j in itertools.product(N, N_star) if self.C[i - 1][j - self.n - 1] == 0]
        self.U0 = U0

        U1_D = list(zip(U1, [1] * len(U1)))
        U_star_D = list(zip(U_star, [1] * len(U_star)))
        U0_D = list(zip(U0, [inf] * len(U0)))

        U = sorted(U1_D + U_star_D + U0_D, key=lambda pair: (pair[0][0], pair[0][1]))
        D = [d for pair, d in U]
        U = [pair for pair, d in U]

        return U, D, s, t

    def build_route(self, matrix, cost):
        answer = []
        for i, row in enumerate(matrix):
            for j, col in enumerate(row):
                if col == 1:
                    answer.append((i, j))

        return answer

    def print(self, text):
        self.log and print(text)

    def pprint(self, text):
        self.log and pprint(text)


if __name__ == '__main__':
    HungarianAlgorithm(C, log=True).solve()
