from pprint import pprint
from copy import deepcopy


inf = 99999999999


# g = [
#     [(1, 9), (3, 3)],
#     [(0, 9), (2, 2), (4, 7)],
#     [(1, 2), (3, 2), (4, 4), (5, 8), (6, 6)],
#     [(0, 3), (2, 2), (6, 5)],
#     [(1, 7), (2, 4), (5, 10)],
#     [(2, 8), (4, 10), (6, 7)],
#     [(2, 6), (3, 5), (5, 7)],
#     [(4, 9), (5, 12), (6, 10)],
# ]

# task 4
g = [
    [(1, 3), (2, 4), (4, 5)],
    [(2, 2), (3, 1), (6, 4)],
    [(3, 3), (4, 2)],
    [(6, 3)],
    [(3, 4), (5, 8), (7, 3)],
    [(3, 5), (7, 2)],
    [(5, 2), (7, 1)],
    [],
]


s = 0
t = 4

n = len(g)
R = [list(range(n)) for i in range(n)]
D = [[inf if i != j else 0 for j in range(n)] for i in range(n)]

for v, g_v in enumerate(g):
    for to, c in g_v:
        D[v][to] = c

for j_bas in range(n):
    # print(f"Iteration - {j_bas + 1}")
    D_bas_col = [row[j_bas] for row in D]
    rows = [i for i, el in enumerate(D_bas_col) if el != inf and i != j_bas]
    cols = [i for i, el in enumerate(D[j_bas]) if el != inf and i != j_bas]
    for i in rows:
        for j in cols:
            if i != j and D[i][j_bas] + D[j_bas][j] < D[i][j]:
                D[i][j] = D[i][j_bas] + D[j_bas][j]
                R[i][j] = R[i][j_bas]

    # pprint(D)
    # pprint([[j + 1 for j in i] for i in R])

pprint(D)
R = [[j + 1 for j in i] for i in R]
pprint(R)


# way = [s]
# to = s
# while to != t:
#     to = R[to][t]
#     way.append(to)

# print(D[s][t])
# print(way)