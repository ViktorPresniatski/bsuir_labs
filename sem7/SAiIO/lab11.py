from collections import Counter
from pprint import pprint
from copy import deepcopy
from functools import reduce
from lab10 import HungarianAlgorithm
from numpy import inf

# Example 1
# D = [
#     [inf, 2, 1, 10, 6],
#     [4, inf, 3, 1, 3],
#     [2, 5, inf, 8, 4],
#     [6, 7, 13, inf, 3],
#     [10, 2, 4, 6, inf],
# ]

# Task 7
D = [
    [inf, 12, 11, 1, 18, 4, 14, 3, 18],
    [9, inf, 14, 12, 7, 10, 4, 18, 9],
    [7, 8, inf, 18, 1, 6, 1, 9, 19],
    [10, 18, 0, inf, 3, 14, 3, 11, 4],
    [7, 3, 17, 10, inf, 14, 14, 9, 8],
    [17, 16, 17, 16, 8, inf, 9, 3, 19],
    [13, 19, 8, 19, 12, 0, inf, 13, 4],
    [3, 3, 7, 6, 9, 15, 16, inf, 15],
    [5, 13, 15, 19, 6, 5, 5, 2, inf],
]

# Task 8
# D = [
#     [inf, 1, 14, 18, 11, 5, 13, 18, 17, 5, 11],
#     [4, inf, 19, 14, 5, 3, 6, 15, 14, 15, 14],
#     [12, 6, inf, 16, 19, 15, 6, 2, 12, 15, 8],
#     [14, 4, 18, inf, 15, 0, 18, 13, 6, 2, 8],
#     [19, 15, 19, 14, inf, 12, 9, 15, 3, 11, 16],
#     [10, 6, 11, 4, 15, inf, 10, 9, 0, 9, 6],
#     [16, 0, 10, 17, 18, 6, inf, 4, 4, 1, 0],
#     [7, 17, 17, 6, 7, 12, 10, inf, 14, 9, 17],
#     [19, 5, 7, 6, 16, 4, 6, 17, inf, 13, 14],
#     [2, 11, 11, 16, 12, 7, 14, 12, 15, inf, 0],
#     [1, 14, 10, 0, 10, 3, 1, 0, 5, 6, inf],
# ]

# Task 9
# D = [
#     [inf, 8, 12, 7,5,0, 11, 5, 13, 9, 18, 1],
#     [10, inf, 14, 4, 7, 4, 10, 10, 6, 6, 4, 3],
#     [4, 16, inf, 13, 3, 2, 5, 5, 15, 7, 11, 19],
#     [3, 7, 11, inf, 7, 6, 14, 3, 3, 8, 8, 18],
#     [11, 15, 18, 12, inf, 19, 12, 13, 11, 16, 1, 12],
#     [8, 7, 16, 19, 1, inf, 3, 16, 12, 11, 0, 5],
#     [5, 10, 8, 0, 17, 10, inf, 6, 13, 1, 0, 6],
#     [6, 6, 6, 5, 1, 5, 17, inf, 7, 14, 11, 5],
#     [19, 8, 4, 19, 13, 2, 5, 14, inf, 12, 15, 16],
#     [11, 8, 8, 3, 4, 3, 4, 11, 2, inf, 4, 15],
#     [9, 6, 12, 0, 18, 13, 14, 3, 12, 16, inf, 4],
#     [18, 10, 8, 3, 18, 17, 16, 19, 7, 0, 12, inf],
# ]

# Task 10
# D = [
#     [inf, 10, 17, 15, 0, 15, 2, 16, 10, 2, 6, 19, 10],
#     [1, inf, 9, 5, 13, 4, 13, 9, 18, 10, 14, 2, 9],
#     [7, 9, inf, 12, 13, 12, 7, 7, 9, 15, 0, 3, 12],
#     [6, 1, 19, inf, 9, 17, 4, 1, 0, 10 ,10, 15, 18],
#     [13, 9, 9, 8, inf, 2, 6, 4, 14, 2, 0, 17, 9],
#     [17, 10, 10, 13, 1, inf, 14, 8, 14, 17, 14, 14, 2],
#     [17, 18, 3, 2, 6, 0, inf, 19, 14, 3, 13, 3, 13],
#     [0, 4, 1, 9, 6, 6, 16, inf, 3, 19, 8, 15, 4],
#     [15, 7, 5, 14, 6, 10, 1, 4, inf, 4, 16, 17, 19],
#     [1, 9, 18, 7, 16, 16, 1 ,19, 16, inf, 1, 6, 12],
#     [7, 6, 7, 13, 8, 18, 10, 5, 19, 9, inf, 5, 10],
#     [10, 16, 10, 5, 2, 5, 9, 13, 6, 7, 9, inf, 7],
#     [18 ,19, 4, 14, 13, 12, 7, 11, 8, 11, 12, 13, inf],
# ]

def initialize():
    n = len(D)
    init_route = [(i, i + 1) for i in range(n - 1)]
    init_route.append((n - 1, 0))
    r0 = calculate_route_length(init_route)
    return init_route, r0


def calculate_route_length(route):
    route = dict(route)
    return reduce(lambda memo, x: memo + D[x][route[x]], route, 0)


def find_cycles(route):
    u = route[:]
    g = dict(u)
    cycles = []
    cur_cycle = []
    v = u[0][0]
    used = [0] * len(g)

    while len(u) != 0:
        used[v] = 1
        to = g[v]
        cur_cycle.append((v, to))
        u.remove((v, to))
        if not used[to]:
            v = to
        else:
            v = u and u[0][0]
            cycles.append(cur_cycle)
            cur_cycle = []

    return cycles


def find_min_cycle(cycles):
    lengths = [len(c) for c in cycles]
    min_len = min(lengths)
    return cycles[lengths.index(min_len)]


def print_answer(route, r0):
    counter = Counter([i for r in route for i in r])
    start = [key for key, val in counter.items() if val == 2][0]
    g = dict(route)
    answer = [start]
    v = g[start]
    while v != start:
        answer.append(v)
        v = g[v]

    answer.append(start)
    print(' -> '.join([str(i + 1) for i in answer]))
    print(f'Length - {r0}')


def main():
    optimal_route, r0 = initialize()
    tasks = [HungarianAlgorithm(deepcopy(D))]
    iteration = 0

    while len(tasks) != 0:
        # iteration += 1
        # print(f'Iteration - {iteration}')

        algo = tasks.pop(0)
        solution = algo.solve()
        route = solution['route']
        r = calculate_route_length(route)

        if r >= r0:
            continue

        cycles = find_cycles(route)
        if len(cycles) == 1:
            r0 = r
            optimal_route = route[:]
            continue

        min_cycle = find_min_cycle(cycles)
        for i, j in min_cycle:
            C = deepcopy(algo.C)
            C[i][j] = inf
            tasks.append(HungarianAlgorithm(C))

    print_answer(optimal_route, r0)

if __name__ == '__main__':
    main()
