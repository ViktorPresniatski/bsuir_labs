import numpy as np
from simplex_method import SimplexMethod
from scipy.optimize import linprog


# matrix = np.array([
#     [1, -1, -2],
#     [-1, 1, 1],
#     [2, -1, 0],
# ])

# Task 1
# matrix = np.array([
#     [1, -1, -2],
#     [-1, 1, 1],
#     [2, -1, -2],
# ])

# Task 2
# matrix = np.array([
#     [1.0, -3.0, 1.0, -3.0],
#     [3.0, 1.0, 2.5, 1.0],
#     [-1.0, 2.5, 1.0, 1.0],
#     [3.0, -1.0, -1.0, 11.0],
# ])

# Task 3 #######
# matrix = np.array([
#     [1, -6, 3, -3],
#     [5, -1, 5, -4],
#     [-3, -3, 1, -1],
# ], dtype=float)

# Task 4 #######
# matrix = np.array([
#     [1, -4, 3, -3],
#     [1, 0, 1, -2],
#     [-2, 1, 2, -2],
#     [2, -2, 1, -1],
# ], dtype=float)

# Task 5
# matrix = np.array([
#     [1, -3, 3, -3],
#     [1, 0, -1, 2],
#     [-2, 1, 2, -2],
#     [2, -2, 1, -1],
# ], dtype=float)

# Task 6
# matrix = np.array([
#     [1, -5, 3, -6],
#     [6, 0, -1, 2],
#     [-4, 1, 2, -2],
#     [2, -2, 3, -3],
# ], dtype=float)

# Task 7
# matrix = np.array([
#     [3, -6, 4, -8],
#     [8, 6, -1, 2],
#     [-4, 1, 5, -2],
# ], dtype=float)

# Task 9
# matrix = np.array([
#     [4, -3, 2, -3],
#     [-5, 2, -1, 2],
#     [-4, 1, 6, 1],
# ], dtype=float)

# Task 10 #######
# matrix = np.array([
#     [4, -3, 2, 4],
#     [-5, 2, -1, 2],
#     [4, 1, -2, 5],
#     [1, -1, -1, 1],
# ], dtype=float)

# Task 11 #######
# matrix = np.array([
#     [4, -3, 2],
#     [-5, 2, -1],
#     [-4, 1, 6],
#     [1, -1, -1],
# ], dtype=float)

# Task 12
matrix = np.array([
    [3, -5, 2],
    [-3, 3, 1],
    [2, -1, -2],
    [4, 2, 5],
], dtype=float)


eps = np.finfo(float).eps


def build_task(A):
    m, n = A.shape

    artifical = -1.0 * np.eye(m)
    A = np.column_stack((A, artifical))

    c = np.zeros(A.shape[1])

    game_cost = -1.0 * np.ones(m)
    A = np.column_stack((A, game_cost))

    strategies = np.ones(n)
    strategies = np.append(strategies, np.zeros(A.shape[1] - n))
    b = np.zeros(A.shape[0])

    A = np.vstack((A, strategies))
    b = np.append(b, np.ones(1))
    c = np.append(c, -1 * np.ones(1))

    return A, b, c


def solve_task(A, b, c):
    bounds = [(0, None) for i in range(len(c) - 1)]
    bounds.append((None, None))
    simplex = linprog(A_eq=A, b_eq=b, c=c, bounds=bounds)
    return simplex.x


def get_basic_indices(x, A, b):
    basic_indices = [i for i, x_i in enumerate(x) if abs(x_i) > eps]

    if len(basic_indices) < len(b):
        J = range(len(x))
        J_n = list(set(J) - set(basic_indices))

        for j_i in reversed(sorted(J_n)):
            J_b = np.append(basic_indices, j_i)
            det = np.linalg.det(A[:, J_b])

            if abs(det) > eps:
                return J_b

    return basic_indices


def main(init_A):
    init_A = init_A.transpose()
    m, n = init_A.shape

    A, b, c = build_task(init_A)
    x = solve_task(A, b, c)
    basic_indices = get_basic_indices(x, A, b)

    v = round(x[-1], 3)
    y = np.dot(c[basic_indices], np.linalg.inv(A[:, basic_indices]))
    x = [round(i, 3) for i in x][:n]
    y = [round(i, 3) for i in y][:m]

    print(f'X - {x}')
    print(f'Y - {y}')
    print(f'v - {v}')

if __name__ == '__main__':
    main(matrix)
