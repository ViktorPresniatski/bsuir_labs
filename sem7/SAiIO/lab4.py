import operator


class ResourceAllocation:
    def __init__(self, c, n, f_x):
        self.c, self.n, self.f_x = c, n, f_x
        self.B = [list(zip(f_x[0], [0] * (c + 1)))]
        self.max_B = (0, 0)

    def solve(self):
        self.fill_B()
        return self.get_optimum_plan()

    def fill_B(self):
        for k in range(1, self.n):
            print(k)
            new_B = [self.get_B_y(z, k) for z in range(0, self.c + 1)]
            b_max = max(new_B, key=operator.itemgetter(0))
            self.B.append(new_B)

            if b_max[0] > self.max_B[0]:
                self.max_B = b_max

    def get_optimum_plan(self):
        c = self.c
        optimum_plan = [self.max_B[1]]
        c -= self.max_B[1]
        for k in list(reversed(range(self.n)))[1:-1]:
            x0 = self.B[k][c]
            optimum_plan.append(x0[1])
            c -= x0[1]

        optimum_plan.append(c)
        return list(reversed(optimum_plan))

    def get_B_y(self, z, k):
        pairs = self.make_pairs(z)
        return self.get_max(k, pairs)

    def make_pairs(self, z):
        ret = []
        for i in range(z + 1):
            ret.append((i, z - i))
        return ret

    def get_max(self, k, pairs):
        temp_arr = [self.func(k, pair[0], pair[1]) for pair in pairs]
        index, value = max(enumerate(temp_arr), key=operator.itemgetter(1))
        return (value, index)

    def func(self, k, f, b):
        return self.f_x[k][f] + self.B[k - 1][b][0]


if __name__ == '__main__':
    c = 6
    n = 3
    f_x = [
        [0, 3, 4, 5, 8, 9, 10],
        [0, 2, 3, 7, 9, 12, 13],
        [0, 1, 2, 6, 11, 11, 13]
    ]

    # # task 1
    # c = 6
    # n = 3
    # f_x = [
    #     [0, 1, 2, 2, 4, 5, 6],
    #     [0, 2, 3, 5, 7, 7, 8],
    #     [0, 2, 4, 5, 6, 7, 7]
    # ]

    # task 3
    # c = 7
    # n = 3
    # f_x = [
    #     [0, 1, 2, 4, 8, 9, 9, 23],
    #     [0, 2, 4, 6, 6, 8, 10, 11],
    #     [0, 3, 4, 7, 7, 8, 8, 24]
    # ]

    # task 5
    # c = 8
    # n = 4
    # f_x = [
    #     [0, 2, 2, 3, 5, 8, 8, 10, 17],
    #     [0, 1, 2, 5, 8, 10, 11, 13, 15],
    #     [0, 4, 4, 5, 6, 7, 13, 14, 14],
    #     [0, 1, 3, 6, 9, 10, 11, 14, 16]
    # ]

    # task 7
    # c = 11
    # n = 5
    # f_x = [
    #     [0, 4, 4, 6, 9, 12, 12, 15, 16, 19, 19, 19],
    #     [0, 1, 1, 1, 4, 7, 8, 8, 13, 13, 13, 19, 20],
    #     [0, 2, 5, 6, 7, 8, 9, 11, 11, 13, 13, 18],
    #     [0, 1, 2, 4, 5, 7, 8, 8, 9, 9, 15, 19],
    #     [0, 2, 5, 7, 8, 9, 10, 10, 11, 14, 17, 21]
    # ]

    ra = ResourceAllocation(c, n, f_x)
    answer = ra.solve()
    print(answer)
