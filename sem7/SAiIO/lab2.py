import numpy as np
from lab1 import DualSimplex

# task 5
A = np.array([[1.0, -5.0, 3.0, 1.0, 0.0, 0.0],
           [4.0, -1.0, 1.0, 0.0, 1.0, 0.0],
           [2.0, 4.0, 2.0, 0.0, 0.0, 1.0]])
b = np.array([-8.0, 22.0, 30.0])
c = np.array([7.0, -2.0, 6.0, 0.0, 5.0, 2.0])
d_min = np.array([2.0, 1.0, 0.0, 0.0, 1.0, 1.0])
d_max = np.array([6.0, 6.0, 5.0, 2.0, 4.0, 6.0])

# task 1
# A = np.array([[1, 0, 0, 12, 1, -3, 4, -1],
#            [0, 1, 0, 11, 12, 3, 5, 3],
#            [0, 0, 1, 1, 0, 22, -2, 1]])
# b = np.array([40, 107, 61])
# c = np.array([2, 1, -2, -1, 4, -5, 5, 5])
# d_min = np.array([0, 0, 0, 0, 0, 0, 0, 0])
# d_max = np.array([3, 5, 5, 3, 4, 5, 6, 3])

# example 1
# A = np.array([
#     [1, -5, 3, 1, 0, 0],
#     [4, -1, 1, 0, 1, 0],
#     [2, 4, 2, 0, 0, 1]
# ])
# b = np.array([-8, 22, 30])
# c = np.array([7, -2, 6, 0, 5, 2])
# d_min = np.array([2, 1, 0, 0, 1, 1])
# d_max = np.array([6, 6, 5, 2, 4, 6])

# example 3
# A = np.array([
#     [1, 0, 1, 0, 0, 1],
#     [1, 2, -1, 1, 1, 2],
#     [-2, 4, 1, 0, 1, 0]
# ])
# b = np.array([-3, 3, 13])
# c = np.array([-3, 2, 0, -2, -5, 2])
# d_min = np.array([-2, -1, -2, 0, 1, -4])
# d_max = np.array([2, 3, 1, 5, 4, -1])


def is_integer(number):
    e = 0.001
    return abs(number - round(number)) < e


r0 = -np.inf
answer = None
x0 = None
tasks = [DualSimplex(A, b, c, d_min, d_max)]


iter_count = 0
while True:
    iter_count += 1
    print(f"Iteration - {iter_count}")

    if len(tasks) == 0:
        if answer:
            print(f"Optimum plan - {x0}")
            print(r0)
        else:
            print("No solution")
        break

    task = tasks.pop(0)
    solution = task.solve()
    r = solution['cx'] if solution['cx'] is not None else 0
    if solution['x'] is not None:
        print(f"d_min - {task.d_min}")
        print(f"d_max - {task.d_max}")
        print(f"Current solution - {solution['x']}")
        print(f"cx - {solution['cx']}")

    if solution['x'] is None or r <= r0:
        continue

    if all([is_integer(x) for x in solution['x']]):
        answer = True
        x0 = solution['x']
        r0 = r
        continue

    j0, cur_x = next(x for x in enumerate(solution['x']) if not is_integer(x[1])) or -1
    k = np.floor(cur_x)
    if cur_x < k < 0:
        k = -1
    # import pdb; pdb.set_trace()
    new_d_max = task.d_max.copy()
    new_d_max[j0] = k
    tasks.append(DualSimplex(A, b, c, task.d_min, new_d_max))
    new_d_min = task.d_min.copy()
    new_d_min[j0] = k + 1
    tasks.append(DualSimplex(A, b, c, new_d_min, task.d_max))
