# Example
# U = [(1, 2), (2, 6), (3, 2), (3, 4), (5, 3), (5, 4), (6, 1), (6, 3), (6, 5)]
# C = [1, 3, 3, 5, 4, 1, -2, 3, 4]
# X = [1, 0, 3, 1, 0, 5, 0, 9, 0]
# U_bas = [(1, 2), (3, 2), (3, 4), (5, 4), (6, 3)]

# Task 7
# U = [(1, 2), (1, 3), (1, 5), (2, 3), (2, 6), (3, 4), (3, 5), (4, 6), (5, 4), (5, 6), (6, 7), (7, 1), (7, 5)]
# C = [7, 6, 3, 4, 3, 6, 5, 1, 4, -1, 4, 2, 7]
# X = [2, 3, 0, 0, 0, 4, 4, 0, 0, 0, 2, 0, 5]
# U_bas = [(1, 2), (1, 3), (3, 4), (3, 5), (6, 7), (7, 5)]

# Task 8
# U = [(1, 2), (1, 5), (1, 6), (2, 4), (2, 6), (3, 1), (3, 2), (3, 4), (3, 5), (4, 5), (6, 4), (6, 5)]
# C = [5, 1, 5, 10, 3, 1, 3, 6, 2, 3, 2, 4]
# X = [4, 0, 2, 5, 0, 0, 0, 1, 0, 0, 0, 3]
# U_bas = [(1, 2), (1, 6), (2, 4), (3, 4), (6, 5)]

# Task 6
U = [(1, 2), (1, 6), (1, 7), (2, 3), (2, 4), (2, 6), (3, 5), (3, 6), (4, 3), (4, 8), (5, 8), (6, 5), (6, 7), (6, 8), (7, 2), (7, 5), (7, 8)]
C = [6, 2, -2, 3, 6, 1, 3, 4, -1, 1, 7, 5, 5, 3, 4, 2, 2]
X = [2, 0, 0, 0, 2, 0, 0, 6, 0, 0, 5, 3, 3, 0, 4, 0, 0]
U_bas = [(1, 2), (2, 4), (3, 6), (5, 8), (6, 5), (6, 7), (7, 2)]


U = [(pair[0] - 1, pair[1] - 1) for pair in U]
U_bas = [(pair[0] - 1, pair[1] - 1) for pair in U_bas]


def get_value(arr, pair):
    i = U.index(pair)
    return arr[i]


def set_value(arr, pair, value):
    i = U.index(pair)
    arr[i] = value


def get_U_n(U_b):
    return list(set(U) - set(U_b))


def get_potentials(U_b):
    u = {0: 0}
    queue = U_b[:]

    while len(queue) != 0:
        i, j = list(filter(lambda x: set(x) & set(u.keys()), queue))[0]
        if i in u:
            u[j] = u[i] - get_value(C, (i, j))
        elif j in u:
            u[i] = u[j] + get_value(C, (i, j))
        queue.remove((i, j))

    return u


def get_deltas(u, U_b):
    deltas = [0 for _ in range(len(U))]
    for i, j in get_U_n(U_b):
        value = u[i] - u[j] - get_value(C, (i, j))
        set_value(deltas, (i, j), value)

    return deltas


def find_cycle(U_b, pair):
    graph = U_b[:]
    graph.append(pair)

    while True:
        counter = {}
        for pair in graph:
            for i in pair:
                counter[i] = counter.get(i, 0) + 1

        ones = [k for k, v in counter.items() if v == 1]
        if not ones:
            break

        graph = [pair for pair in graph if not set(pair) & set(ones)]

    return graph


def get_U_plus_minus(cycle, pair_0):
    graph = cycle[:]
    direction = 1
    signs = {}
    U_plus_minus = ([], [])

    v = pair_0
    signs[v] = direction
    graph.remove(v)

    while graph:
        next_v = [c for c in graph if c[direction ^ 1] == v[direction]]
        if not next_v:
            next_v = [c for c in graph if c[direction] == v[direction]]
            direction ^= 1

        v = next_v[0]
        signs[v] = direction
        graph.remove(v)

    for pair, sign in signs.items():
        U_plus_minus[sign ^ 1].append(pair)

    return U_plus_minus


def get_theta_with_old_pair(x, U_minus):
    old_pair = min(U_minus, key=lambda pair: get_value(x, pair))
    theta = get_value(x, old_pair)
    return theta, old_pair


def recalculate_flow(x, theta, U_plus, U_minus):
    new_x = x[:]

    for pair in U_plus:
        value = get_value(new_x, pair) + theta
        set_value(new_x, pair, value)

    for pair in U_minus:
        value = get_value(new_x, pair) - theta
        set_value(new_x, pair, value)

    return new_x


def print_answer(x):
    answer = {}
    cost = 0
    for index, x_i in enumerate(x):
        i, j = U[index]
        cost += x_i * C[index]
        answer[f"x{i + 1}{j + 1}"] = x_i

    print(answer)
    print(f"Cost - {cost}")


def main():
    x = X[:]
    U_b = U_bas[:]

    iteration = 0
    while True:
        iteration += 1
        print(f"\nIteration - {iteration}")
        print(f"X - {x}")
        print(f"U_b - {U_b}")

        u = get_potentials(U_b)
        print(f"u - {u}")

        deltas = get_deltas(u, U_b)
        print(f"Deltas - {deltas}")

        d_great_zero = [d for d in deltas if d > 0]
        if not d_great_zero:
            print_answer(x)
            print(f"Basis - {U_b}")
            return x, U_b

        pair_0 = U[deltas.index(d_great_zero[0])]
        print(f"i0, j0 - {pair_0}")

        cycle = find_cycle(U_b, pair_0)
        print(f"Grpah with cycle - {list(set(U_b) | set([pair_0]))}")
        print(f"Cycle - {cycle}")

        U_plus, U_minus = get_U_plus_minus(cycle, pair_0)
        print(f"U+ - {U_plus}")
        print(f"U- - {U_minus}")

        theta, old_pair = get_theta_with_old_pair(x, U_minus)
        print(f"Theta - {theta}")
        print(f"i*, j* - {old_pair}")

        x = recalculate_flow(x, theta, U_plus, U_minus)
        U_b.remove(old_pair)
        U_b.append(pair_0)


if __name__ == '__main__':
    main()
