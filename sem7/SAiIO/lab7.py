inf = 99999999999

# g = [
#     [(1, 2), (5, 1)],
#     [(2, 2), (4, 7)],
#     [(3, 8)],
#     [],
#     [(2, 1), (3, 1)],
#     [(1, 4), (2, 4), (4, 1)],
# ]
# s = 0
# t = 3

# g = [
#     [(1, 3), (2, 4), (3, 6), (4, 2)],
#     [(3, 5), (4, 1)],
#     [(1, 3), (3, 2), (5, 6)],
#     [(4, 4), (5, 7), (6, 2)],
#     [(5, 3), (6, 7), (7, 1)],
#     [(6, 1), (7, 4)],
#     [(7, 6)],
#     [],
# ]
# s = 0
# t = 7

# Task 4
g = [
    [(1, 3), (2, 4), (3, 6), (4, 2)],
    [(3, 5), (4, 1)],
    [(1, 3), (3, 2), (5, 6)],
    [(4, 4), (5, 7), (6, 2)],
    [(5, 3), (6, 7), (7, 1)],
    [(6, 1), (7, 4)],
    [(7, 6)],
    [],
]
s = 0
t = 7

# Task 5
# g = [
#     [(1, 7), (2, 9), (3, 6), (5, 3)],
#     [(5, 6)],
#     [(1, 4), (4, 3), (5, 1), (6, 4)],
#     [(1, 2), (2, 1), (4, 8)],
#     [(6, 5), (6, 1)],
#     [(6, 3)],
#     [],
# ]
# s = 0
# t = 6


n = len(g)
B = [inf] * n
f = [-1] * n

B[0] = 0
I_star = [s]

g_inv = [[] for j in range(n)]
for v, g_v in enumerate(g):
    for to, c in g_v:
        g_inv[to].append(v)

U = {}
for v, g_v in enumerate(g):
    for to, c in g_v:
        U[(v, to)] = c

# print(U)
# print(g_inv)

while t not in I_star:
    W = []
    for i in I_star:
        W.extend([v[0] for v in g[i]])
    W = list(set(W) - set(I_star))

    j_stars = [to for to in W if set(g_inv[to]) <= set(I_star)]
    for j_star in j_stars:
        temp = [B[i] + U[(i, j_star)] for i in g_inv[j_star]]
        B[j_star] = max(temp)
        f[j_star] = g_inv[j_star][temp.index(B[j_star])]
        I_star.append(j_star)

to = t
way = []
while to != -1:
    way.append(to)
    to = f[to]

way = list(reversed(way))

print(B[t])
print(way)
