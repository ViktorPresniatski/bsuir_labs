from queue import PriorityQueue

n = 10

g = [
    [(1, 5), (7, 3)],
    [(2, 2), (6, 3)],
    [(4, 5)],
    [(2, 2)],
    [(3, 1), (9, 2)],
    [(2, 4), (4, 1), (8, 2), (7, 6)],
    [(2, 2), (0, 2), (5, 5)],
    [(1, 1), (6, 4), (8, 1)],
    [(9, 5)],
    [(3, 6), (5, 3)]
]

s = 0
p = [0] * n
d = [float("inf")] * n
d[s] = 0
qu = PriorityQueue()
qu.put((0, s))
while not qu.empty():
    cur_d, v = qu.get()
    if cur_d > d[v]: continue
    for to, length in g[v]:
        if d[v] + length < d[to]:
            d[to] = d[v] + length
            p[to] = v
            qu.put((d[to], to))

print(d)
start = 0
to = 9
save = []
while start != to:
    save.append(p[to])
    to = p[to]

save.reverse()
print(save)
